#!/bin/bash
# setup {{{
# if zero arguments passed, exit
[[ $# -eq 0 ]] && echo 'Commands are: save, load, gen' && exit 1

pwd=$(pwd)
out=$pwd/out
[ ! -d "$out" ] && mkdir -p $out
dir=$2
proj=$pwd/$dir
# }}}
# save {{{
save() {
	# if out directory exists, delete it
	[ -d $out ] && rm -rf $out

	# find files in directory
	cd $proj
	txt="Clearing $out\nSaving files: $proj --> $out"
	echo -e "\e[32m$txt\e[0m"

	# copy files from proj to out
	fd -t f -H '.*(vue|js|json|html|scss|gitignore|LICENSE)$' . |
		xargs -I % sh -c '{ file=% && echo '"$proj"'/$file "-->" '"$out"'/$file && mkdir -p '"$out"'/$(dirname $file) && cp '"$proj"'/$file '"$out"'/$file ; }'

}
# }}}
# load {{{
load() {
	cd $out
	txt="Loading files: $out --> $proj"
	echo -e "\e[32m$txt\e[0m"

	fd -t f -IH . . |
		xargs -I % sh -c '{ file=% && echo '"$out"'/$file "-->" '"$proj"'/$file && mkdir -p '"$proj"'/$(dirname $file) && cp '"$out"'/$file '"$proj"'/$file ; }'
}
# }}}
# gen {{{
gen() {
  echo "generating $dir"
	[ -d $dir ] && txt="Removing $dir" && echo -e "\e[32m$txt\e[0m" && rm -rf $dir && mkdir $dir || echo "$dir does not exist"
}
# }}}
# fill {{{
fill() {
	cd $dir && ncu -u
	# images {{{
	# blog image
	file=content/blog/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author cover
	file=content/author/cover/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=90'
	wget -c $dl -O $file

	# author image
	name=wexor_tmg_L-2p8fapOA8
	file=content/author/images/$name.webp
	mkdir -p "$(dirname $file)"
	src='https://images.unsplash.com/photo-1437622368342-7a3d73a34c8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1100&q=80'
	z=2
	x=.7
	y=.4
	w=64
	h=64
	dl="$(echo $src | cut -f 1 -d '?')?fp-z=$z&fp-y=$y&fp-x=$x&crop=focalpoint&fit=crop&w=$w&h=$h"
	wget -c $dl -O $file

	# favicon image
	name=wexor_tmg_L-2p8fapOA8
	file=src/favicon.png
	mkdir -p "$(dirname $file)"
	src='https://images.unsplash.com/photo-1437622368342-7a3d73a34c8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1100&q=80'
	z=2
	x=.7
	y=.4
	w=180
	h=180
	dl="$(echo $src | cut -f 1 -d '?')?fp-z=$z&fp-y=$y&fp-x=$x&crop=focalpoint&fit=crop&w=$w&h=$h"
	wget -c $dl -O $file

	# assets images
	file=src/assets/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file
	# }}}
	# featured {{{
	for num in {1..10}; do
		file=content/blog/featured$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Featured$num
EOF
		cat <<'EOF' >>$file
tags: ['tag3', 'tag4']
author: ['author3', 'author4']
created: 2019-01-07
category: News 
image: ./images/danil_silantev_F6Da4r2x5to.jpg
excerpt: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
featured: true
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# blog {{{
	for num in {1..20}; do
		file=content/blog/entry$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Styles$num
EOF
		cat <<'EOF' >>$file
tags: ['tag1', 'tag2']
author: ['author1', 'author2']
created: 2019-01-07
category: Digital
image: ./images/danil_silantev_F6Da4r2x5to.jpg
excerpt: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
---

# Heading 1 {style="color:red;"}

## Heading 2 {style="color:yellow;"}

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# author {{{
	for num in {1..3}; do
		file=content/author/author$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
id: author$num
name: author$num
EOF
		cat <<'EOF' >>$file
bio: Primis vitae mauris turpis ornare libero odio torquent vehicula proin consequat curabitur mattis
facebook: https://www.facebook.com
twitter: https://www.twitter.com
linkedin: https://www.linkedin.com
image: ./images/wexor_tmg_L-2p8fapOA8.webp
cover: ./cover/danil_silantev_F6Da4r2x5to.jpg
---
EOF
	done
	# }}}
	# about.md {{{
	file=content/pages/about.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: About us
---

## Ossa narrat sortita fecerat sit conataque

Lorem markdownum aptos pes, Inachidos caput corrumpere! Hanc haud quam [est
candore](http://quisquis-in.io/ramossuperum) conpulit meriti. Vincere ferocia
arva.

## Eleis celeberrimus loci ait falsa infelix tuoque

Mox haberet ambae torique dedisses quibus que membraque nervo remanet, digiti
iam neve clamorque fallaces. Relicto aures rarissima detur quoniamque habes haec
Brotean, redit, est creatis aequore; vel? Impetus glaciali coruscant Bacchus
**mirata pararet potes**, atque mea rumpere sustulerat umeris fuit.

## Facundis quid

Venerit conveniunt per memori sed laniarat Dromas, solum tum. Undis lacteus
infitiatur adest [acies certius](http://www.tollit-clamavit.io/) inscius, cum ad
emittunt dextra.

Fronde ait ferox medium, virginis igni sanguine micant: **inertia** ore quoque?
Iaculi quicquid **virescere misit stirpe** Theseus Venerem! Falce taceo oves,
idem fugit, non abiit palam quantum, fontes vinci et abiit. Deiectoque exstabant
**Phrygiae** cepit munus tanto.

## Et capienda Peneia

*Haec moenia pater* signataque urget, ait quies laqueo sumitque. Misit sit
moribunda terrae sequar longis hoc, cingebant copia cultros! Alis templi taeda
solet suum mihi penates quae. Cecidere *deo agger infantem* indetonsusque ipsum;
ova formasque cornu et pectora [voce oculos](http://www.tibibene.io/iter.html),
prodis pariterque sacra finibus, Sabinae. Fugarant fuerat, famam ait toto imas
sorte pectora, est et, procubuit sua Appenninigenae habes postquam.

## Quoque aut gurgite aliquis igneus

Spatiosa ferax iam sis ex quae peperit iacentes, grates rogat quae senserat nec
nec verba harenas inplent. Per dum necis in in versus quin loquendi latens;
inde. **Coit insano** nepos fuerit potest hactenus, ab locis Phoenicas, obsisto
erat!

> Nec uterum Aurorae petentes abstulit. Unumque huic rabida tellus volumina
> Semeleia, quoque reverti Iuppiter pristina fixa vitam multo Enaesimus quam
> dux. Sua **damus** decipere, ut **obortas** nomen sine vestrae vita.

Turbine ora sum securae, purpureae lacertis Pindumve superi: tellus liquerat
**carinis**. Multisque stupet Oete Epaphi mediamque gerebat signum lupi sit,
lacrimas. Tumidi fassusque hosti, deus [vixque desint
dedit](http://hisnurus.com/putares-pars) dum et, quo non, dea [suras
tantum](http://mactata.org/inducere.php). Unus acta capulo. In Dryope sic
vestigia est neu ignis in **illa mirantur agilis** densior.
EOF
	# }}}
}
# }}}
# launch {{{
[ $1 = "save" ] || [ $1 = "load" ] || [ $1 = "gen" ] && [[ ! $# -eq 2 ]] && echo 'Directory required' && exit 1
[ $1 = "save" ] && [[ $# -eq 2 ]] && save
[ $1 = "load" ] && [[ $# -eq 2 ]] && load
[ $1 = "gen" ] && [[ $# -eq 2 ]] && gen && fill && load && cd $pwd/$dir && ncu -u && yarn && gridsome develop
#[ $1 = "blank" ] && [[ $# -eq 2 ]] && gen && load && cd $pwd/$dir && ncu -u && yarn && gridsome develop
# }}}
