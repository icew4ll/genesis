#!/bin/bash
gen() { #{{{
	[ -d $dir ] && echo "Removing $dir" && rm -rf $dir
	mkdir $dir
	cd $dir
	# .gitignore {{{
	file=.gitignore
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
*.log
.cache
.DS_Store
src/.temp
node_modules
dist
.env
.env.*
EOF
	# }}}
	# package.json {{{
	file=package.json
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
{
  "name": "test",
  "private": true,
  "scripts": {
    "build": "gridsome build",
    "develop": "gridsome develop",
    "explore": "gridsome explore"
  },
  "dependencies": {
    "gridsome": "^0.7.19"
  },
  "devDependencies": {
    "@gridsome/plugin-sitemap": "^0.4.0",
    "gridsome-plugin-remark-prismjs-all": "^0.3.5",
    "gridsome-source-static-meta": "github:noxify/gridsome-source-static-meta#master",
    "@gridsome/source-filesystem": "^0.6.2",
    "@gridsome/transformer-remark": "^0.6.1",
    "remark-attr": "^0.11.1",
    "gridsome-plugin-rss": "^1.2.0",
    "gridsome-plugin-tailwindcss": "^3.0.1",
    "gridsome-plugin-pwa": "^0.0.18",
    "gridsome-plugin-robots": "^0.2.1",
    "vue-headroom": "^0.10.1",
    "node-sass": "^4.14.1",
    "rfs": "^9.0.3",
    "sass-loader": "^9.0.2",
    "tailwindcss": "^1.5.1",
    "tailwindcss-dark-mode": "^1.1.4",
    "vue-slick-carousel": "^1.0.6",
    "v-click-outside": "^3.0.1",
    "v-tooltip": "^2.0.3",
    "vue-infinite-loading": "^2.4.5",
    "gridsome-plugin-flexsearch": "^1.0.2",
    "vue-feather-icons": "^5.0.0"
  }
}
EOF
	# }}}
	ncu -u
	yarn
}        #}}}
fill() { #{{{
	# license {{{
	file=LICENSE
	year=$(date '+%Y')
	name="icew4ll"
	mkdir -p "$(dirname $file)"
	cat <<EOF >$file
MIT License

Copyright (c) $year $name
Copyright (c) 2020 Marcus Reinhardt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
EOF
	# }}}
	# main.js {{{
	file=src/main.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
import '~/assets/scss/main.scss'
import DefaultLayout from "~/layouts/Default.vue";
import ClickOutside from "v-click-outside";
import { VTooltip, VPopover, VClosePopover } from "v-tooltip";
import InfiniteLoading from "vue-infinite-loading";

export default function(Vue, { router, head, isClient }) {
  Vue.component("Layout", DefaultLayout);
  Vue.use(ClickOutside);

  if (isClient) {
    VTooltip.options.defaultPlacement = "top-end";
    VTooltip.options.defaultClass =
      "bg-black text-xs px-2 leading-normal py-1 rounded absolute text-gray-400 max-w-xs ml-2 mt-3";
    VTooltip.options.defaultBoundariesElement = document.body;

    Vue.directive("tooltip", VTooltip);
    Vue.directive("close-popover", VClosePopover);
    Vue.component("v-popover", VPopover);
    Vue.use(InfiniteLoading);
  }
}
EOF
	# }}}
	# main.scss {{{
	file=src/assets/scss/main.scss
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
@import "~rfs/scss";

@tailwind base;

blockquote {
  @apply border-l;
  @apply border-l-4;
  @apply border-l-blue-500;
  @apply pl-4;
  @apply italic;
  @apply my-8;

  p {
    padding: 0 !important;
  }
}

h1,
.h1 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(3rem);
}

h2,
.h2 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(2.25rem);
}

h3,
.h3 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.875rem);
}

h4,
.h4 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.5rem);
}

h5,
.h5 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.25rem);
}

h6,
.h6 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.125rem);
}

@tailwind components;
@tailwind utilities;

@import "node_modules/vue-slick-carousel/dist/vue-slick-carousel";

.container {
  @apply max-w-screen-xl;
  @apply px-0;
}

.fade-enter-active,
.fade-leave-active {
  @apply transition-all;
  @apply duration-200;
}

.fade-enter,
.fade-leave-to {
  opacity: 0;
}

.featured-post-card {
  .slick-list {
    @apply h-full;
    @apply rounded-lg;

    div:not(.post-card-author):not(.featured-label):not(.post-card-content):not(.post-card-footer) {
      @apply h-full;
    }
  }

  .slick-arrow {
    @apply absolute;
    @apply bottom-0;
    @apply right-0;
    @apply text-white;
  }

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-content {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
    @apply mt-20;
    @apply ml-10;
    @apply text-white;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
    @apply left-0;
    @apply text-white;
    @apply ml-10;
    @apply mb-10;
    @apply text-sm;
    @apply font-semibold;
  }
}

.post-card {
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  &:hover {
    transform: translateY(-5px);
  }

  @apply relative;
  @apply border;

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-title {
    @apply leading-8;
    @apply text-2xl;
  }

  .post-card-excerpt {
    @apply font-serif;
  }

  .post-card-content {
    @apply relative;
    @apply flex-1;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
  }
}

.header {
  min-height: 500px;
}

.mobileSubnav {
  @apply absolute;
  @apply w-full;
  @apply -mx-2;
}

@media (max-width: 767px) {
  .header {
    min-height: 360px;
  }
}

.headroom {
  z-index: 500 !important;
  @apply shadow-md;
}

/* ––––––––––––––––––––––––––––––––––––––––––––––––––
    Based on: https://codepen.io/nickelse/pen/YGPJQG
    Influenced by: https://sproutsocial.com/
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

/* #Mega Menu Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

.mega-menu {
  left: 0;
  top: 0;
  @apply mt-16;
  @apply w-full;
  @apply absolute;
  @apply text-left;
}

/* ––––––––––––––––––––––––––––––––––––––––––––––––––
    Tooltip and Popover 
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

.tooltip {
  &[aria-hidden="true"] {
    visibility: hidden;
    opacity: 0;
    transition: opacity 0.15s, visibility 0.15s;
  }

  &[aria-hidden="false"] {
    visibility: visible;
    opacity: 1;
    transition: opacity 0.15s;
  }

  &.card-author-tooltip {
    @apply z-1000;
    @apply bg-black;
    @apply text-xs;
    @apply px-2;
    @apply leading-normal;
    @apply py-1;
    @apply rounded;
    @apply absolute;
    @apply text-gray-400;
    @apply max-w-xs;
    @apply ml-2;
    @apply mt-3;

    &::after,
    &::before {
      right: 100%;
      top: 50%;
      border: solid transparent;
      content: " ";
      height: 0;
      width: 0;
      position: absolute;
      pointer-events: none;
    }

    &::after {
      border-color: rgba(0, 0, 0, 0);
      border-right-color: #000000;
      border-width: 4px;
      margin-top: -4px;
    }

    &::before {
      border-color: rgba(0, 0, 0, 0);
      border-right-color: #000;
      border-width: 4px;
      margin-top: -4px;
    }
  }

  &.navbar-popover {
    @apply bg-black;
    @apply text-xs;
    @apply px-2;
    @apply leading-normal;
    @apply py-1;
    @apply rounded;
    @apply absolute;
    @apply text-gray-400;
    @apply max-w-xs;
    @apply ml-2;
    @apply -mt-3;
    @apply z-1000;

    &::after {
      bottom: 100%;
      left: 50%;
      border: solid transparent;
      content: " ";
      @apply h-0;
      @apply w-0;
      pointer-events: none;
      @apply absolute;
      @apply border-8;
      border-bottom-color: #000;

      margin-left: -8px;
    }
  }

  &.mobile-navbar-popover {
    @apply bg-black;
    @apply text-xs;
    @apply px-2;
    @apply leading-normal;
    @apply py-1;
    @apply rounded;
    @apply absolute;
    @apply text-gray-400;
    @apply max-w-xs;
    @apply ml-2;
    @apply -mt-3;
    @apply z-1000;

    &::after {
      right: 100%;
      top: 50%;
      border: solid transparent;
      content: " ";
      @apply h-0;
      @apply w-0;
      pointer-events: none;
      @apply absolute;
      @apply border-8;
      border-right-color: #000;

      margin-left: -8px;
    }
  }
}

/* ––––––––––––––––––––––––––––––––––––––––––––––––––
    Content
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

.mode-dark {
  .post-content {
    p,
    span:not(.token),
    li {
      @apply tracking-wider;
      @apply leading-relaxed;
      @apply font-normal;
      @apply text-dark0;
      @include font-size(1.1rem);
    }
  }
}

.post-content {
  p,
  span:not(.token),
  li {
    @apply tracking-wider;
    @apply leading-relaxed;
    @apply font-normal;
    @apply text-light0;
    @include font-size(1.1rem);
  }

  ol {
    @apply list-decimal;
    @apply ml-5;
    @apply mt-5;
  }

  ul {
    @apply list-disc;
    @apply ml-5;
    @apply mt-5;
  }

  li > ul {
    @apply mt-0;
  }
}

.post-authors a::after {
  content: ", ";
}

.post-authors a:last-child::after {
  content: "";
}

/* Generated with http://k88hudson.github.io/syntax-highlighting-theme-generator/www */
/* http://k88hudson.github.io/react-markdocs */
/**
 * @author k88hudson
 *
 * Based on prism.js default theme for JavaScript, CSS and HTML
 * Based on dabblet (http://dabblet.com)
 * @author Lea Verou
 */
/*********************************************************
* General
*/
pre[class*="language-"],
code[class*="language-"] {
  color: #5c6e74;

  text-shadow: none;
  font-family: Consolas, Monaco, "Andale Mono", "Ubuntu Mono", monospace;
  @include font-size(1.2rem);
  font-weight: bold;
  direction: ltr;
  text-align: left;
  white-space: pre;
  word-spacing: normal;
  word-break: normal;
  line-height: 1.5;
  -moz-tab-size: 4;
  -o-tab-size: 4;
  tab-size: 4;
  -webkit-hyphens: none;
  -moz-hyphens: none;
  -ms-hyphens: none;
  hyphens: none;
}

pre[class*="language-"]::selection,
code[class*="language-"]::selection,
pre[class*="language-"]::mozselection,
code[class*="language-"]::mozselection {
  text-shadow: none;
  @apply bg-light2;
}

pre[class*="language-"] {
  padding: 1em;
  margin: 0.5em 0;
  overflow: auto;
  @apply bg-light2;
}

:not(pre) > code[class*="language-"] {
  padding: 0.3em 0.3em;
  border-radius: 0.3em;
  color: #db4c69;
  @apply bg-light2;
}

.mode-dark {
  pre[class*="language-"]::selection,
  code[class*="language-"]::selection,
  pre[class*="language-"]::mozselection,
  code[class*="language-"]::mozselection {
    text-shadow: none;
    @apply bg-gray-800;
  }

  pre[class*="language-"],
  code[class*="language-"] {
    @apply text-gray-400;
  }

  pre[class*="language-"] {
    padding: 1em;
    margin: 0.5em 0;
    overflow: auto;
    @apply bg-gray-800;
  }

  :not(pre) > code[class*="language-"] {
    padding: 0.3em 0.3em;
    border-radius: 0.3em;
    color: #db4c69;
    @apply bg-gray-800;
  }
}

@media print {
  pre[class*="language-"],
  code[class*="language-"] {
    text-shadow: none;
  }
}

/*********************************************************
* Tokens
*/
.namespace {
  opacity: 0.7;
}

.token.comment,
.token.prolog,
.token.doctype,
.token.cdata {
  color: #93a1a1;
}

.token.punctuation {
  color: #999999;
}

.token.property,
.token.tag,
.token.boolean,
.token.number,
.token.constant,
.token.symbol,
.token.deleted {
  color: #990055;
}

.token.selector,
.token.attr-name,
.token.string,
.token.char,
.token.builtin,
.token.inserted {
  color: #258e25;
}

.token.operator,
.token.entity,
.token.url,
.language-css .token.string,
.style .token.string {
  color: #a67f59;
  background: transparent;
}

.token.atrule,
.token.attr-value,
.token.keyword {
  color: #0077aa;
}

.token.function {
  color: #dd4a68;
}

.token.regex,
.token.important,
.token.variable {
  color: #b05500;
}

.token.important,
.token.bold {
  font-weight: bold;
}

.token.italic {
  font-style: italic;
}

.token.entity {
  cursor: help;
}

/*********************************************************
* Line highlighting
*/
pre[data-line] {
  position: relative;
}

pre[class*="language-"] > code[class*="language-"] {
  position: relative;
  z-index: 1;
}

.line-highlight {
  position: absolute;
  left: 0;
  right: 0;
  padding: inherit 0;
  margin-top: 1em;
  background: #000;
  box-shadow: inset 5px 0 0 #f7d87c;
  z-index: 0;
  pointer-events: none;
  line-height: inherit;
  white-space: pre;
}

.mode-dark {
  .medium-zoom-overlay {
    background-color: #000 !important;
  }
}

.medium-zoom-overlay {
  @apply z-1000;
}

.medium-zoom-image {
  @apply z-1000;
}

figure {
  @apply my-8;
  @apply -mx-16;
  width: calc(100% + 8rem);

  figcaption {
    @apply text-center;
  }
}
EOF
	# }}}
	# images {{{
	# blog image
	file=content/blog/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author cover
	file=content/author/cover/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=90'
	wget -c $dl -O $file

	# author image
	name=wexor_tmg_L-2p8fapOA8
	file=content/author/images/$name.webp
	mkdir -p "$(dirname $file)"
	src='https://images.unsplash.com/photo-1437622368342-7a3d73a34c8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1100&q=80'
	z=2
	x=.7
	y=.4
	w=64
	h=64
	dl="$(echo $src | cut -f 1 -d '?')?fp-z=$z&fp-y=$y&fp-x=$x&crop=focalpoint&fit=crop&w=$w&h=$h"
	wget -c $dl -O $file

	# favicon image
	name=wexor_tmg_L-2p8fapOA8
	file=src/favicon.png
	mkdir -p "$(dirname $file)"
	src='https://images.unsplash.com/photo-1437622368342-7a3d73a34c8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1100&q=80'
	z=2
	x=.7
	y=.4
	w=180
	h=180
	dl="$(echo $src | cut -f 1 -d '?')?fp-z=$z&fp-y=$y&fp-x=$x&crop=focalpoint&fit=crop&w=$w&h=$h"
	wget -c $dl -O $file

	# assets images
	file=src/assets/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file
	# }}}
	# featured {{{
	for num in {1..10}; do
		file=content/blog/featured$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Featured$num
EOF
		cat <<'EOF' >>$file
tags: ['tag3', 'tag4']
author: ['author3', 'author4']
created: 2019-01-07
category: News 
image: ./images/danil_silantev_F6Da4r2x5to.jpg
excerpt: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
featured: true
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# blog {{{
	for num in {1..20}; do
		file=content/blog/entry$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Styles$num
EOF
		cat <<'EOF' >>$file
tags: ['tag1', 'tag2']
author: ['author1', 'author2']
created: 2019-01-07
category: Digital
image: ./images/danil_silantev_F6Da4r2x5to.jpg
excerpt: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# author {{{
	for num in {1..3}; do
		file=content/author/author$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
id: author$num
name: author$num
EOF
		cat <<'EOF' >>$file
bio: Primis vitae mauris turpis ornare libero odio torquent vehicula proin consequat curabitur mattis
facebook: https://www.facebook.com
twitter: https://www.twitter.com
linkedin: https://www.linkedin.com
image: ./images/wexor_tmg_L-2p8fapOA8.webp
cover: ./cover/danil_silantev_F6Da4r2x5to.jpg
---
EOF
	done
	# }}}
	# about.md {{{
	file=content/pages/about.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: About us
---

## Ossa narrat sortita fecerat sit conataque

Lorem markdownum aptos pes, Inachidos caput corrumpere! Hanc haud quam [est
candore](http://quisquis-in.io/ramossuperum) conpulit meriti. Vincere ferocia
arva.

## Eleis celeberrimus loci ait falsa infelix tuoque

Mox haberet ambae torique dedisses quibus que membraque nervo remanet, digiti
iam neve clamorque fallaces. Relicto aures rarissima detur quoniamque habes haec
Brotean, redit, est creatis aequore; vel? Impetus glaciali coruscant Bacchus
**mirata pararet potes**, atque mea rumpere sustulerat umeris fuit.

## Facundis quid

Venerit conveniunt per memori sed laniarat Dromas, solum tum. Undis lacteus
infitiatur adest [acies certius](http://www.tollit-clamavit.io/) inscius, cum ad
emittunt dextra.

Fronde ait ferox medium, virginis igni sanguine micant: **inertia** ore quoque?
Iaculi quicquid **virescere misit stirpe** Theseus Venerem! Falce taceo oves,
idem fugit, non abiit palam quantum, fontes vinci et abiit. Deiectoque exstabant
**Phrygiae** cepit munus tanto.

## Et capienda Peneia

*Haec moenia pater* signataque urget, ait quies laqueo sumitque. Misit sit
moribunda terrae sequar longis hoc, cingebant copia cultros! Alis templi taeda
solet suum mihi penates quae. Cecidere *deo agger infantem* indetonsusque ipsum;
ova formasque cornu et pectora [voce oculos](http://www.tibibene.io/iter.html),
prodis pariterque sacra finibus, Sabinae. Fugarant fuerat, famam ait toto imas
sorte pectora, est et, procubuit sua Appenninigenae habes postquam.

## Quoque aut gurgite aliquis igneus

Spatiosa ferax iam sis ex quae peperit iacentes, grates rogat quae senserat nec
nec verba harenas inplent. Per dum necis in in versus quin loquendi latens;
inde. **Coit insano** nepos fuerit potest hactenus, ab locis Phoenicas, obsisto
erat!

> Nec uterum Aurorae petentes abstulit. Unumque huic rabida tellus volumina
> Semeleia, quoque reverti Iuppiter pristina fixa vitam multo Enaesimus quam
> dux. Sua **damus** decipere, ut **obortas** nomen sine vestrae vita.

Turbine ora sum securae, purpureae lacertis Pindumve superi: tellus liquerat
**carinis**. Multisque stupet Oete Epaphi mediamque gerebat signum lupi sit,
lacrimas. Tumidi fassusque hosti, deus [vixque desint
dedit](http://hisnurus.com/putares-pars) dum et, quo non, dea [suras
tantum](http://mactata.org/inducere.php). Unus acta capulo. In Dryope sic
vestigia est neu ignis in **illa mirantur agilis** densior.
EOF
	# }}}
	# gridsome.config.js {{{
	file=gridsome.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
const url = "https://spiritwalk.netlify.com";
const site = "icew4ll";
const desc = "starterdamus";
module.exports = {
  siteName: site,
  siteDescription: desc,
  siteUrl: url,
  titleTemplate: `%s | ` + site,

  templates: {
    Blog: [
      {
        path: "/:title"
      }
    ],
    CustomPage: [
      {
        path: "/:title",
        component: "~/templates/CustomPage.vue"
      }
    ],
    Category: [
      {
        path: "/category/:title",
        component: "~/templates/Category.vue"
      }
    ],
    Author: [
      {
        path: "/author/:name",
        component: "~/templates/Author.vue"
      }
    ],
    Tag: [
      {
        path: "/tags/:title",
        component: "~/templates/Tag.vue"
      }
    ]
  },

  plugins: [
    "gridsome-plugin-robots",
    {
      use: "gridsome-plugin-pwa",
      options: {
        title: "icew4ll Starter",
        description: "icew4ll starter", // Optional
        startUrl: "/",
        display: "standalone",
        gcm_sender_id: undefined,
        statusBarStyle: "default",
        manifestPath: "manifest.json",
        disableServiceWorker: false,
        serviceWorkerPath: "service-worker.js",
        cachedFileTypes: "js,json,css,html,png,jpg,jpeg,svg",
        shortName: "icew4ll starter",
        themeColor: "#000000",
        lang: "en-US",
        backgroundColor: "#000000",
        icon: "./src/favicon.png", // must be provided like 'src/favicon.png'
        msTileColor: "#000000"
      }
    },
    {
      use: "gridsome-source-static-meta",
      options: {
        path: "content/site/*.json"
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Author",
        path: "./content/author/*.md"
      }
    },
    {
      // Create posts from markdown files
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Blog",
        path: "content/blog/**/*.md",
        refs: {
          // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
          author: "Author",
          tags: {
            typeName: "Tag",
            create: true
          },
          category: {
            typeName: "Category",
            create: true
          }
        }
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "CustomPage",
        path: "./content/pages/*.md"
      }
    },
    {
      use: "gridsome-plugin-tailwindcss",
      options: {
        tailwindConfig: "./tailwind.config.js",
        purgeConfig: {
          whitelist: [
            "svg-inline--fa",
            "table",
            "table-striped",
            "table-bordered",
            "table-hover",
            "table-sm"
          ],
          whitelistPatterns: [
            /fa-$/,
            /blockquote$/,
            /code$/,
            /pre$/,
            /table$/,
            /table-$/,
            /vueperslide$/,
            /vueperslide-$/
          ]
        },
        presetEnvConfig: {},
        shouldPurge: false,
        shouldImport: true,
        shouldTimeTravel: true,
        shouldPurgeUnusedKeyframes: true
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        cacheTime: 600000 // default
      }
    },
    {
      use: "gridsome-plugin-rss",
      options: {
        contentTypeName: "Blog",
        feedOptions: {
          title: desc,
          feed_url: url + "/rss.xml",
          site_url: url
        },
        feedItemOptions: node => ({
          title: node.title,
          excerpt: node.excerpt,
          url: url + node.path,
          author: node.author,
          created: node.created
        }),
        output: {
          dir: "./static",
          name: "rss.xml"
        }
      }
    },
    {
      use: "gridsome-plugin-flexsearch",
      options: {
        searchFields: ["title", "category", "excerpt", "content", "tags"],
        collections: [
          {
            typeName: "Blog",
            indexName: "Blog",
            fields: ["title", "category", "excerpt", "content", "tags"]
          }
        ]
      }
    }
  ],
  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: "_blank",
      externalLinksRel: ["nofollow", "noopener", "noreferrer"],
      anchorClassName: "icon icon-link",
      plugins: [
        "remark-autolink-headings",
        "remark-attr",
        [
          "gridsome-plugin-remark-prismjs-all",
          {
            noInlineHighlight: false,
            showLineNumbers: false
          }
        ]
      ]
    }
  },
  chainWebpack: config => {
    config.resolve.alias.set("@pageImage", "@/assets/images");
  }
};
EOF
	# }}}
	# tailwind.config.js {{{
	file=tailwind.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
var _ = require("lodash");
var flattenColorPalette = require("tailwindcss/lib/util/flattenColorPalette")
  .default;

module.exports = {
  purge: {
    content: ["./src/**/*.html", "./src/**/*.vue", "./src/**/*.jsx"],
    options: {
      whitelist: [
        "bg-opacity-0",
        "bg-opacity-25",
        "bg-opacity-50",
        "bg-opacity-75",
        "bg-opacity-100",
        "mode-dark"
      ]
    }
  },
  theme: {
    extend: {
      colors: {
        light3: "#fdf6e3",
        light2: "#eee8d5",
        light1: "#ccccb3",
        light0: "#073642",
        dark3: "#3e3519",
        dark2: "#403005",
        dark1: "#4d4d00",
        dark0: "#d1cdc7"
      },
      height: {
        "96": "24rem",
        "128": "32rem",
        "half-screen": "50vh"
      },
      backgroundOpacity: {
        "0": "0",
        "10": "0.1",
        "20": "0.2",
        "30": "0.3",
        "40": "0.4",
        "50": "0.5",
        "60": "0.6",
        "70": "0.7",
        "80": "0.8",
        "90": "0.9",
        "100": "1"
      }
    },
    fontFamily: {
      sans: [
        '"Source Sans Pro"',
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: ["Georgia", "Cambria", '"Times New Roman"', "Times", "serif"],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    },
    zIndex: {
      "-10": "-10",
      "0": 0,
      "10": 10,
      "20": 20,
      "30": 30,
      "40": 40,
      "50": 50,
      "25": 25,
      "50": 50,
      "75": 75,
      "100": 100,
      "1000": 1000,
      auto: "auto"
    },
    boxShadow: {
      default: "0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)",
      md: "0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)",
      lg:
        "0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)",
      xl:
        "0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)",
      "2xl": "0 25px 50px -12px rgba(0, 0, 0, .25)",
      "2xl-strong": "0 25px 50px -12px rgba(0, 0, 0, .5)",
      "3xl": "0 35px 60px -15px rgba(0, 0, 0, .3)",
      inner: "inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)",
      outline: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      focus: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      none: "none"
    }
  },
  variants: {
    backgroundColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    textColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    borderColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ]
  },
  plugins: [
    require("tailwindcss-dark-mode")(),
    function({ addUtilities, e, theme, variants }) {
      const colors = flattenColorPalette(theme("borderColor"));

      const utilities = _.flatMap(
        _.omit(colors, "default"),
        (value, modifier) => ({
          [`.${e(`border-t-${modifier}`)}`]: {
            borderTopColor: `${value}`
          },
          [`.${e(`border-r-${modifier}`)}`]: {
            borderRightColor: `${value}`
          },
          [`.${e(`border-b-${modifier}`)}`]: {
            borderBottomColor: `${value}`
          },
          [`.${e(`border-l-${modifier}`)}`]: {
            borderLeftColor: `${value}`
          }
        })
      );

      addUtilities(utilities, variants("borderColor"));
    }
  ],
  corePlugins: {
    container: false
  }
};
EOF
	# }}}
	# navigation.json {{{
	file=content/site/navigation.json
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
{
  "headerNavigation": [
    {
      "name": "Home",
      "link": "/",
      "external": false
    },
    {
      "name": "About",
      "link": "/about-us",
      "external": false
    },
    {
      "name": "More",
      "link": "#",
      "children": [
        {
          "name": "Theme Repository",
          "link": "https://www.github.com/noxify/gridsome-starter-liebling",
          "external": true
        },
        {
          "name": "Gridsome Docs",
          "link": "https://www.gridsome.org/docs",
          "external": true
        },
        {
          "name": "Internal Page ( About )",
          "link": "/about-us",
          "external": false
        }
      ]
    }
  ],
  "footerNavigation": [
    {
      "name": "Latest Posts",
      "link": "/",
      "external": false
    },
    {
      "name": "Facebook",
      "link": "https://www.facebook.com",
      "external": true
    },
    {
      "name": "Gridsome",
      "link": "https://www.gridsome.org",
      "external": true
    },
    {
      "name": "RSS",
      "link": "/rss.xml",
      "external": true
    }
  ]
}
EOF
	# }}}
	# social.json {{{
	file=content/site/social.json
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
{
  "social": [
    {
      "name": "Facebook",
      "link": "#",
      "icon": "facebook-square"
    },
    {
      "name": "Instagram",
      "link": "#",
      "icon": "instagram"
    },
    {
      "name": "Github",
      "link": "#",
      "icon": "github"
    }
  ]
}
EOF
	# }}}
}        #}}}
file() { #{{{
	# Index.html {{{
	file=src/index.html
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<!DOCTYPE html>
<html ${htmlAttrs}>
  <head>
    ${head}
  </head>
  <body ${bodyAttrs}>
    <script>
      // Add dark / light detection that runs before Vue.js load. Borrowed from overreacted.io
      // for this starter, i used the code from gridsome.org
      (function() {
        window.__onThemeChange = function() {};
        function setTheme(newTheme) {
          window.__theme = newTheme;
          preferredTheme = newTheme;
          document.body.setAttribute("data-theme", newTheme);
          document.documentElement.classList.remove("mode-light");
          document.documentElement.classList.remove("mode-dark");
          document.documentElement.classList.add("mode-" + newTheme);

          window.__onThemeChange(newTheme);
        }

        var preferredTheme;
        try {
          preferredTheme = localStorage.getItem("theme");
        } catch (err) {}

        window.__setPreferredTheme = function(newTheme) {
          setTheme(newTheme);
          try {
            localStorage.setItem("theme", newTheme);
          } catch (err) {}
        };

        var darkQuery = window.matchMedia("(prefers-color-scheme: dark)");

        darkQuery.addListener(function(e) {
          window.__setPreferredTheme(e.matches ? "dark" : "light");
        });

        setTheme(preferredTheme || (darkQuery.matches ? "dark" : "light"));
      })();
    </script>

    ${app} ${scripts}
  </body>
</html>
EOF
	# }}}
	# Layouts:
	# Default.vue {{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div id="app" class="bg-light3 dark:bg-dark3">
    <navbar @setTheme="setTheme" :theme="this.theme"></navbar>
    <slot />
    <v-footer></v-footer>
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<script>
import Navbar from "~/components/Navbar/Navbar.vue";
import VFooter from "~/components/Partials/Footer.vue";

export default {
  data: function() {
    return {
      theme: "light"
    };
  },
  components: {
    Navbar,
    VFooter
  },
  methods: {
    setTheme(mode) {
      this.theme = mode;
    }
  }
};
</script>
EOF
	# }}}
	# Pages:
	# Index.vue {{{
	file=src/pages/Index.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <div class="container mx-auto">
      <FeaturedCard
        v-if="$page.featured.totalCount > 0"
        :records="$page.featured.edges"
      />
      <transition-group name="fade" class="flex flex-wrap my-4" tag="div">
        <CardItem
          v-for="{ node } of loadedPosts"
          :key="node.id"
          :record="node"
        />
      </transition-group>
      <ClientOnly>
        <infinite-loading @infinite="infiniteHandler" spinner="spiral">
          <div slot="no-more"></div>
          <div slot="no-results"></div>
        </infinite-loading>
      </ClientOnly>
    </div>
  </Layout>
</template>

<page-query>
  query($page: Int) {
    featured: allBlog(limit: 4, filter: { featured: { eq: true } }, sortBy:"created") {
      totalCount
      edges {
        node {
          id
          title
          image(width: 800)
          path
          timeToRead
          humanTime: created(format: "DD MMM YYYY")
          datetime: created
          category {
            id
            title
            path
          }
          author {
            id
            name
            image(width: 64, height: 64, fit: inside)
            path
          }
        }
      }
    }
    entries: allBlog(perPage: 6, page: $page, sortBy:"created") @paginate {
      totalCount
      pageInfo {
        totalPages
        currentPage
      }
      edges {
        node {
          id
          title
          image(width: 800)
          path
          timeToRead
          featured
          humanTime: created(format: "DD MMM YYYY")
          datetime: created
          category {
            id
            title
            path
          }
          author {
            id
            name
            image(width: 64, height: 64, fit: inside)
            path
          }
        }
      }
    }
  }
</page-query>

<static-query>
query {
  metadata {
    siteName
    siteDescription
  }
}
</static-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";
import FeaturedCard from "~/components/Content/FeaturedCard.vue";

export default {
  metaInfo: {
    title: "Hello, world!"
  },
  components: {
    CardItem,
    FeaturedCard,
    ContentHeader
  },
  data() {
    return {
      loadedPosts: [],
      currentPage: 1
    };
  },
  created() {
    this.loadedPosts.push(...this.$page.entries.edges);
  },
  methods: {
    async infiniteHandler($state) {
      if (this.currentPage + 1 > this.$page.entries.pageInfo.totalPages) {
        $state.complete();
      } else {
        const { data } = await this.$fetch(`/${this.currentPage + 1}`);
        if (data.entries.edges.length) {
          this.currentPage = data.entries.pageInfo.currentPage;
          this.loadedPosts.push(...data.entries.edges);
          $state.loaded();
        } else {
          $state.complete();
        }
      }
    }
  }
};
</script>

<style scoped>
.fade-enter-active,
.fade-leave-active {
  transition: ease opacity 0.3s;
}
.fade-enter,
.fade-leave-to {
  opacity: 0;
}
</style>
EOF
	# }}}
	# Components:
	# NavbarSubNavigation.vue {{{
	file=src/components/Navbar/NavbarSubNavigation.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="container mx-auto">
    <div class="flex flex-wrap md:my-4 md:mx-4">
      <div class="w-full mb-2">
        <h2 class="text-xl mt-0 mb-2">Recent articles</h2>

        <div class="w-full">
          <VueSlickCarousel
            :arrows="true"
            :dots="false"
            class="-mx-4"
            v-bind="sliderSettings"
          >
            <div
              v-for="edge in $static.recent.edges"
              :key="edge.node.id"
              class="px-4 dark-hover:bg-dark1 hover:bg-light1 rounded-md"
            >
              <g-link :to="edge.node.path">
                <div :id="edge.node.id" class>
                  <g-image
                    :src="edge.node.image"
                    :alt="edge.node.title"
                    class="rounded-lg h-32 object-cover w-full"
                  ></g-image>

                  <div class="post-card-content">
                    <h3
                      class="tracking-wider mt-3 mb-3 text-lg font-light max-w-xl"
                    >
                      {{ edge.node.title }}
                    </h3>
                  </div>

                  <div class="post-card-footer">
                    <p class="text-xs">
                      <time :datetime="edge.node.datetime">{{
                        edge.node.humanTime
                      }}</time>
                      &nbsp;&bull;&nbsp;
                      {{ edge.node.timeToRead }} min read
                    </p>
                  </div>
                </div>
              </g-link>
            </div>
          </VueSlickCarousel>
        </div>
      </div>
      <div class="w-full">
        <h2 class="text-xl mt-2 mb-2">Tags</h2>

        <section class="post-tags container mx-auto relative py-4">
          <g-link
            v-for="tag in $static.tags.edges"
            :key="tag.node.id"
            :to="tag.node.path"
            class="text-xs bg-transparent dark-hover:bg-dark1 hover:bg-light1 py-2 px-4 mr-2 border border-light0 dark:border-dark0 text-light0 dark:text-dark0 rounded-full"
            >{{ tag.node.title }}</g-link
          >
        </section>
      </div>
    </div>
  </div>
</template>

<script>
import VueSlickCarousel from "vue-slick-carousel";

export default {
  components: {
    VueSlickCarousel
  },
  data() {
    return {
      sliderSettings: {
        dots: false,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              arrows: false,
              slidesToShow: 3,
              slidesToScroll: 1,
              dots: false,
              infinite: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              arrows: false,
              slidesToShow: 2,
              slidesToScroll: 1,
              initialSlide: 2,
              dots: false,
              infinite: true
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
              infinite: true
            }
          }
        ]
      }
    };
  }
};
</script>

<style></style>

<static-query>

query {
  tags: allTag {
    edges {
      node {
        title
        path
      }
    }
  },
  recent : allBlog(limit: 4, sort: { by: "created", order: DESC }) {
    edges {
      node {
        id
        title
        path
        image(width:230, height:130)
        humanTime: created(format: "DD MMM YYYY")
        datetime: created
        timeToRead
      }
    }
  }
}


</static-query>
EOF
	# }}}
	# SearchButton.vue {{{
	file=src/components/Navbar/SearchButton.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <a
    role="button"
    @click.prevent="openSearchModal()"
    aria-label="Open Search"
    title="Open Search"
  >
    <search-icon size="1.5x" class="custom-class"></search-icon>
  </a>
</template>

<script>
import { SearchIcon } from "vue-feather-icons";

export default {
  components: {
    SearchIcon
  },
  methods: {
    openSearchModal() {
      this.$emit("openSearchModal");
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# Modal.vue {{{
	file=src/components/Modal/Modal.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Transition name="fade">
    <div
      v-if="showModal"
      class="fixed inset-0 w-full h-full overflow-y-auto overflow-x-hidden flex items-center justify-center dark:bg-dark2 bg-light2 z-1000"
      @click.self="close"
    >
      <div class="relative w-full h-full p-8">
        <button
          aria-label="close"
          class="absolute mr-8 right-0 text-xl border rounded-full dark:border-dark1 border-light1 text-dark3 dark:text-light3 hover:bg-light1 dark-hover:bg-dark1 hover:text-dark3 dark-hover:text-light3 h-8 w-8 md:h-12 md:w-12 focus:outline-none"
          @click.prevent="close"
        >
          <x-icon size="1.5x" class="md:mx-2"></x-icon>
        </button>
        <div class="mt-12 md:mt-16 overflow-y-auto">
          <slot />
        </div>
      </div>
    </div>
  </Transition>
</template>

<script>
import { XIcon } from "vue-feather-icons";

export default {
  components: {
    XIcon
  },
  props: {
    showModal: {
      required: true,
      type: Boolean
    }
  },
  methods: {
    close() {
      this.$emit("close");
    }
  }
};
</script>
EOF
	# }}}
	# NavbarMobileModal.vue {{{
	file=src/components/Modal/NavbarMobileModal.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div class="w-full mb-2 overflow-x-hidden text-light0 dark:text-dark0">
      <h2 class="text-xl my-0">Navigation</h2>
      <div class="menu-links">
        <ul>
          <li
            v-for="navItem in $static.metadata.headerNavigation"
            :key="navItem.name"
            class="py-1"
          >
            <g-link
              class="hover:bg-light1 dark-hover:bg-dark1 rounded-sm block py-1"
              :to="navItem.link"
              :title="navItem.name"
              v-if="navItem.external != true && navItem.children.length <= 0"
              >{{ navItem.name }}</g-link
            >
            <a
              class="block"
              :href="navItem.link"
              target="_blank"
              :title="navItem.name"
              v-if="navItem.external == true && navItem.children.length <= 0"
              >{{ navItem.name }}</a
            >
            <ClientOnly>
              <v-popover
                placement="right"
                popoverClass="mobile-navbar-popover"
                offset="16"
                v-if="navItem.children.length > 0"
              >
                <a
                  class="hover:bg-light1 dark-hover:bg-dark1 rounded-sm block flex py-1"
                  style="cursor:pointer;"
                >
                  {{ navItem.name }}
                  <chevron-right-icon
                    size="1.5x"
                    class="custom-class"
                  ></chevron-right-icon>
                </a>

                <template slot="popover">
                  <ul>
                    <li
                      v-for="subItem in navItem.children"
                      :key="subItem.name"
                      class="hover:bg-dark1 px-4 py-2 hover:text-white"
                    >
                      <g-link
                        class="block"
                        :to="subItem.link"
                        :title="subItem.name"
                        v-if="subItem.external != true"
                        >{{ subItem.name }}</g-link
                      >
                      <a
                        class="block"
                        :href="subItem.link"
                        target="_blank"
                        :title="subItem.name"
                        v-if="subItem.external == true"
                        >{{ subItem.name }}</a
                      >
                    </li>
                  </ul>
                </template>
              </v-popover>
            </ClientOnly>
          </li>
        </ul>
      </div>
    </div>
    <div class="mobileSubnav pl-2 text-light0 dark:text-dark0">
      <subnavigation />
    </div>
  </div>
</template>

<static-query>
query {
   metadata {
    siteName
    headerNavigation {
      name
      link
      external
      children {
        name
        link
        external
      }
    }
  }
}
</static-query>
<script>
import { ChevronRightIcon } from "vue-feather-icons";
import Subnavigation from "~/components/Navbar/NavbarSubNavigation.vue";

export default {
  components: {
    Subnavigation,
    ChevronRightIcon
  }
};
</script>

<style></style>
EOF
	# }}}
	# SearchModal.vue {{{
	file=src/components/Modal/SearchModal.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div class="flex justify-center">
      <div class="search-results w-full">
        <div class="container px-5 mx-auto">
          <div class="flex flex-wrap items-stretch w-full mb-4 relative">
            <div class="flex">
              <span
                class="flex items-center leading-normal rounded rounded-r-none border border-r-0 border-gray-500 px-3 whitespace-no-wrap text-gray-400 dark:bg-gray-900 dark:text-gray-700 dark:border-gray-700"
              >
                <search-icon size="1.5x" class="custom-class"></search-icon>
              </span>
            </div>
            <input
              type="text"
              class="flex-shrink flex-grow flex-auto text-gray-700 dark:text-gray-600 leading-normal w-px flex-1 border h-12 text-xl md:h-16 md:text-3xl border-l-0 focus:outline-none focus:shadow-none border-gray-500 bg-light3 dark:bg-dark3 dark:border-gray-700 rounded rounded-l-none px-3 relative"
              placeholder="Search..."
              id="search"
              v-model="searchTerm"
            />
          </div>
          <div class="flex flex-wrap">
            <div
              class="py-2 px-2 sm:w-2/4 md:w-1/2 lg:w-1/3"
              v-for="resultEntry in searchResults"
              :key="resultEntry.id"
            >
              <g-link :to="resultEntry.path">
                <div
                  class="h-full flex items-start dark-hover:bg-dark1 hover:bg-light1 bg-light3 dark:bg-dark3 rounded-lg"
                >
                  <div class="flex-grow px-6">
                    <h2
                      class="tracking-widest text-xs title-font font-medium text-indigo-500 mb-1"
                    >
                      {{ resultEntry.node.category }}
                    </h2>
                    <h1
                      class="title-font text-xl font-medium text-gray-900 dark:text-gray-400 mb-3"
                    >
                      {{ resultEntry.title }}
                    </h1>
                    <p class="leading-relaxed mb-5 dark:text-gray-500">
                      {{ resultEntry.node.excerpt }}
                    </p>
                    <ul class="flex mb-5">
                      <li
                        v-for="item in resultEntry.node.tags"
                        class="leading-relaxed mx-2 text-indigo-500"
                      >
                        {{ item }}
                      </li>
                    </ul>
                  </div>
                </div>
              </g-link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
import { SearchIcon } from "vue-feather-icons";

export default {
  components: {
    SearchIcon
  },
  data: () => ({
    searchTerm: ""
  }),
  computed: {
    searchResults() {
      const searchTerm = this.searchTerm;
      if (searchTerm.length < 3) return [];
      return this.$search.search({ query: searchTerm });
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# ThemeSwitcher.vue {{{
	file=src/components/Navbar/ThemeSwitcher.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <a
    role="button"
    @click.prevent="toggleTheme()"
    :aria-label="'Toggle ' + nextTheme"
    :title="'Toggle ' + nextTheme"
    class="toggle-theme"
  >
    <sun-icon
      size="1.5x"
      class="custom-class"
      v-if="theme === 'light'"
    ></sun-icon>
    <moon-icon
      size="1.5x"
      class="custom-class"
      v-if="theme === 'dark'"
    ></moon-icon>
  </a>
</template>

<script>
import { SunIcon, MoonIcon } from "vue-feather-icons";
let themes = ["light", "dark"];

export default {
  components: {
    SunIcon,
    MoonIcon
  },
  props: {
    theme: {
      type: String
    }
  },

  computed: {
    nextTheme() {
      const currentIndex = themes.indexOf(this.theme);
      const nextIndex = (currentIndex + 1) % themes.length;
      return themes[nextIndex];
    }
  },
  methods: {
    toggleTheme() {
      const currentIndex = themes.indexOf(this.theme);
      const nextIndex = (currentIndex + 1) % themes.length;
      window.__setPreferredTheme(themes[nextIndex]);

      this.$emit("setTheme", themes[nextIndex]);
    }
  },
  async mounted() {
    // set default
    if (typeof window.__theme !== "undefined")
      this.$emit("setTheme", window.__theme);
  }
};
</script>
EOF
	# }}}
	# NavbarDesktop.vue {{{
	file=src/components/Navbar/NavbarDesktop.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <nav
    class="hidden md:block lg:block xl:block flex items-center justify-between flex-wrap container mx-auto py-3 z-20 text-light0 dark:text-dark0"
  >
    <div class="block flex-grow flex items-center w-auto mx-4">
      <div
        class="hover:bg-light1 dark-hover:bg-dark1 rounded-sm flex items-center flex-shrink-0 mr-6"
      >
        <g-link class="font-semibold text-xl tracking-tight" to="/">{{
          $static.metadata.siteName
        }}</g-link>
      </div>

      <div class="flex-grow">
        <ul class="list-none flex justify-left">
          <li
            v-for="navItem in $static.metadata.headerNavigation"
            :key="navItem.name"
            class="hover:bg-light1 dark-hover:bg-dark1 rounded-sm px-4 py-1"
          >
            <g-link
              class="rounded-sm block py-1"
              :to="navItem.link"
              :title="navItem.name"
              v-if="navItem.external != true && navItem.children.length <= 0"
              >{{ navItem.name }}</g-link
            >
            <a
              class="block"
              :href="navItem.link"
              target="_blank"
              :title="navItem.name"
              v-if="navItem.external == true && navItem.children.length <= 0"
              >{{ navItem.name }}</a
            >
            <ClientOnly>
              <v-popover
                placement="top"
                popoverClass="navbar-popover"
                offset="16"
                v-if="navItem.children.length > 0"
              >
                <a
                  class="hover:bg-light1 dark-hover:bg-dark1 rounded-sm block py-1"
                  style="cursor:pointer;"
                >
                  <div class="container mx-auto">
                    <div class="flex">
                      {{ navItem.name }}
                      <chevron-down-icon
                        size="1.5x"
                        class="custom-class"
                      ></chevron-down-icon>
                    </div>
                  </div>
                </a>

                <template slot="popover">
                  <ul>
                    <li
                      v-for="subItem in navItem.children"
                      :key="subItem.name"
                      class="hover:bg-dark1 px-4 py-2 submenu-item hover:text-white"
                    >
                      <g-link
                        class="rounded-sm block"
                        :to="subItem.link"
                        :title="subItem.name"
                        v-if="subItem.external != true"
                        >{{ subItem.name }}</g-link
                      >
                      <a
                        class="block"
                        :href="subItem.link"
                        target="_blank"
                        :title="subItem.name"
                        v-if="subItem.external == true"
                        >{{ subItem.name }}</a
                      >
                    </li>
                  </ul>
                </template>
              </v-popover>
            </ClientOnly>
          </li>
          <li class="hover:bg-light1 dark-hover:bg-dark1 rounded-sm px-4 py-1">
            <a
              role="button"
              @click.prevent="toggleSubNavigation()"
              class="block px-4 py-1"
              aria-label="Open Subnavigation"
              title="Open Subnavigation"
              v-bind:class="{
                'text-blue-600': showSubNavigation,
                '': !showSubNavigation
              }"
            >
              <more-horizontal-icon
                size="1.5x"
                class="custom-class"
              ></more-horizontal-icon>
            </a>

            <div
              v-click-outside="onClickOutside"
              class="py-4 mega-menu mb-16 border-t border-gray-200 shadow-xl bg-light3 dark:bg-dark3 dark:border-gray-900"
              v-bind:class="{
                '': showSubNavigation,
                hidden: !showSubNavigation
              }"
            >
              <div>
                <subnavigation />
              </div>
            </div>
          </li>
        </ul>
      </div>

      <div class="inline-block">
        <ul class="list-none flex justify-center md:justify-end">
          <li class="hover:bg-light1 dark-hover:bg-dark1 rounded-full mr-6">
            <search-button v-on="$listeners"></search-button>
          </li>
          <li class="hover:bg-light1 dark-hover:bg-dark1 rounded-full">
            <theme-switcher v-on="$listeners" :theme="theme" />
          </li>
        </ul>
      </div>
    </div>
  </nav>
</template>

<script>
import { ChevronDownIcon, MoreHorizontalIcon } from "vue-feather-icons";
import ThemeSwitcher from "~/components/Navbar/ThemeSwitcher.vue";
import Subnavigation from "~/components/Navbar/NavbarSubNavigation.vue";
import SearchButton from "~/components/Navbar/SearchButton.vue";

export default {
  components: {
    MoreHorizontalIcon,
    ChevronDownIcon,
    ThemeSwitcher,
    SearchButton,
    Subnavigation
  },
  data: function() {
    return {
      showSubNavigation: false,
      vcoConfig: {
        events: ["dblclick", "click"],
        isActive: true
      }
    };
  },
  props: {
    theme: {
      type: String
    },
    hideSubnav: {
      type: Boolean
    }
  },
  methods: {
    toggleSubNavigation() {
      this.showSubNavigation = !this.showSubNavigation;
    },
    onClickOutside(event) {
      if (!event.defaultPrevented && this.showSubNavigation == true) {
        this.toggleSubNavigation();
      }
    },
    hideSubNavigation() {
      this.showSubNavigation = false;
    }
  },
  watch: {
    hideSubnav() {
      if (this.hideSubnav) {
        this.hideSubNavigation();
      }
    },
    $route(to, from) {
      this.hideSubNavigation();
    }
  }
};
</script>

<static-query>
query {
  metadata {
    siteName
    headerNavigation {
      name
      link
      external
      children {
        name
        link
        external
      }
    }
  }
}
</static-query>
EOF
	# }}}
	# NavbarMobile.vue {{{
	file=src/components/Navbar/NavbarMobile.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <nav
    class="block md:hidden flex items-center justify-between flex-wrap container mx-auto py-4 text-light0 dark:text-dark0"
  >
    <div class="block flex-grow flex items-center w-auto mx-4">
      <div
        class="hover:bg-light1 dark-hover:bg-dark1 flex items-center flex-shrink-0 mr-6"
      >
        <a
          role="button"
          @click.prevent="openNavbarModal()"
          aria-label="Open Navigation"
          title="Open Navigation"
        >
          <menu-icon size="1.5x" class="custom-class"></menu-icon>
        </a>
      </div>
      <div class="flex-grow text-center font-bold text-lg">
        <g-link
          class="hover:bg-light1 dark-hover:bg-dark1 rounded-sm font-semibold text-xl tracking-tight"
          to="/"
          >{{ $static.metadata.siteName }}</g-link
        >
      </div>

      <div class="inline-block">
        <ul class="list-none flex justify-center md:justify-end">
          <li class="hover:bg-light1 dark-hover:bg-dark1 rounded-full mr-6">
            <search-button v-on="$listeners"></search-button>
          </li>
          <li class="hover:bg-light1 dark-hover:bg-dark1 rounded-full">
            <theme-switcher v-on="$listeners" :theme="theme" />
          </li>
        </ul>
      </div>
    </div>
  </nav>
</template>

<script>
import ThemeSwitcher from "~/components/Navbar/ThemeSwitcher.vue";
import SearchButton from "~/components/Navbar/SearchButton.vue";
import { MenuIcon } from "vue-feather-icons";

export default {
  components: {
    MenuIcon,
    SearchButton,
    ThemeSwitcher
  },
  props: {
    theme: {
      type: String
    },
    showNavigation: {
      type: Boolean
    }
  },
  methods: {
    openNavbarModal() {
      this.$emit("openNavbarModal");
    }
  }
};
</script>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>
EOF
	# }}}
	# Navbar.vue {{{
	file=src/components/Navbar/Navbar.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="h-16">
    <headroom
      :classes="{
        initial:
          'headroom bg-light2 dark:bg-dark2 border-b dark:border-gray-900'
      }"
      :downTolerance="10"
      :upTolerance="10"
      :offset="15"
      @unpin="navbarUnpinned = true"
      @pin="navbarUnpinned = false"
    >
      <navbar-desktop
        v-on="$listeners"
        @openSearchModal="openSearchModal"
        :theme="theme"
        :hideSubnav="this.navbarUnpinned"
      />

      <navbar-mobile
        @openSearchModal="openSearchModal"
        @openNavbarModal="openNavbarModal"
        v-on="$listeners"
        :theme="theme"
      />
    </headroom>

    <modal :showModal="this.showSearchModal" @close="closeSearchModal">
      <search-modal></search-modal>
    </modal>

    <modal :showModal="this.showNavbarModal" @close="closeNavbarModal">
      <navbar-modal></navbar-modal>
    </modal>
  </div>
</template>

<script>
import NavbarDesktop from "~/components/Navbar/NavbarDesktop.vue";
import NavbarMobile from "~/components/Navbar/NavbarMobile.vue";
import Modal from "~/components/Modal/Modal.vue";
import SearchModal from "~/components/Modal/SearchModal.vue";
import NavbarModal from "~/components/Modal/NavbarMobileModal.vue";
import { headroom } from "vue-headroom";

export default {
  props: {
    theme: {
      type: String
    }
  },
  data: function() {
    return {
      showSearchModal: false,
      showNavbarModal: false,
      headerHeight: 100,
      navbarUnpinned: false
    };
  },
  components: {
    NavbarDesktop,
    NavbarMobile,
    Modal,
    SearchModal,
    NavbarModal,
    headroom
  },
  methods: {
    openSearchModal() {
      this.showSearchModal = true;
    },
    closeSearchModal() {
      this.showSearchModal = false;
    },
    openNavbarModal() {
      this.showNavbarModal = true;
    },
    closeNavbarModal() {
      this.showNavbarModal = false;
    }
  },
  watch: {
    $route(to, from) {
      this.closeNavbarModal();
      this.closeSearchModal();
    }
  }
};
</script>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>
EOF
	# }}}
	# ContentHeader.vue {{{
	file=src/components/Partials/ContentHeader.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="mb-4">
    <div
      class="z-100 text-center bg-gray-200 dark:bg-gray-900 lg:py-16 md:py-12 sm:py-10 py-8"
      v-if="!hasImage"
    >
      <h2 v-if="title != null" class="h1 font-extrabold dark:text-gray-400">
        {{ title }}
      </h2>
      <p v-if="sub != null" class="text-gray-600 text-light font-sans">
        {{ sub }}
      </p>
    </div>

    <div v-if="hasImage" class="z-100 relative mt-0 h-auto">
      <g-image
        v-if="hasImage && staticImage"
        :src="require(`!!assets-loader!@pageImage/${image}`)"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
        alt="Cover Image"
      ></g-image>

      <g-image
        v-if="hasImage && !staticImage"
        :src="image"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
        alt="Cover Image"
      ></g-image>

      <slot>
        <div
          class="text-center text-dark0 bg-gray-800 lg:py-48 md:py-32 py-24"
          :class="`bg-opacity-${opacity}`"
        >
          <h2 v-if="title != null" class="h1 font-extrabold">{{ title }}</h2>
          <p v-if="sub != null" class="h5 font-sans">{{ sub }}</p>
        </div>
      </slot>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    title: {
      type: String,
      default: null
    },
    sub: {
      type: String,
      default: null
    },
    image: {
      type: String | Object,
      default: null
    },
    staticImage: {
      type: Boolean,
      default: true
    },
    opacity: {
      type: Number,
      default: 0
    }
  },
  computed: {
    hasImage() {
      return this.image ? true : false;
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# CardItem.vue {{{
	file=src/components/Content/CardItem.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="w-full md:w-1/2 lg:w-1/3 px-2 mb-2">
    <div
      class="mb-1 post-card border-light1 dark:border-dark1 rounded-lg hover:shadow-xl z-100"
      :id="record.id"
    >
      <g-link :to="record.path" class="post-card-image-link">
        <div
          v-if="record.featured"
          class="absolute top-0 right-0 pr-4 pt-4 z-10"
        >
          <span
            class="p-1 relative block text-center text-xs leading-tight bg-dark2 border border-dark0 text-dark0 rounded-full"
          >
            <star-icon size="1.5x" class="custom-class"></star-icon>
          </span>
        </div>
        <g-image
          :src="record.image"
          :alt="record.title"
          class="post-card-image"
        ></g-image>
      </g-link>
      <div
        class="post-card-content bg-light2 dark:bg-dark2 h-full rounded-b-lg"
      >
        <g-link
          :to="record.path"
          class="flex-col relative flex justify-between rounded-b-lg px-6 h-40 text-light0 dark:text-dark0"
        >
          <h3 class="post-card-title tracking-wide">{{ record.title }}</h3>

          <div class="text-sm leading-none absolute bottom-0 pb-6">
            <ul>
              <li>{{ record.tags.title }}</li>
            </ul>
            <p>
              <g-link :to="record.category.path" class="">{{
                record.category.title
              }}</g-link>
              &nbsp;&bull;&nbsp;
              <time :datetime="record.datetime">{{ record.humanTime }}</time>
              &nbsp;&bull;&nbsp;
              {{ record.timeToRead }} min read &nbsp;&bull;&nbsp;
              {{ record.tags.title }}
            </p>
          </div>
        </g-link>
      </div>
    </div>
  </div>
</template>

<script>
import { StarIcon } from "vue-feather-icons";

export default {
  components: {
    StarIcon
  },
  props: {
    record: {}
  },
  computed: {
    authors() {
      let tooltipText = [];
      for (let index = 0; index < this.record.author.length; index++) {
        if (index == 0) {
          tooltipText.push(`Posted by ${this.record.author[index].name}`);
        } else {
          if (index == 1) {
            tooltipText.push(
              `<br> Among with ${this.record.author[index].name}`
            );
          } else {
            tooltipText.push(`, ${this.record.author[index].name}`);
          }
        }
      }

      return tooltipText.join("");
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# FeaturedCard.vue {{{
	file=src/components/Content/FeaturedCard.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="w-full px-2 my-4 items-stretch">
    <div
      class="featured-post-card rounded-lg shadow-xl z-100 h-64 md:h-96 lg:h-128"
    >
      <VueSlickCarousel
        :arrows="true"
        :autoplay="true"
        class="h-full rounded-lg"
      >
        <div
          class="h-full relative"
          v-for="edge in records"
          :key="edge.node.id"
        >
          <g-link :to="edge.node.path">
            <div class="h-full relative" :id="edge.node.id">
              <g-image
                :src="edge.node.image"
                :alt="edge.node.title"
                class="rounded-lg object-cover absolute -z-10 h-full w-full"
              ></g-image>

              <div
                class="featured-label absolute top-0 right-0 pr-10 pt-10 z-10"
              >
                <span
                  class="p-1 relative block text-center text-xs leading-tight bg-dark2 border border-dark0 text-dark0 rounded-full"
                >
                  <star-icon size="1.5x" class="custom-class"></star-icon>
                </span>
              </div>

              <div class="absolute top-0 left-0 text-light2 m-6">
                <p class="tracking-wide font-semibold">
                  <g-link :to="edge.node.category.path">{{
                    edge.node.category.title
                  }}</g-link>
                </p>

                <h3 class="tracking-wider mt-3 mb-3 text-4xl max-w-xl">
                  {{ edge.node.title }}
                </h3>
                <p>
                  <time :datetime="edge.node.datetime">{{
                    edge.node.humanTime
                  }}</time>
                  &nbsp;&bull;&nbsp;
                  {{ edge.node.timeToRead }} min read
                </p>
              </div>
            </div>
          </g-link>
        </div>
        <template #prevArrow>
          <div class="w-16 h-16 mr-10 z-40">
            <arrow-left-icon size="1.5x" class="custom-class"></arrow-left-icon>
          </div>
        </template>
        <template #nextArrow>
          <div class="w-16 h-16 z-50">
            <arrow-right-icon
              size="1.5x"
              class="custom-class"
            ></arrow-right-icon>
          </div>
        </template>
      </VueSlickCarousel>
    </div>
  </div>
</template>

<script>
import VueSlickCarousel from "vue-slick-carousel";
import { StarIcon, ArrowLeftIcon, ArrowRightIcon } from "vue-feather-icons";

export default {
  props: {
    records: {}
  },
  components: {
    VueSlickCarousel,
    ArrowLeftIcon,
    ArrowRightIcon,
    StarIcon
  },
  methods: {
    authors(record) {
      let tooltipText = [];
      for (let index = 0; index < record.author.length; index++) {
        if (index == 0) {
          tooltipText.push(`Posted by ${record.author[index].name}`);
        } else {
          if (index == 1) {
            tooltipText.push(`<br> Among with ${record.author[index].name}`);
          } else {
            tooltipText.push(`, ${record.author[index].name}`);
          }
        }
      }

      return tooltipText.join("");
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# Footer.vue {{{
	file=src/components/Partials/Footer.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div
    class="dark:bg-dark2 bg-light2 dark:text-dark0 text-light text-center text-md"
  >
    <div class="container mx-auto py-16">
      <div class="mb-4">
        <p>
          Copyright {{ new Date().getFullYear() }} by
          {{ $static.metadata.siteName }} &middot; Powered by
          <a href="https://www.gridsome.org" rel="noreferrer" target="_blank"
            >Gridsome</a
          >
        </p>
      </div>
      <div class="mb-4">
        <ul class="list-reset flex justify-center">
          <li
            v-for="socialItem in $static.metadata.social"
            :key="socialItem.name"
            class="px-4"
          >
            <a
              :href="socialItem.link"
              rel="noreferrer"
              target="_blank"
              :title="socialItem.name"
            >
              <github-icon size="1.5x" class="custom-class"></github-icon>
            </a>
          </li>
        </ul>
      </div>
      <div class="mb-4">
        <ul class="list-reset flex justify-center">
          <li
            v-for="navItem in $static.metadata.footerNavigation"
            :key="navItem.name"
            class="px-4"
          >
            <g-link
              :to="navItem.link"
              :title="navItem.name"
              v-if="navItem.external != true"
              >{{ navItem.name }}</g-link
            >
            <a
              :href="navItem.link"
              target="_blank"
              rel="noreferrer"
              :title="navItem.name"
              v-if="navItem.external == true"
              >{{ navItem.name }}</a
            >
          </li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
import { GithubIcon } from "vue-feather-icons";

export default {
  components: {
    GithubIcon
  }
};
</script>

<static-query>
query {
  metadata {
    siteName

    social {
      name
      icon
      link
    }
  
    footerNavigation {
        name
        link
        external
    }
  }
}
</static-query>
EOF
	# }}}
	# Templates:
	# Blog.vue {{{
	file=src/templates/Blog.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :image="$page.blog.image"
      :staticImage="false"
      :opacity="0"
    ></content-header>

    <div
      class="container sm:pxi-0 mx-auto overflow-x-hidden text-light0 dark:text-dark0"
    >
      <div class="lg:mx-32 md:mx-16 sm:mx-8 mx-4">
        <section class="post-header container mx-auto px-0 mb-16 text-center">
          <h1
            class="text-light0 dark:text-dark0 font-extrabold tracking-wider mb-6"
          >
            {{ $page.blog.title }}
          </h1>
          <span class="tracking-wide text-md">
            <g-link class="font-medium" :to="$page.blog.category.path">{{
              $page.blog.category.title
            }}</g-link
            >&nbsp;&middot;&nbsp;
            <time :datetime="$page.blog.datetime">{{
              $page.blog.humanTime
            }}</time>
            &nbsp;&middot;&nbsp;
            {{ $page.blog.timeToRead }} min read
          </span>
        </section>
      </div>

      <div class="lg:mx-32 md:mx-16 px-4">
        <section class="post-content container mx-auto relative">
          <div class="" v-html="$page.blog.content"></div>
        </section>

        <section class="post-tags container mx-auto relative py-10">
          <g-link
            v-for="tag in $page.blog.tags"
            :key="tag.id"
            :to="tag.path"
            class="text-xs bg-transparent dark-hover:bg-dark1 hover:bg-light1 py-2 px-4 mr-2 border border-light0 dark:border-dark0 text-light0 dark:text-dark0 rounded-full"
            >{{ tag.title }}</g-link
          >
        </section>
      </div>
    </div>

    <div
      class="border-t border-b border-light2 dark:border-dark2 bg-light2 dark:bg-dark2 text-light0 dark:text-dark0"
    >
      <div class="container mx-auto">
        <div class="lg:mx-32 md:mx-16 px-4 sm:px-0">
          <section class="container mx-auto">
            <div class="flex flex-wrap justify-center">
              <div class="w-full flex justify-center md:w-10/12 text-center">
                <div class="mb-2 sm:mb-0 w-full">
                  <div class="md:flex p-6 pl-0 self-center">
                    <g-image
                      :src="$page.blog.author[0].image"
                      class="border-2 border-black w-20 md:h-24 md:w-24 mx-auto md:mx-0 md:mr-6 rounded-md bg-gray-200"
                      alt="Author Image"
                    ></g-image>

                    <div class="text-center md:text-left">
                      <g-link
                        :to="$page.blog.author[0].path"
                        class="text-light0 dark:text-dark0"
                      >
                        <h2 class="text-lg my-1 mt-2 md:mt-0">
                          {{ $page.blog.author[0].name }}
                        </h2>
                      </g-link>
                      <div
                        v-if="authors.length > 0"
                        class="post-authors font-light text-sm pt-2"
                      >
                        Among with
                        <g-link
                          class="font-normal"
                          :to="author.path"
                          v-for="author in authors"
                          :key="author.name"
                          >{{ author.name }}</g-link
                        >
                      </div>
                      <div
                        class="font-light tracking-wider leading-relaxed py-4"
                      >
                        {{ $page.blog.author[0].bio }}
                      </div>
                      <div class="flex justify-center md:justify-start">
                        <a
                          :href="$page.blog.author[0].twitter"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500 flex items-center"
                        >
                          Twitter
                          <twitter-icon size="2.5x" class="mx-2"></twitter-icon>
                        </a>
                        &nbsp;
                        <a
                          :href="$page.blog.author[0].linkedin"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500 flex items-center"
                        >
                          Github
                          <github-icon size="2.5x" class="mx-2"></github-icon>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>

    <section class="post-related pt-10" v-if="relatedRecords.length > 0">
      <div class="container mx-auto">
        <div class="text-center">
          <h4 class="font-light text-light0 dark:text-dark0 my-0">
            Recommended for you
          </h4>
        </div>
        <div class="flex flex-wrap justify-center pt-8 pb-8">
          <CardItem
            :record="relatedRecord.node"
            v-for="relatedRecord in relatedRecords"
            :key="relatedRecord.node.id"
          ></CardItem>
        </div>
      </div>
    </section>
  </Layout>
</template>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";
import { sampleSize } from "lodash";
import { GithubIcon, TwitterIcon } from "vue-feather-icons";

export default {
  components: {
    CardItem,
    ContentHeader,
    TwitterIcon,
    GithubIcon
  },
  metaInfo() {
    return {
      title: this.$page.blog.title,
      meta: [
        {
          name: "description",
          content: this.$page.blog.excerpt
        }
      ]
    };
  },
  computed: {
    authors() {
      let authors = [];
      for (let index = 1; index < this.$page.blog.author.length; index++) {
        authors.push({
          name: this.$page.blog.author[index].name,
          path: this.$page.blog.author[index].path
        });
      }
      return authors;
    },
    relatedRecords() {
      return sampleSize(this.$page.related.edges, 2);
    }
  }
};
</script>

<page-query>
query($id: ID!, $tags: [ID]) {
  blog(id: $id) {
    title
    path
    image(width:1600, height:800, blur:10)
    excerpt 
    content
    humanTime : created(format:"DD MMMM YYYY")
    datetime : created(format:"ddd MMM DD YYYY hh:mm:ss zZ")
    timeToRead
    tags {
      id
      title
      path
    }
    category {
      id
      title
      path
      belongsTo(limit:4) {
        totalCount
        edges {
          node {
            ... on Blog {
              title
              path
            }
          }
        }
      }
    }
    author {
      id
      name
      image
      path
      bio
      facebook
      twitter
      linkedin
    }
  }

  related: allBlog(
    filter: { id: { ne: $id }, tags: {containsAny: $tags} }
  ) {
    edges {
      node {
        title
    path
    image(width:1600, height:800)
    excerpt
    content
    humanTime : created(format:"DD MMMM YYYY")
    datetime : created(format:"ddd MMM DD YYYY hh:mm:ss zZ")
    featured
    timeToRead
    tags {
      id
      title
      path
    }
    category {
      id
      title
      path
      belongsTo(limit:4) {
        totalCount
        edges {
          node {
            ... on Blog {
              title
              path
            }
          }
        }
      }
    }
    author {
      id
      name
      image
      path
    }
      }
    }
  }
}
</page-query>

<style lang="scss"></style>
EOF
	# }}}
	# Category.vue {{{
	file=src/templates/Category.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :title="$page.category.title"
      image="danil_silantev_F6Da4r2x5to.jpg"
    >
    </content-header>

    <div class="container mx-auto">
      <transition-group name="fade" class="flex flex-wrap my-4" tag="div">
        <CardItem
          v-for="{ node } of loadedPosts"
          :key="node.id"
          :record="node"
        />
      </transition-group>
      <ClientOnly>
        <infinite-loading @infinite="infiniteHandler" spinner="spiral">
          <div slot="no-more"></div>
          <div slot="no-results"></div>
        </infinite-loading>
      </ClientOnly>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!, $page:Int) {
    category(id: $id) {
      title
      path
      belongsTo(perPage: 6, page: $page, sortBy: "created") @paginate {
        totalCount
        pageInfo {
          totalPages
          currentPage
        }
        edges {
          node {
            ...on Blog {
              id
              title
              image(width: 800)
              path
              timeToRead
              featured
              humanTime: created(format: "DD MMM YYYY")
              datetime: created
              category {
                id
                title
                path
              }
              author {
                id
                name
                image(width: 64, height: 64, fit: inside)
                path
              }
            }
          }
        }
      }
    }  
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    CardItem,
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.category.title
    };
  },
  data() {
    return {
      loadedPosts: [],
      currentPage: 1
    };
  },
  created() {
    this.loadedPosts.push(...this.$page.category.belongsTo.edges);
  },
  methods: {
    async infiniteHandler($state) {
      if (
        this.currentPage + 1 >
        this.$page.category.belongsTo.pageInfo.totalPages
      ) {
        $state.complete();
      } else {
        const { data } = await this.$fetch(
          `${this.$page.category.path}${this.currentPage + 1}`
        );
        if (data.category.belongsTo.edges.length) {
          this.currentPage = data.category.belongsTo.pageInfo.currentPage;
          this.loadedPosts.push(...data.category.belongsTo.edges);
          $state.loaded();
        } else {
          $state.complete();
        }
      }
    }
  }
};
</script>
EOF
	# }}}
	# Author.vue {{{
	file=src/templates/Author.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header :image="$page.author.cover" :staticImage="false">
      <div
        class="text-center text-dark0 bg-gray-800 bg-opacity-50 lg:py-16 md:py-12 sm:py-10 py-8"
      >
        <div class="w-full">
          <g-image
            class="border-2 border-black md:h-32 md:w-32 h-24 w-24 rounded-md bg-white mx-auto"
            :src="$page.author.image"
            width="100"
            height="100"
            alt="Author Image"
          ></g-image>
        </div>
        <div class="w-full text-center pb-5">
          <h2 class="sm:text-5xl text-3xl font-extrabold">
            {{ $page.author.name }}
          </h2>
          <p class="sm:text-xl font-sans">{{ $page.author.bio }}</p>
        </div>
        <div class="w-full text-center pb-5">
          Blog Posts: {{ $page.author.belongsTo.totalCount }}
        </div>
        <div class="flex justify-center">
          <a
            :href="$page.author.facebook"
            target="_blank"
            rel="noopener noreferrer"
            class="hover:text-blue-500 flex items-center"
          >
            Facebook
            <github-icon size="2.5x" class="mx-2"></github-icon>
          </a>
          &nbsp;
          <a
            :href="$page.author.twitter"
            target="_blank"
            rel="noopener noreferrer"
            class="hover:text-blue-500 flex items-center"
          >
            Twitter
            <twitter-icon size="2.5x" class="mx-2"></twitter-icon>
          </a>
        </div>
      </div>
    </content-header>

    <div class="container mx-auto">
      <transition-group name="fade" class="flex flex-wrap my-4" tag="div">
        <CardItem
          v-for="{ node } of loadedPosts"
          :key="node.id"
          :record="node"
        />
      </transition-group>
      <ClientOnly>
        <infinite-loading @infinite="infiniteHandler" spinner="spiral">
          <div slot="no-more"></div>
          <div slot="no-results"></div>
        </infinite-loading>
      </ClientOnly>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!, $page:Int) {
    author(id: $id) {
      name
      path
      bio
      image(width:150, height:150)
      cover
      facebook
      twitter
      linkedin
      belongsTo(perPage: 6, page: $page) @paginate {
        totalCount
        pageInfo {
          totalPages
          currentPage
        }
        edges {
          node {
            ... on Blog {
              id
              title
              image(width: 800)
              path
              timeToRead
              featured
              humanTime: created(format: "DD MMM YYYY")
              datetime: created
              category {
                id
                title
                path
              }
              author {
                id
                name
                image(width: 64, height: 64, fit: inside)
                path
              }
            }
          }
        }
      }
    }  
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";
import { GithubIcon, TwitterIcon } from "vue-feather-icons";

export default {
  components: {
    CardItem,
    ContentHeader,
    TwitterIcon,
    GithubIcon
  },
  metaInfo() {
    return {
      title: this.$page.author.name
    };
  },
  data() {
    return {
      loadedPosts: [],
      currentPage: 1
    };
  },
  created() {
    this.loadedPosts.push(...this.$page.author.belongsTo.edges);
  },
  methods: {
    async infiniteHandler($state) {
      if (
        this.currentPage + 1 >
        this.$page.author.belongsTo.pageInfo.totalPages
      ) {
        $state.complete();
      } else {
        const { data } = await this.$fetch(
          `${this.$page.author.path}${this.currentPage + 1}`
        );
        if (data.author.belongsTo.edges.length) {
          this.currentPage = data.author.belongsTo.pageInfo.currentPage;
          this.loadedPosts.push(...data.author.belongsTo.edges);
          $state.loaded();
        } else {
          $state.complete();
        }
      }
    }
  }
};
</script>
EOF
	# }}}
	# CustomPage {{{
	file=src/templates/CustomPage.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header :title="$page.page.title"></content-header>
    <div
      class="container sm:pxi-0 mx-auto overflow-x-hidden text-gray-800 dark:text-gray-400"
    >
      <div class="lg:mx-32 md:mx-16 px-4 mb-8">
        <section
          class="post-content container mx-auto relative dark:text-gray-400"
        >
          <div v-html="$page.page.content"></div>
        </section>
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!) {
    page: customPage(id: $id) {
      title
      content
    }
  }
</page-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.page.title
    };
  }
};
</script>
EOF
	# }}}
	# Tag.vue {{{
	file=src/templates/Tag.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :title="$page.tag.title"
      image="danil_silantev_F6Da4r2x5to.jpg"
    >
    </content-header>
    <div class="container mx-auto">
      <transition-group name="fade" class="flex flex-wrap my-4" tag="div">
        <CardItem
          v-for="{ node } of loadedPosts"
          :key="node.id"
          :record="node"
        />
      </transition-group>
      <ClientOnly>
        <infinite-loading @infinite="infiniteHandler" spinner="spiral">
          <div slot="no-more"></div>
          <div slot="no-results"></div>
        </infinite-loading>
      </ClientOnly>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!, $page:Int) {
    tag(id: $id) {
      title
      path
      belongsTo(perPage: 6, page: $page, sortBy: "created") @paginate {
        totalCount
        pageInfo {
          totalPages
          currentPage
        }
        edges {
          node {
            ...on Blog {
              id
              title
              image(width: 800)
              path
              timeToRead
              featured
              humanTime: created(format: "DD MMM YYYY")
              datetime: created
              category {
                id
                title
                path
              }
              author {
                id
                name
                image(width: 64, height: 64, fit: inside)
                path
              }
            }
          }
        }
      }
    }  
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    CardItem,
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.tag.title
    };
  },
  data() {
    return {
      loadedPosts: [],
      currentPage: 1
    };
  },
  created() {
    this.loadedPosts.push(...this.$page.tag.belongsTo.edges);
  },
  methods: {
    async infiniteHandler($state) {
      if (this.currentPage + 1 > this.$page.tag.belongsTo.pageInfo.totalPages) {
        $state.complete();
      } else {
        const { data } = await this.$fetch(
          `${this.$page.tag.path}${this.currentPage + 1}`
        );
        if (data.tag.belongsTo.edges.length) {
          this.currentPage = data.tag.belongsTo.pageInfo.currentPage;
          this.loadedPosts.push(...data.tag.belongsTo.edges);
          $state.loaded();
        } else {
          $state.complete();
        }
      }
    }
  }
};
</script>
EOF
	# }}}
	# Page: Category.vue {{{
	file=src/pages/Category.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    {{ $page.categories }}
  </Layout>
</template>

<page-query>
  query {
    categories: allCategory(order: DESC) {
      totalCount
      pageInfo {
        totalPages
        currentPage
      }
      edges {
        node {
          title
        }
      }
    }
  }
</page-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  metaInfo: {
    title: "Categories"
  },
  components: {
    ContentHeader
  }
};
</script>

<style scoped></style>
EOF
	# }}}
} #}}}
# main {{{
[ -z $1 ] && opt="null" || opt=$1
[ -z $2 ] && dir="t2" || dir=$2
arr=(
	gen
	all
)
[[ " ${arr[@]} " =~ " ${opt} " ]] || cd $dir

case $opt in
"gen") gen ;;
"deps") deps ;;
"fill") fill ;;
"file") file ;;
"all") gen && fill && file && gridsome develop ;;
*) echo "Options are: gen, fill, dev" ;;
esac
