#!/bin/bash

name=$(cat config)
cd $name

setup() { #{{{
	rm -rf $name
	gridsome create $name
	dev=(
		@gridsome/source-filesystem
		@gridsome/transformer-remark
		@gridsome/plugin-sitemap
		gridsome-plugin-remark-shiki
		gridsome-plugin-tailwindcss
		tailwindcss
		tailwindcss-gradients
		tailwindcss-tables
		vue-feather-icons
		node-sass
		sass-loader
	)
	yarn add -D ${dev[@]}
}          #}}}
images() { #{{{
	# cover image
	file=content/posts/images/angus_gray_qlTW7eHUPdQ.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1590795620985-c15a2b137fb6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80'
	wget -c $dl -O $file

	# author
	file=src/assets/images/author.jpg
	mkdir -p "$(dirname $file)"
	dl='https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimg4.wikia.nocookie.net%2F__cb20100607195345%2Fdeusex%2Fen%2Fimages%2F3%2F32%2FJccover.jpg&f=1&nofb=1'
	wget -c $dl -O $file
}            #}}}
tag_file() { #{{{
	file=content/tags/test.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
id: vue
color: green
---
EOF
}        #}}}
tags() { #{{{
	file=src/components/PostTags.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
   <div class="post-tags">
   		<g-link class="post-tags__link" v-for="tag in post.tags" :key="tag.id" :to="tag.path">
   			<span>#</span> {{ tag.id }}
   		</g-link>
    </div>
</template>

<script>
export default {
  props: ['post']
}
</script>

<style lang="scss">
</style>
EOF
}                #}}}
tag_template() { #{{{
	file=src/templates/Tag.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <h1>{{ $page.tag.id }}</h1>
    <div v-for="edge in $page.tag.belongsTo.edges" :key="edge.node.id">
      <g-link :to="edge.node.path">{{ edge.node.title }}</g-link>
    </div>
  </div>
</template>

<page-query>
query Tag($id: ID!) {
  tag(id: $id) {
    id
    belongsTo {
      edges {
        node {
          ... on Post {
            title
            path
          }
        }
      }
    }
  }
}
</page-query>
EOF
}           #}}}
Default() { #{{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <header>
      <strong>
        <g-link to="/">{{ $static.metadata.siteName }}</g-link>
      </strong>
      <nav>
        <g-link to="/">Home</g-link>
        <g-link to="/test/">Test</g-link>
      </nav>
    </header>
    <slot />
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<style lang="scss">
html {
  background-color: gray;
}
</style>
#+END_SRC
EOF
}          #}}}
author() { #{{{
	file=src/components/Author.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
	<div class="author">

		<g-image alt="Author image" class="author__image" src="~/assets/images/author.jpg" width="180" height="180" blur="5" />

		<h1 v-if="showTitle" class="author__site-title">
			{{ $static.metadata.siteName }}
		</h1>

		<p class="author__intro">
			A simple, hackable & minimalistic starter for Gridsome that uses Markdown for content.
		</p>

		<p class="author__links">
			<a href="//twitter.com/gridsome">Follow on Twitter</a>
			<a href="//github.com/gridsome/gridsome-starter-blog">GitHub</a>
		</p>

	</div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<script>
export default {
	props: ['showTitle']
}
</script>

<style lang="scss">
</style>
EOF
}             #}}}
post_meta() { #{{{
	file=src/components/PostMeta.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="post-meta">
    Posted {{ post.date }}.
    <template v-if="post.timeToRead">
      <strong>{{ post.timeToRead }} min read.</strong>
    </template>
  </div>
</template>

<script>
export default {
  props: ["post"]
};
</script>

<style lang="scss">
</style>
EOF
}           #}}}
postvue() { #{{{
	file=src/templates/Post.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <div>
    <h1>
      {{ $page.post.title }}
    </h1>
    <PostMeta :post="$page.post" />
    <strong>Author: </strong><span>{{ $page.post.author }}</span>
    <br />
    <strong>Date: </strong><span>{{ $page.post.date }}</span>
    <br />
    <span>{{ $page.post.created_at }}</span>
    <br />
    <g-image alt="Cover image" v-if="$page.post.cover_image" :src="$page.post.cover_image" />
    <br />
      <div v-html="$page.post.content" />
        <PostTags :post="$page.post" />
    <Author class="post-author" />
    </div>
  </Layout>
</template>

<script>
import PostMeta from '~/components/PostMeta'
import Author from '~/components/Author.vue'
import PostTags from '~/components/PostTags'

export default {
  components: {
    Author,
    PostMeta,
    PostTags
  },
  metaInfo () {
    return {
      title: this.$page.post.title,
      meta: [
        {
          name: 'description',
          content: this.$page.post.description
        }
      ]
    }
  }
}
</script>

<page-query>
query Post($id: ID!) {
  post(id: $id) {
    title
    path
    author
    date (format: "D. MMMM YYYY")
    cover_image (width: 860, blur: 10)
    timeToRead
    description
    content
    tags {
      id
      color
      path
    }
  }
}
</page-query>
EOF
}             #}}}
post_file() { #{{{
	file=content/posts/test.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: test
author: Fluid
date: 2019-01-07
cover_image: ./images/angus_gray_qlTW7eHUPdQ.jpg
tags: [vue, tutorial]
description: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
---

# TEST

## Using the Gridsome CLI

The easiest way to install this theme or a Gridsome theme in general is by using their CLI tool.

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```
EOF
}              #}}}
post_file2() { #{{{
	file=content/posts/test2.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: test2
author: Fluid
date: 2019-01-07
cover_image: ./images/angus_gray_qlTW7eHUPdQ.jpg
tags: [vue, tutorial]
description: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
---

# TEST2

## Using the Gridsome CLI

The easiest way to install this theme or a Gridsome theme in general is by using their CLI tool.

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```
EOF
}              #}}}
post_file3() { #{{{
	file=content/posts/test3.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: test3
author: Fluid
date: 2019-01-07
cover_image: ./images/angus_gray_qlTW7eHUPdQ.jpg
tags: [vue, tutorial]
description: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
---

# TEST3

## Using the Gridsome CLI

The easiest way to install this theme or a Gridsome theme in general is by using their CLI tool.

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```
EOF
}        #}}}
card() { #{{{
	file=src/components/PostCard.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="">
    <div class="post-card__header">
      <g-image alt="Cover image" v-if="post.cover_image" class="post-card__image" :src="post.cover_image" />
    </div>
    <div class="post-card__content">
      <h2 class="post-card__title" v-html="post.title" />
      <p class="post-card__description" v-html="post.description" />

      <PostMeta class="post-card__meta" :post="post" />
      <PostTags class="post-card__tags" :post="post" />

      <g-link class="post-card__link" :to="post.path">Link</g-link>
    </div>
  </div>
</template>

<script>
import PostMeta from '~/components/PostMeta'
import PostTags from '~/components/PostTags'

export default {
  components: {
    PostMeta,
    PostTags
  },
  props: ['post'],
}
</script>

<style lang="scss">
</style>
EOF
}         #}}}
index() { #{{{
	file=src/pages/Index.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <div class="">
      <div class="">
    <!-- List posts -->
      <PostCard v-for="edge in $page.posts.edges" :key="edge.node.id" :post="edge.node"/>
    </div>
    </div>
    <!-- Author intro -->
    <Author :show-title="true" />

  </Layout>
</template>

<page-query>
query($page:Int) {
  posts: allPost(perPage: 1, page: $page) @paginate {
    totalCount
    pageInfo {
      totalPages
      currentPage
    }
    edges {
      node {
        id
        title
        date (format: "DD MMM YYYY")
        timeToRead
        description
        cover_image (width: 770, height: 380, blur: 10)
        path
        tags {
          id
          path
        }
      }
    }
  }
}
</page-query>

<script>
import Author from '~/components/Author.vue'
import PostCard from '~/components/PostCard.vue'

export default {
  components: {
    Author,
    PostCard
  },
  metaInfo: {
    title: 'Hello, world!'
  }
}
</script>
EOF
}            #}}}
gridsome() { #{{{
	file=gridsome.config.js
	cat <<'EOF' >$file
module.exports = {
  siteName: "Gridsome",
  siteDescription: 'A simple, hackable & minimalistic starter for Gridsome that uses Markdown for content.',

  plugins: [
    {
      use: 'gridsome-plugin-tailwindcss',
      options: {
        tailwindConfig: './tailwind.config.js',
        purgeConfig: {
          whitelist: ['svg-inline--fa', 'table', 'table-striped', 'table-bordered', 'table-hover', 'table-sm'],
          whitelistPatterns: [/fa-$/, /blockquote$/, /code$/, /pre$/, /table$/, /table-$/]
        },
        presetEnvConfig: {},
        shouldPurge: false,
        shouldImport: true,
        shouldTimeTravel: true,
        shouldPurgeUnusedKeyframes: true,
      }
    }, 
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Post",
        path: "content/posts/*.md",
        refs: {
          tags: "Tag"
        },
        remark: {
          plugins: [
            [
              "gridsome-plugin-remark-shiki",
              { theme: "Material-Theme-Palenight", skipInline: false }
            ]
          ]
        }
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Tag",
        path: "content/tags/*.md",
        pathPrefix: "/tags",
      }
    }
  ],

  templates: {
    Post: "/:title",
    Tag: "/tags/:id"
  }
}
EOF
}            #}}}
tailwind() { #{{{
	file=tailwind.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
//tailwind border color plugin powered by
//https://github.com/tailwindcss/tailwindcss/pull/560#issuecomment-503222143
var _ = require('lodash')
var flattenColorPalette = require('tailwindcss/lib/util/flattenColorPalette').default


module.exports = {
  purge: ["./src/**/*.html", "./src/**/*.vue", "./src/**/*.jsx"],
  theme: {
    borderWidth: {
      default: '1px',
      '0': '0',
      '2': '2px',
      '3': '3px',
      '4': '4px',
      '6': '6px',
      '8': '8px'
    },
    flex: {
      '1': '1 1 0%',
      auto: '1 1 auto',
      initial: '0 1 auto',
      none: 'none',
      'post': '1 1 300px',
      '100': '1 1 100%',
      'post-large-content': '0 1 361px',
    },
    zIndex: {
      '-10': '-10',
      '0': 0,
      '10': 10,
      '20': 20,
      '30': 30,
      '40': 40,
      '50': 50,
      '25': 25,
      '50': 50,
      '75': 75,
      '100': 100,
      '1000': 1000,
      'auto': 'auto',
    },
    corePlugins: {
      container: false
    },
    extend: {},
    radialGradients: {
      shapes: { // defaults to this value
        'default': 'ellipse',
      },
      sizes: { // defaults to this value
        'default': '',
      },
      positions: { // defaults to these values
        'default': 'center',
        't': 'top'
      },
      colors: { // defaults to {}
        'gray-to-black': ['rgba(25, 25, 25, 1)', 'rgba(8, 8, 8, 1)', 'rgba(0, 0, 0, 1)']
      },
    },
  },
  variants: {},
  plugins: [
    function ({
      addComponents
    }) {
      addComponents({
        '.container': {
          maxWidth: '100%',
          '@screen sm': {
            maxWidth: '640px',
          },
          '@screen md': {
            maxWidth: '768px',
          },
          '@screen lg': {
            maxWidth: '1024px',
          },
          '@screen xl': {
            maxWidth: '1040px',
          },
        }
      })
    },
    function ({
      addUtilities,
      e,
      theme,
      variants
    }) {
      const colors = flattenColorPalette(theme('borderColor'))

      const utilities = _.flatMap(_.omit(colors, 'default'), (value, modifier) => ({

        [`.${e(`border-t-${modifier}`)}`]: {
          borderTopColor: `${value}`
        },
        [`.${e(`border-r-${modifier}`)}`]: {
          borderRightColor: `${value}`
        },
        [`.${e(`border-b-${modifier}`)}`]: {
          borderBottomColor: `${value}`
        },
        [`.${e(`border-l-${modifier}`)}`]: {
          borderLeftColor: `${value}`
        },
      }))

      addUtilities(utilities, variants('borderColor'))
    },
    function ({
      addBase,
      config
    }) {

      addBase({
        'h1': {
          fontSize: config('theme.fontSize.5xl'),
          fontWeight: config('theme.fontWeight.bold'),
          fontFamily: config('theme.fontFamily.sans').join(', '),
          marginTop: config('theme.margin.4'),
          marginBottom: config('theme.margin.4')
        },
        'h2': {
          fontSize: config('theme.fontSize.4xl'),
          fontWeight: config('theme.fontWeight.bold'),
          fontFamily: config('theme.fontFamily.sans').join(', '),
          marginTop: config('theme.margin.4'),
          marginBottom: config('theme.margin.4')
        },
        'h3': {
          fontSize: config('theme.fontSize.3xl'),
          fontWeight: config('theme.fontWeight.bold'),
          fontFamily: config('theme.fontFamily.sans').join(', '),
          marginTop: config('theme.margin.4'),
          marginBottom: config('theme.margin.4')
        },
        'h4': {
          fontSize: config('theme.fontSize.2xl'),
          fontWeight: config('theme.fontWeight.bold'),
          fontFamily: config('theme.fontFamily.sans').join(', '),
          marginTop: config('theme.margin.4'),
          marginBottom: config('theme.margin.4')
        },
        'h5': {
          fontSize: config('theme.fontSize.xl'),
          fontWeight: config('theme.fontWeight.bold'),
          fontFamily: config('theme.fontFamily.sans').join(', '),
          marginTop: config('theme.margin.4'),
          marginBottom: config('theme.margin.4')
        },
        'h6': {
          fontSize: config('theme.fontSize.lg'),
          fontWeight: config('theme.fontWeight.bold'),
          fontFamily: config('theme.fontFamily.sans').join(', '),
          marginTop: config('theme.margin.4'),
          marginBottom: config('theme.margin.4')
        },
      })
    },
    require('tailwindcss-tables')(),
    require('tailwindcss-gradients')
  ]
}
EOF
}       #}}}
run() { #{{{
	#setup
	#images
	tag_file
	tags
	tag_template
	Default
	author
	post_meta
	postvue
	post_file
	post_file2
	post_file3
	card
	index
	gridsome
	tailwind
} #}}}
run
echo Complete $(date)!
