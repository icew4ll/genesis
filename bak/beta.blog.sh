#!/bin/bash
gen() { #{{{
	[ -d $dir ] && echo "Removing $dir" && rm -rf $dir
	gridsome create $dir
	cd $dir
	ncu -u
	yarn
}        #}}}
deps() { #{{{
	dev=(
		@gridsome/plugin-sitemap
		gridsome-plugin-rss
		#gridsome-plugin-remark-shiki
		#@gridsome/remark-prismjs
		gridsome-plugin-remark-prismjs-all
		@gridsome/source-filesystem
		@gridsome/transformer-remark
		node-sass
		sass-loader
		tailwindcss
		tailwindcss-dark-mode
		gridsome-plugin-tailwindcss
		#tailwindcss-gradients
		#tailwindcss-tables
		rfs
		vue-feather-icons
	)
	yarn add -D ${dev[@]}
}        #}}}
fill() { #{{{
	# main.js {{{
	file=src/main.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
import '~/assets/scss/main.scss'
import DefaultLayout from "~/layouts/Default.vue";

export default function(Vue, { router, head, isClient }) {
  Vue.component("Layout", DefaultLayout);
}
EOF
	# }}}
	# main.scss {{{
	file=src/assets/scss/main.scss
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
@import "~rfs/scss";

@tailwind base;

html {
  background: gray;
  color: white;
}

blockquote {
  @apply border-l;
  @apply border-l-4;
  @apply border-l-blue-500;
  @apply pl-4;
  @apply italic;
  @apply my-8;

  p {
    padding: 0 !important;
  }
}

h1,
.h1 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(3rem);
}

h2,
.h2 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(2.25rem);
}

h3,
.h3 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.875rem);
}

h4,
.h4 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.5rem);
}

h5,
.h5 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.25rem);
}

h6,
.h6 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.125rem);
}

@tailwind components;
@tailwind utilities;

.container {
  @apply max-w-screen-xl;
  @apply px-0;
}

.fade-enter-active,
.fade-leave-active {
  @apply transition-all;
  @apply duration-200;
}

.fade-enter,
.fade-leave-to {
  opacity: 0;
}

.featured-post-card {
  .slick-list {
    @apply h-full;
    @apply rounded-lg;

    div:not(.post-card-author):not(.featured-label):not(.post-card-content):not(.post-card-footer) {
      @apply h-full;
    }
  }

  .slick-arrow {
    @apply absolute;
    @apply bottom-0;
    @apply right-0;
    @apply text-white;
  }

  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  &:hover {
    transform: translateY(-5px);
  }

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-content {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
    @apply mt-20;
    @apply ml-10;
    @apply text-white;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
    @apply left-0;
    @apply text-white;
    @apply ml-10;
    @apply mb-10;
    @apply text-sm;
    @apply font-semibold;
  }
}

.post-card {
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  &:hover {
    transform: translateY(-5px);
  }

  @apply relative;
  @apply border;

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-title {
    @apply leading-8;
    @apply text-2xl;
  }

  .post-card-excerpt {
    @apply font-serif;
  }

  .post-card-content {
    @apply relative;
    @apply flex-1;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
  }
}

.header {
  min-height: 500px;
}

.mobileSubnav {
  @apply absolute;
  @apply w-full;
  @apply -mx-2;
}

@media (max-width: 767px) {
  .header {
    min-height: 360px;
  }
}

.mega-menu {
  left: 0;
  top: 0;
  @apply mt-16;
  @apply w-full;
  @apply absolute;
  @apply text-left;
}

/* ––––––––––––––––––––––––––––––––––––––––––––––––––
    Content
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

.mode-dark {
  .post-content {
    p,
    span:not(.token),
    li {
      @apply tracking-wider;
      @apply leading-relaxed;
      @apply font-normal;
      @apply text-gray-500;
      @include font-size(1.1rem);
    }
  }
}

.post-content {
  p,
  span:not(.token),
  li {
    @apply tracking-wider;
    @apply leading-relaxed;
    @apply font-normal;
    @apply text-gray-800;
    @include font-size(1.1rem);
  }

  ol {
    @apply list-decimal;
    @apply ml-5;
    @apply mt-5;
  }

  ul {
    @apply list-disc;
    @apply ml-5;
    @apply mt-5;
  }

  li > ul {
    @apply mt-0;
  }
}

.post-authors a::after {
  content: ", ";
}

.post-authors a:last-child::after {
  content: "";
}

pre[class*="language-"],
code[class*="language-"] {
  color: #5c6e74;

  text-shadow: none;
  font-family: Consolas, Monaco, "Andale Mono", "Ubuntu Mono", monospace;
  @include font-size(0.9rem);
  direction: ltr;
  text-align: left;
  white-space: pre;
  word-spacing: normal;
  word-break: normal;
  line-height: 1.5;
  -moz-tab-size: 4;
  -o-tab-size: 4;
  tab-size: 4;
  -webkit-hyphens: none;
  -moz-hyphens: none;
  -ms-hyphens: none;
  hyphens: none;
}

pre[class*="language-"]::selection,
code[class*="language-"]::selection,
pre[class*="language-"]::mozselection,
code[class*="language-"]::mozselection {
  text-shadow: none;
  @apply bg-gray-200;
}

pre[class*="language-"] {
  padding: 1em;
  margin: 0.5em 0;
  overflow: auto;
  @apply bg-gray-200;
}

:not(pre) > code[class*="language-"] {
  padding: 0.3em 0.3em;
  border-radius: 0.3em;
  color: #db4c69;
  @apply bg-gray-200;
}

.mode-dark {
  pre[class*="language-"]::selection,
  code[class*="language-"]::selection,
  pre[class*="language-"]::mozselection,
  code[class*="language-"]::mozselection {
    text-shadow: none;
    @apply bg-gray-800;
  }

  pre[class*="language-"],
  code[class*="language-"] {
    @apply text-gray-400;
  }

  pre[class*="language-"] {
    padding: 1em;
    margin: 0.5em 0;
    overflow: auto;
    @apply bg-gray-800;
  }

  :not(pre) > code[class*="language-"] {
    padding: 0.3em 0.3em;
    border-radius: 0.3em;
    color: #db4c69;
    @apply bg-gray-800;
  }
}

@media print {
  pre[class*="language-"],
  code[class*="language-"] {
    text-shadow: none;
  }
}

/*********************************************************
* Tokens
*/
.namespace {
  opacity: 0.7;
}

.token.comment,
.token.prolog,
.token.doctype,
.token.cdata {
  color: #93a1a1;
}

.token.punctuation {
  color: #999999;
}

.token.property,
.token.tag,
.token.boolean,
.token.number,
.token.constant,
.token.symbol,
.token.deleted {
  color: #990055;
}

.token.selector,
.token.attr-name,
.token.string,
.token.char,
.token.builtin,
.token.inserted {
  color: #669900;
}

.token.operator,
.token.entity,
.token.url,
.language-css .token.string,
.style .token.string {
  color: #a67f59;
  background: transparent;
}

.token.atrule,
.token.attr-value,
.token.keyword {
  color: #0077aa;
}

.token.function {
  color: #dd4a68;
}

.token.regex,
.token.important,
.token.variable {
  color: #ee9900;
}

.token.important,
.token.bold {
  font-weight: bold;
}

.token.italic {
  font-style: italic;
}

.token.entity {
  cursor: help;
}

/*********************************************************
* Line highlighting
*/
pre[data-line] {
  position: relative;
}

pre[class*="language-"] > code[class*="language-"] {
  position: relative;
  z-index: 1;
}

.line-highlight {
  position: absolute;
  left: 0;
  right: 0;
  padding: inherit 0;
  margin-top: 1em;
  background: #000;
  box-shadow: inset 5px 0 0 #f7d87c;
  z-index: 0;
  pointer-events: none;
  line-height: inherit;
  white-space: pre;
}

.mode-dark {
  .medium-zoom-overlay {
    background-color: #000 !important;
  }
}

.medium-zoom-overlay {
  @apply z-1000;
}

.medium-zoom-image {
  @apply z-1000;
}

figure {
  @apply my-8;
  @apply -mx-16;
  width: calc(100% + 8rem);

  figcaption {
    @apply text-center;
  }
}
EOF
	# }}}
	# image {{{
	# blog
	file=content/blog/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author
	file=content/author/images/author.jpg
	mkdir -p "$(dirname $file)"
	dl='https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimg4.wikia.nocookie.net%2F__cb20100607195345%2Fdeusex%2Fen%2Fimages%2F3%2F32%2FJccover.jpg&f=1&nofb=1'
	wget -c $dl -O $file

	# cover
	file=src/assets/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author cover
	file=content/author/cover/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file
	# }}}
	# blog {{{
	file=content/blog/entry1.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: Styles1
tags: tag1, tag2
category: Digital
excerpt: Lorem markdownum aptos pes, Inachidos caput corrumpere! Vincere ferocia arva.
created: 2019-01-10
image: ./images/danil_silantev_F6Da4r2x5to.jpg
image_caption: Photo by Josh Spires on Unsplash
author: author1, author2, author3
featured: true
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	for num in {2..12}; do
		file=content/blog/entry$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Styles$num
EOF
		cat <<'EOF' >>$file
tags: tag1, tag2
category: Digital
excerpt: Lorem markdownum aptos pes, Inachidos caput corrumpere! Vincere ferocia arva.
created: 2019-01-10
image: ./images/danil_silantev_F6Da4r2x5to.jpg
image_caption: Photo by Josh Spires on Unsplash
author: author1, author2, author3
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# author {{{
	for num in {1..3}; do
		file=content/author/author$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
id: author$num
name: author$num
EOF
		cat <<'EOF' >>$file
bio: Primis vitae mauris turpis ornare libero odio torquent vehicula proin consequat curabitur mattis
facebook: https://www.facebook.com
twitter: https://www.twitter.com
linkedin: https://www.linkedin.com
image: ./images/author.png
cover: ./cover/danil_silantev_F6Da4r2x5to.jpg
---
EOF
	done
	# }}}
	# about.md {{{
	file=content/pages/about.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: About us
---

## Ossa narrat sortita fecerat sit conataque

Lorem markdownum aptos pes, Inachidos caput corrumpere! Hanc haud quam [est
candore](http://quisquis-in.io/ramossuperum) conpulit meriti. Vincere ferocia
arva.

## Eleis celeberrimus loci ait falsa infelix tuoque

Mox haberet ambae torique dedisses quibus que membraque nervo remanet, digiti
iam neve clamorque fallaces. Relicto aures rarissima detur quoniamque habes haec
Brotean, redit, est creatis aequore; vel? Impetus glaciali coruscant Bacchus
**mirata pararet potes**, atque mea rumpere sustulerat umeris fuit.

## Facundis quid

Venerit conveniunt per memori sed laniarat Dromas, solum tum. Undis lacteus
infitiatur adest [acies certius](http://www.tollit-clamavit.io/) inscius, cum ad
emittunt dextra.

Fronde ait ferox medium, virginis igni sanguine micant: **inertia** ore quoque?
Iaculi quicquid **virescere misit stirpe** Theseus Venerem! Falce taceo oves,
idem fugit, non abiit palam quantum, fontes vinci et abiit. Deiectoque exstabant
**Phrygiae** cepit munus tanto.

## Et capienda Peneia

*Haec moenia pater* signataque urget, ait quies laqueo sumitque. Misit sit
moribunda terrae sequar longis hoc, cingebant copia cultros! Alis templi taeda
solet suum mihi penates quae. Cecidere *deo agger infantem* indetonsusque ipsum;
ova formasque cornu et pectora [voce oculos](http://www.tibibene.io/iter.html),
prodis pariterque sacra finibus, Sabinae. Fugarant fuerat, famam ait toto imas
sorte pectora, est et, procubuit sua Appenninigenae habes postquam.

## Quoque aut gurgite aliquis igneus

Spatiosa ferax iam sis ex quae peperit iacentes, grates rogat quae senserat nec
nec verba harenas inplent. Per dum necis in in versus quin loquendi latens;
inde. **Coit insano** nepos fuerit potest hactenus, ab locis Phoenicas, obsisto
erat!

> Nec uterum Aurorae petentes abstulit. Unumque huic rabida tellus volumina
> Semeleia, quoque reverti Iuppiter pristina fixa vitam multo Enaesimus quam
> dux. Sua **damus** decipere, ut **obortas** nomen sine vestrae vita.

Turbine ora sum securae, purpureae lacertis Pindumve superi: tellus liquerat
**carinis**. Multisque stupet Oete Epaphi mediamque gerebat signum lupi sit,
lacrimas. Tumidi fassusque hosti, deus [vixque desint
dedit](http://hisnurus.com/putares-pars) dum et, quo non, dea [suras
tantum](http://mactata.org/inducere.php). Unus acta capulo. In Dryope sic
vestigia est neu ignis in **illa mirantur agilis** densior.
EOF
	# }}}
	# gridsome.config.js {{{
	file=gridsome.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
const url = "https://spiritwalk.netlify.com";
const site = "Spiritwalk";
const desc =
  "Blog posts";
module.exports = {
  siteName: site,
  siteDescription: desc,
  siteUrl: url,
  titleTemplate: `%s | ` + site,
  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Blog",
        path: "./content/blog/**/*.md",
        refs: {
          author: "Author",
          tags: {
            typeName: "Tag",
            create: true
          },
          category: {
            typeName: "Category",
            create: true
          }
        }
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Author",
        path: "./content/author/*.md"
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "CustomPage",
        path: "./content/pages/*.md"
      }
    },
    {
      use: 'gridsome-plugin-tailwindcss',
      options: {
        tailwindConfig: './tailwind.config.js',
        purgeConfig: {
          // Prevent purging of prism classes.
          whitelistPatternsChildren: [
            /token$/
          ]
        }
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        cacheTime: 600000 // default
      }
    },
    {
      use: "gridsome-plugin-rss",
      options: {
        contentTypeName: "Blog",
        feedOptions: {
          title: desc,
          feed_url: url + "/rss.xml",
          site_url: url
        },
        feedItemOptions: node => ({
          title: node.title,
          description: node.description,
          url: url + node.path,
          author: node.author,
          date: node.date
        }),
        output: {
          dir: "./static",
          name: "rss.xml"
        }
      }
    }
  ],
  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: "_blank",
      externalLinksRel: ["nofollow", "noopener", "noreferrer"],
      anchorClassName: "icon icon-link",
      plugins: [
        [
          "gridsome-plugin-remark-prismjs-all",
          {
            noInlineHighlight: false,
            showLineNumbers: false
          }
        ]
      ]
    }
  },
  templates: {
    Blog: [
      {
        path: "/:title",
        component: "~/templates/BlogPost.vue"
      }
    ],
    CustomPage: [
      {
        path: "/:title",
        component: "~/templates/CustomPage.vue"
      }
    ],
    Category: [
      {
        path: "/category/:title",
        component: "~/templates/Category.vue"
      }
    ],
    Author: [
      {
        path: "/author/:name",
        component: "~/templates/Author.vue"
      }
    ],
    Tag: [
      {
        path: "/tags/:title",
        component: "~/templates/Tag.vue"
      }
    ]
  },
  chainWebpack: config => {
    config.resolve.alias.set("@pageImage", "@/assets/images");
  }
};
EOF
	# }}}
	# tailwind.config.js {{{
	file=tailwind.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
//tailwind border color plugin powered by
//https://github.com/tailwindcss/tailwindcss/pull/560#issuecomment-503222143
var _ = require("lodash");
var flattenColorPalette = require("tailwindcss/lib/util/flattenColorPalette")
  .default;

module.exports = {
  purge: {
    content: ["./src/**/*.html", "./src/**/*.vue", "./src/**/*.jsx"],
    options: {
      whitelist: [
        "bg-opacity-0",
        "bg-opacity-25",
        "bg-opacity-50",
        "bg-opacity-75",
        "bg-opacity-100",
        "mode-dark"
      ]
    }
  },
  theme: {
    extend: {
      height: {
        "128": "32rem",
        "half-screen": "50vh"
      },
      backgroundOpacity: {
        "0": "0",
        "10": "0.1",
        "20": "0.2",
        "30": "0.3",
        "40": "0.4",
        "50": "0.5",
        "60": "0.6",
        "70": "0.7",
        "80": "0.8",
        "90": "0.9",
        "100": "1"
      }
    },
    fontFamily: {
      sans: [
        '"Source Sans Pro"',
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: ["Georgia", "Cambria", '"Times New Roman"', "Times", "serif"],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    },
    zIndex: {
      "-10": "-10",
      "0": 0,
      "10": 10,
      "20": 20,
      "30": 30,
      "40": 40,
      "50": 50,
      "25": 25,
      "50": 50,
      "75": 75,
      "100": 100,
      "1000": 1000,
      auto: "auto"
    },
    boxShadow: {
      default: "0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)",
      md: "0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)",
      lg:
        "0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)",
      xl:
        "0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)",
      "2xl": "0 25px 50px -12px rgba(0, 0, 0, .25)",
      "2xl-strong": "0 25px 50px -12px rgba(0, 0, 0, .5)",
      "3xl": "0 35px 60px -15px rgba(0, 0, 0, .3)",
      inner: "inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)",
      outline: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      focus: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      none: "none"
    }
  },
  variants: {
    backgroundColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    textColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    borderColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ]
  },
  plugins: [
    require("tailwindcss-dark-mode")(),
    function({ addUtilities, e, theme, variants }) {
      const colors = flattenColorPalette(theme("borderColor"));

      const utilities = _.flatMap(
        _.omit(colors, "default"),
        (value, modifier) => ({
          [`.${e(`border-t-${modifier}`)}`]: {
            borderTopColor: `${value}`
          },
          [`.${e(`border-r-${modifier}`)}`]: {
            borderRightColor: `${value}`
          },
          [`.${e(`border-b-${modifier}`)}`]: {
            borderBottomColor: `${value}`
          },
          [`.${e(`border-l-${modifier}`)}`]: {
            borderLeftColor: `${value}`
          }
        })
      );

      addUtilities(utilities, variants("borderColor"));
    }
  ],
  corePlugins: {
    container: false
  }
};
EOF
	# }}}
}        #}}}
file() { #{{{
	# Index.html {{{
	file=src/index.html
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<!DOCTYPE html>
<html ${htmlAttrs}>
  <head>
    ${head}
  </head>
  <body ${bodyAttrs}>
    <script>
      // Add dark / light detection that runs before Vue.js load. Borrowed from overreacted.io
      // for this starter, i used the code from gridsome.org
      (function() {
        window.__onThemeChange = function() {};
        function setTheme(newTheme) {
          window.__theme = newTheme;
          preferredTheme = newTheme;
          document.body.setAttribute("data-theme", newTheme);
          document.documentElement.classList.remove("mode-light");
          document.documentElement.classList.remove("mode-dark");
          document.documentElement.classList.add("mode-" + newTheme);

          window.__onThemeChange(newTheme);
        }

        var preferredTheme;
        try {
          preferredTheme = localStorage.getItem("theme");
        } catch (err) {}

        window.__setPreferredTheme = function(newTheme) {
          setTheme(newTheme);
          try {
            localStorage.setItem("theme", newTheme);
          } catch (err) {}
        };

        var darkQuery = window.matchMedia("(prefers-color-scheme: dark)");

        darkQuery.addListener(function(e) {
          window.__setPreferredTheme(e.matches ? "dark" : "light");
        });

        setTheme(preferredTheme || (darkQuery.matches ? "dark" : "light"));
      })();
    </script>

    ${app} ${scripts}
  </body>
</html>
EOF
	# }}}
	# LAYOUTS:
	# Default.vue {{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div id="app" class="dark:bg-black">
    <slot />
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<script>
export default {
  data: function() {
    return {
      theme: "light"
    };
  },
  components: {
  },
  methods: {
    setTheme(mode) {
      this.theme = mode;
    }
  }
};
</script>
EOF
	# }}}
	# COMPONENTS:
	# ContentHeader.vue {{{
	file=src/components/Partials/ContentHeader.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div
      class="z-100 text-center bg-gray-200 dark:bg-gray-900 py-10 md:py-20"
      v-if="!hasImage"
    >
      <h2 v-if="title != null" class="h1 font-extrabold dark:text-gray-400">
        {{ title }}
      </h2>
      <p v-if="sub != null" class="text-gray-600 text-light font-sans">
        {{ sub }}
      </p>
    </div>

    <div v-if="hasImage" class="z-100 relative mt-0 h-auto">
      <g-image
        v-if="hasImage && staticImage"
        :src="require(`!!assets-loader!@pageImage/${image}`)"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <g-image
        v-if="hasImage && !staticImage"
        :src="image"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <slot>
        <div
          class="text-center text-white bg-gray-800 lg:py-48 md:py-32 py-24"
          :class="`bg-opacity-${opacity}`"
        >
          <h2 v-if="title != null" class="h1 font-extrabold">{{ title }}</h2>
          <p v-if="sub != null" class="h5 font-sans">{{ sub }}</p>
        </div>
      </slot>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    title: {
      type: String,
      default: null
    },
    sub: {
      type: String,
      default: null
    },
    image: {
      type: String | Object,
      default: null
    },
    staticImage: {
      type: Boolean,
      default: true
    },
    opacity: {
      type: Number,
      default: 50
    }
  },
  computed: {
    hasImage() {
      return this.image ? true : false;
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# CardItem.vue {{{
	file=src/components/Content/CardItem.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="w-full md:w-1/2 lg:w-1/3 px-4 my-4">
    <div
      class="post-card border-gray-200 dark:border-gray-900 bg-white dark:bg-black rounded-lg hover:shadow-xl z-100 dark:bg-gray-900"
      :id="record.id"
    >
      <div
        class="post-card-author pt-4 pl-4"
        v-tooltip="{
          classes: 'card-author-tooltip',
          content: this.authors,
          placement: 'right'
        }"
      >
        <g-link
          :to="record.author[0].path"
          @mouseover="showTooltip = true"
          @mouseleave="showTooltip = false"
          @mouseover.native="showTooltip = true"
          @mouseleave.native="showTooltip = false"
        >
          <g-image
            :src="record.author[0].image"
            :alt="record.author[0].name"
            class="w-8 h-8 rounded-full bg-gray-200 border-2 border-white"
          />
        </g-link>
      </div>

      <g-link :to="record.path" class="post-card-image-link">
        <div
          v-if="record.featured"
          class="absolute top-0 right-0 pr-4 pt-4 z-10"
        >
          <span
            class="w-6 h-6 relative block text-center leading-tight bg-white border border-gray-300 text-black rounded-full"
          >
            <font-awesome :icon="['fas', 'star']" size="xs"></font-awesome>
          </span>
        </div>
        <g-image
          :src="record.image"
          :alt="record.title"
          class="post-card-image"
        ></g-image>
      </g-link>
      <div
        class="post-card-content bg-white dark:bg-gray-900 h-full rounded-b-lg"
      >
        <div class="flex-col relative flex justify-between px-6 pt-4">
          <p class="text-xs tracking-wide font-medium mt-3 dark:text-white">
            <g-link :to="record.category.path">{{
              record.category.title
            }}</g-link>
          </p>
        </div>
        <g-link
          :to="record.path"
          class="flex-col relative flex justify-between rounded-b-lg px-6 h-40 mt-2 dark:text-white"
        >
          <h3 class="post-card-title tracking-wide mt-0">{{ record.title }}</h3>

          <div class="text-xs leading-none absolute bottom-0 pb-6">
            <p>
              <time :datetime="record.datetime">{{ record.humanTime }}</time>
              &nbsp;&bull;&nbsp;
              {{ record.timeToRead }} min read
            </p>
          </div>
        </g-link>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    record: {}
  },
  computed: {
    authors() {
      let tooltipText = [];
      for (let index = 0; index < this.record.author.length; index++) {
        if (index == 0) {
          tooltipText.push(`Posted by ${this.record.author[index].name}`);
        } else {
          if (index == 1) {
            tooltipText.push(
              `<br> Among with ${this.record.author[index].name}`
            );
          } else {
            tooltipText.push(`, ${this.record.author[index].name}`);
          }
        }
      }

      return tooltipText.join("");
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# ContentHeader.vue {{{
	file=src/components/Partials/ContentHeader.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div
      class="z-100 text-center bg-gray-200 dark:bg-gray-900 py-10 md:py-20"
      v-if="!hasImage"
    >
      <h2 v-if="title != null" class="h1 font-extrabold dark:text-gray-400">
        {{ title }}
      </h2>
      <p v-if="sub != null" class="text-gray-600 text-light font-sans">
        {{ sub }}
      </p>
    </div>

    <div v-if="hasImage" class="z-100 relative mt-0 h-auto">
      <g-image
        v-if="hasImage && staticImage"
        :src="require(`!!assets-loader!@pageImage/${image}`)"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <g-image
        v-if="hasImage && !staticImage"
        :src="image"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <slot>
        <div
          class="text-center text-white bg-gray-800 lg:py-48 md:py-32 py-24"
          :class="`bg-opacity-${opacity}`"
        >
          <h2 v-if="title != null" class="h1 font-extrabold">{{ title }}</h2>
          <p v-if="sub != null" class="h5 font-sans">{{ sub }}</p>
        </div>
      </slot>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    title: {
      type: String,
      default: null
    },
    sub: {
      type: String,
      default: null
    },
    image: {
      type: String | Object,
      default: null
    },
    staticImage: {
      type: Boolean,
      default: true
    },
    opacity: {
      type: Number,
      default: 50
    }
  },
  computed: {
    hasImage() {
      return this.image ? true : false;
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# PAGES:
  # TODO: blog, nav
	# Index.vue {{{
	file=src/pages/Index.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :title="$static.metadata.siteName"
      :sub="$static.metadata.siteDescription"
      image="danil_silantev_F6Da4r2x5to.jpg"
    >
    </content-header>

    {{ $page.entries.edges }}

    <div class="container mx-auto">
      <div class="flex flex-wrap my-4">
        <CardItem
          v-for="edge in $page.entries.edges"
          :key="edge.node.id"
          :record="edge.node"
        />
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($page: Int) {
    entries: allBlog(perPage: 24, page: $page, sortBy:"created") @paginate {
      totalCount
      pageInfo {
        totalPages
        currentPage
      }
      edges {
        node {
          id
          title
          image(width: 800)
          path
          timeToRead
          featured
          humanTime: created(format: "DD MMM YYYY")
          datetime: created
          category {
            id
            title
            path
          }
          author {
            id
            name
            image(width: 64, height: 64, fit: inside)
            path
          }
        }
      }
    }
  }
</page-query>

<static-query>
query {
  metadata {
    siteName
    siteDescription
  }
}
</static-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";
import CardItem from "~/components/Content/CardItem.vue";

export default {
  metaInfo: {
    title: "Hello, world!"
  },
  components: {
    ContentHeader,
    CardItem,
  }
};
</script>
EOF
	# }}}
	# Templates:
	# BlogPost.vue {{{
	file=src/templates/BlogPost.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :image="$page.blog.image"
      :staticImage="false"
      :opacity="0"
    ></content-header>

    <div
      class="container sm:pxi-0 mx-auto overflow-x-hidden text-gray-800 dark:text-gray-500"
    >
      <div class="lg:mx-32 md:mx-16 sm:mx-8 mx-4 pt-8">
        <section class="post-header container mx-auto px-0 mb-16 text-center">
          <h1
            class="text-gray-800 dark:text-gray-400 font-extrabold tracking-wider mb-6"
          >
            {{ $page.blog.title }}
          </h1>
          <span class="tracking-wide text-sm">
            <g-link class="font-medium" :to="$page.blog.category.path">{{
              $page.blog.category.title
            }}</g-link
            >&nbsp;&middot;&nbsp;
            <time :datetime="$page.blog.datetime">{{
              $page.blog.humanTime
            }}</time>
            &nbsp;&middot;&nbsp;
            {{ $page.blog.timeToRead }} min read
          </span>
        </section>
      </div>

      <div class="lg:mx-32 md:mx-16 px-4">
        <section class="post-content container mx-auto relative">
          <div v-html="$page.blog.content"></div>
        </section>

        <section class="post-tags container mx-auto relative py-10">
          <g-link
            v-for="tag in $page.blog.tags"
            :key="tag.id"
            :to="tag.path"
            class="text-xs bg-transparent hover:text-blue-700 py-2 px-4 mr-2 border hover:border-blue-500 border-gray-600 text-gray-700 dark:text-gray-400 rounded-full"
            >{{ tag.title }}</g-link
          >
        </section>
      </div>
    </div>

    <div
      class="border-t border-b bg-gray-100 dark:border-black dark:bg-gray-900 dark:text-gray-500"
    >
      <div class="container mx-auto">
        <div class="lg:mx-32 md:mx-16 px-4 sm:px-0">
          <section class="container mx-auto py-10">
            <div class="flex flex-wrap justify-center">
              <div
                class="w-full flex justify-center md:w-10/12 mb-4 text-center"
              >
                <div class="mb-2 sm:mb-0 w-full">
                  <div class="md:flex p-6 pl-0 self-center">
                    <g-image
                      :src="$page.blog.author[0].image"
                      class="h-16 w-16 md:h-24 md:w-24 mx-auto md:mx-0 md:mr-6 rounded-full bg-gray-200"
                    ></g-image>

                    <div class="text-center md:text-left">
                      <g-link
                        :to="$page.blog.author[0].path"
                        class="text-black dark:text-white"
                      >
                        <h2 class="text-lg my-1 mt-2 md:mt-0">
                          {{ $page.blog.author[0].name }}
                        </h2>
                      </g-link>
                      <div
                        v-if="authors.length > 0"
                        class="post-authors font-light text-sm pt-2"
                      >
                        Among with
                        <g-link
                          class="font-normal"
                          :to="author.path"
                          v-for="author in authors"
                          :key="author.name"
                          >{{ author.name }}</g-link
                        >
                      </div>
                      <div
                        class="font-light tracking-wider leading-relaxed py-4"
                      >
                        {{ $page.blog.author[0].bio }}
                      </div>
                      <div class>
                        <a
                          :href="$page.blog.author[0].facebook"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500"
                        >
                          <font-awesome :icon="['fab', 'facebook']" />
                        </a>
                        &nbsp;
                        <a
                          :href="$page.blog.author[0].twitter"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500"
                        >
                          <font-awesome :icon="['fab', 'twitter']" />
                        </a>
                        &nbsp;
                        <a
                          :href="$page.blog.author[0].linkedin"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500"
                        >
                          <font-awesome :icon="['fab', 'linkedin']" />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>

    <section
      class="post-related pt-10 border-b border-b-gray-900"
      v-if="relatedRecords.length > 0"
    >
      <div class="container mx-auto">
        <div class="text-center">
          <h4 class="font-light my-0">Recommended for you</h4>
        </div>
        <div class="flex flex-wrap justify-center pt-8 pb-8">
          <CardItem
            :record="relatedRecord.node"
            v-for="relatedRecord in relatedRecords"
            :key="relatedRecord.node.id"
          ></CardItem>
        </div>
      </div>
    </section>
  </Layout>
</template>

<page-query>
  query($recordId: ID!, $tags: [ID]) {
    blog(id: $recordId) {
      title
      path
      image(width:1600, height:800)
      image_caption
      excerpt
      content
      humanTime : created(format:"DD MMMM YYYY")
      datetime : created(format:"ddd MMM DD YYYY hh:mm:ss zZ")
      
      timeToRead
      tags {
        id
        title
        path
      }
      category {
        id
        title
        path
        belongsTo(limit:4) {
          totalCount
          edges {
            node {
              ... on Blog {
                title
                path
              }
            }
          }
        }
      }
      author {
        id
        name
        image
        path
        bio
      }
    }

    related: allBlog(
      filter: { id: { ne: $recordId }, tags: {containsAny: $tags} }
    ) {
      edges {
        node {
          title
      path
      image(width:1600, height:800)
      image_caption
      excerpt
      content
      humanTime : created(format:"DD MMMM YYYY")
      datetime : created(format:"ddd MMM DD YYYY hh:mm:ss zZ")
      
      timeToRead
      tags {
        id
        title
        path
      }
      category {
        id
        title
        path
        belongsTo(limit:4) {
          totalCount
          edges {
            node {
              ... on Blog {
                title
                path
              }
            }
          }
        }
      }
      author {
        id
        name
        image
        path
      }
        }
      }
    }


    
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";
import { sampleSize } from "lodash";

export default {
  components: {
    CardItem,
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.blog.title
    };
  },
  computed: {
    relatedRecords() {
      return sampleSize(this.$page.related.edges, 2);
    },
    authors() {
      let authors = [];
      for (let index = 1; index < this.$page.blog.author.length; index++) {
        authors.push({
          name: this.$page.blog.author[index].name,
          path: this.$page.blog.author[index].path
        });
      }

      return authors;
    }
  },
  mounted() {
    mediumZoom(".post-content img");
  }
};
</script>
EOF
	# }}}
}        #}}}
prob() { #{{{
	echo hi
}         #}}}
merge() { #{{{
  # gridsome.server.js {{{
	file=gridsome.server.js
mkdir -p "$(dirname $file)"
cat <<'EOF'> $file
const _ = require("lodash");

module.exports = function(api) {
  api.loadSource(({ addCollection }) => {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/
  });

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  });

  api.onCreateNode(options => {
    if (options.internal.typeName === "Blog") {
      options.featured = options.featured ? options.featured : false;
      options.tags =
        typeof options.tags === "string"
          ? options.tags.split(",").map(string => string.trim())
          : options.tags;
      options.author =
        typeof options.author === "string"
          ? options.author.split(",").map(string => string.trim())
          : options.author;
      return {
        ...options
      };
    }
    if (options.internal.typeName === "CustomPage") {
      options.subtitle = options.subtitle || "";
    }
  });

  api.createPages(async ({ graphql, createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api
    const { data } = await graphql(`
      {
        allBlog {
          edges {
            node {
              id
              path
              tags {
                title
              }
            }
          }
        }
      }
    `);

    data.allBlog.edges.forEach(({ node }) => {
      //without the map, you will get an 500 error
      //because the graphql filter requires an array
      //not an object
      var tags = _.map(node.tags, function(tag) {
        return tag.title;
      });

      createPage({
        path: node.path,
        component: "./src/templates/BlogPost.vue",
        context: {
          recordId: node.id,
          tags: tags
        }
      });
    });
  });
};
EOF
  # }}}
	# Layouts
	# Default.vue {{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div id="app" class="dark:bg-black">
    <navbar @setTheme="setTheme" :theme="this.theme"></navbar>

    <slot />

    <v-footer></v-footer>
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<script>
import Navbar from "~/components/Navbar/Navbar.vue";
import VFooter from "~/components/Partials/Footer.vue";
export default {
  data: function() {
    return {
      theme: "light"
    };
  },
  components: {
    Navbar,
    VFooter
  },
  methods: {
    setTheme(mode) {
      this.theme = mode;
    }
  }
};
</script>
EOF
	# }}}
	# Pages:
	# Index.vue {{{
	file=src/pages/Index.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :title="$static.metadata.siteName"
      :sub="$static.metadata.siteDescription"
      image="phoenix-han-Nqdh0G8rdCc-unsplash.jpg"
    >
    </content-header>

    <div class="container mx-auto">
      <div class="flex flex-wrap my-4">
        <FeaturedCard
          v-if="$page.featured.totalCount > 0"
          :records="$page.featured.edges"
        />

        <CardItem
          v-for="edge in $page.entries.edges"
          :key="edge.node.id"
          :record="edge.node"
        />
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($page: Int) {
    featured: allBlog(limit: 4, filter: { featured: { eq: true } }, sortBy:"created") {
      totalCount
      edges {
        node {
          id
          title
          image(width: 800)
          path
          timeToRead
          humanTime: created(format: "DD MMM YYYY")
          datetime: created
          category {
            id
            title
            path
          }
          author {
            id
            name
            image(width: 64, height: 64, fit: inside)
            path
          }
        }
      }
    }
    entries: allBlog(perPage: 24, page: $page, sortBy:"created") @paginate {
      totalCount
      pageInfo {
        totalPages
        currentPage
      }
      edges {
        node {
          id
          title
          image(width: 800)
          path
          timeToRead
          featured
          humanTime: created(format: "DD MMM YYYY")
          datetime: created
          category {
            id
            title
            path
          }
          author {
            id
            name
            image(width: 64, height: 64, fit: inside)
            path
          }
        }
      }
    }
  }
</page-query>

<static-query>
query {
  metadata {
    siteName
    siteDescription
  }
}
</static-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import FeaturedCard from "~/components/Content/FeaturedCard.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  metaInfo: {
    title: "Hello, world!"
  },
  components: {
    CardItem,
    FeaturedCard,
    ContentHeader
  }
};
</script>
EOF
	# }}}
	# Templates:
	# BlogPost.vue {{{
	file=src/templates/BlogPost.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :image="$page.blog.image"
      :staticImage="false"
      :opacity="0"
    ></content-header>

    <div
      class="container sm:pxi-0 mx-auto overflow-x-hidden text-gray-800 dark:text-gray-500"
    >
      <div class="lg:mx-32 md:mx-16 sm:mx-8 mx-4 pt-8">
        <section class="post-header container mx-auto px-0 mb-16 text-center">
          <h1
            class="text-gray-800 dark:text-gray-400 font-extrabold tracking-wider mb-6"
          >
            {{ $page.blog.title }}
          </h1>
          <span class="tracking-wide text-sm">
            <g-link class="font-medium" :to="$page.blog.category.path">{{
              $page.blog.category.title
            }}</g-link
            >&nbsp;&middot;&nbsp;
            <time :datetime="$page.blog.datetime">{{
              $page.blog.humanTime
            }}</time>
            &nbsp;&middot;&nbsp;
            {{ $page.blog.timeToRead }} min read
          </span>
        </section>
      </div>

      <div class="lg:mx-32 md:mx-16 px-4">
        <section class="post-content container mx-auto relative">
          <div v-html="$page.blog.content"></div>
        </section>

        <section class="post-tags container mx-auto relative py-10">
          <g-link
            v-for="tag in $page.blog.tags"
            :key="tag.id"
            :to="tag.path"
            class="text-xs bg-transparent hover:text-blue-700 py-2 px-4 mr-2 border hover:border-blue-500 border-gray-600 text-gray-700 dark:text-gray-400 rounded-full"
            >{{ tag.title }}</g-link
          >
        </section>
      </div>
    </div>

    <div
      class="border-t border-b bg-gray-100 dark:border-black dark:bg-gray-900 dark:text-gray-500"
    >
      <div class="container mx-auto">
        <div class="lg:mx-32 md:mx-16 px-4 sm:px-0">
          <section class="container mx-auto py-10">
            <div class="flex flex-wrap justify-center">
              <div
                class="w-full flex justify-center md:w-10/12 mb-4 text-center"
              >
                <div class="mb-2 sm:mb-0 w-full">
                  <div class="md:flex p-6 pl-0 self-center">
                    <g-image
                      :src="$page.blog.author[0].image"
                      class="h-16 w-16 md:h-24 md:w-24 mx-auto md:mx-0 md:mr-6 rounded-full bg-gray-200"
                    ></g-image>

                    <div class="text-center md:text-left">
                      <g-link
                        :to="$page.blog.author[0].path"
                        class="text-black dark:text-white"
                      >
                        <h2 class="text-lg my-1 mt-2 md:mt-0">
                          {{ $page.blog.author[0].name }}
                        </h2>
                      </g-link>
                      <div
                        v-if="authors.length > 0"
                        class="post-authors font-light text-sm pt-2"
                      >
                        Among with
                        <g-link
                          class="font-normal"
                          :to="author.path"
                          v-for="author in authors"
                          :key="author.name"
                          >{{ author.name }}</g-link
                        >
                      </div>
                      <div
                        class="font-light tracking-wider leading-relaxed py-4"
                      >
                        {{ $page.blog.author[0].bio }}
                      </div>
                      <div class>
                        <a
                          :href="$page.blog.author[0].facebook"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500"
                        >
                          <font-awesome :icon="['fab', 'facebook']" />
                        </a>
                        &nbsp;
                        <a
                          :href="$page.blog.author[0].twitter"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500"
                        >
                          <font-awesome :icon="['fab', 'twitter']" />
                        </a>
                        &nbsp;
                        <a
                          :href="$page.blog.author[0].linkedin"
                          target="_blank"
                          rel="noopener noreferrer"
                          class="hover:text-blue-500"
                        >
                          <font-awesome :icon="['fab', 'linkedin']" />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>

    <section
      class="post-related pt-10 border-b border-b-gray-900"
      v-if="relatedRecords.length > 0"
    >
      <div class="container mx-auto">
        <div class="text-center">
          <h4 class="font-light my-0">Recommended for you</h4>
        </div>
        <div class="flex flex-wrap justify-center pt-8 pb-8">
          <CardItem
            :record="relatedRecord.node"
            v-for="relatedRecord in relatedRecords"
            :key="relatedRecord.node.id"
          ></CardItem>
        </div>
      </div>
    </section>
  </Layout>
</template>

<page-query>
  query($recordId: ID!, $tags: [ID]) {
    blog(id: $recordId) {
      title
      path
      image(width:1600, height:800)
      image_caption
      excerpt
      content
      humanTime : created(format:"DD MMMM YYYY")
      datetime : created(format:"ddd MMM DD YYYY hh:mm:ss zZ")
      
      timeToRead
      tags {
        id
        title
        path
      }
      category {
        id
        title
        path
        belongsTo(limit:4) {
          totalCount
          edges {
            node {
              ... on Blog {
                title
                path
              }
            }
          }
        }
      }
      author {
        id
        name
        image
        path
        bio
      }
    }

    related: allBlog(
      filter: { id: { ne: $recordId }, tags: {containsAny: $tags} }
    ) {
      edges {
        node {
          title
      path
      image(width:1600, height:800)
      image_caption
      excerpt
      content
      humanTime : created(format:"DD MMMM YYYY")
      datetime : created(format:"ddd MMM DD YYYY hh:mm:ss zZ")
      
      timeToRead
      tags {
        id
        title
        path
      }
      category {
        id
        title
        path
        belongsTo(limit:4) {
          totalCount
          edges {
            node {
              ... on Blog {
                title
                path
              }
            }
          }
        }
      }
      author {
        id
        name
        image
        path
      }
        }
      }
    }


    
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";
import { sampleSize } from "lodash";

export default {
  components: {
    CardItem,
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.blog.title
    };
  },
  computed: {
    relatedRecords() {
      return sampleSize(this.$page.related.edges, 2);
    },
    authors() {
      let authors = [];
      for (let index = 1; index < this.$page.blog.author.length; index++) {
        authors.push({
          name: this.$page.blog.author[index].name,
          path: this.$page.blog.author[index].path
        });
      }

      return authors;
    }
  },
  mounted() {
    mediumZoom(".post-content img");
  }
};
</script>
EOF
	# }}}
	# Tag.vue {{{
	file=src/templates/Tag.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header :title="$page.tag.title" :sub="subTitle"></content-header>

    <div class="container mx-auto">
      <div class="flex flex-wrap my-4">
        <CardItem
          v-for="edge in $page.tag.belongsTo.edges"
          :key="edge.node.id"
          :record="edge.node"
        />
      </div>

      <div class="pagination flex justify-center mb-8">
        <Pagination
          :baseUrl="$page.tag.path"
          :currentPage="$page.tag.belongsTo.pageInfo.currentPage"
          :totalPages="$page.tag.belongsTo.pageInfo.totalPages"
          :maxVisibleButtons="5"
          v-if="$page.tag.belongsTo.pageInfo.totalPages > 1"
        />
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!, $page:Int) {
    tag(id: $id) {
      title
      path
      belongsTo(perPage: 3, page: $page) @paginate {
        totalCount
        pageInfo {
          totalPages
          currentPage
        }
        edges {
          node {
            ... on Blog {
              id
              title
              image(width: 800)
              path
              timeToRead
              featured
              humanTime: created(format: "DD MMM YYYY")
              datetime: created
              category {
                id
                title
                path
              }
              author {
                id
                name
                image(width: 64, height: 64, fit: inside)
                path
              }
            }
          }
        }
      }
    }  
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import Pagination from "~/components/Content/Pagination.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    Pagination,
    CardItem,
    ContentHeader
  },
  computed: {
    postLabel: function() {
      var pluralize = require("pluralize");
      return pluralize("post", this.$page.tag.belongsTo.totalCount);
    },
    subTitle: function() {
      return `A collection of ${this.$page.tag.belongsTo.totalCount} ${
        this.postLabel
      }`;
    }
  },
  metaInfo() {
    return {
      title: this.$page.tag.title
    };
  }
};
</script>
EOF
	# }}}
	# CustomPage.vue {{{
	file=src/templates/CustomPage.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :title="$page.page.title"
      :sub="$page.page.subtitle"
    ></content-header>

    <div
      class="container sm:pxi-0 mx-auto overflow-x-hidden text-gray-800 dark:text-gray-400"
    >
      <div class="lg:mx-32 md:mx-16 px-4 mb-8">
        <section
          class="post-content container mx-auto relative dark:text-gray-400"
        >
          <div v-html="$page.page.content"></div>
        </section>
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!) {
    page: customPage(id: $id) {
      title
      subtitle
      content
    }
  }
</page-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.page.title
    };
  },
  mounted() {
    mediumZoom(".post-content img");
  }
};
</script>
EOF
	# }}}
	# Category.vue {{{
	file=src/templates/Category.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :title="$page.category.title"
      :sub="subTitle"
    ></content-header>

    <div class="container mx-auto">
      <div class="flex flex-wrap my-4">
        <CardItem
          v-for="edge in $page.category.belongsTo.edges"
          :key="edge.node.id"
          :record="edge.node"
        />
      </div>

      <div class="pagination flex justify-center mb-8">
        <Pagination
          :baseUrl="$page.category.path"
          :currentPage="$page.category.belongsTo.pageInfo.currentPage"
          :totalPages="$page.category.belongsTo.pageInfo.totalPages"
          :maxVisibleButtons="5"
          v-if="$page.category.belongsTo.pageInfo.totalPages > 1"
        />
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!, $page:Int) {
    category(id: $id) {
      title
      path
      belongsTo(perPage: 6, page: $page) @paginate {
        totalCount
        pageInfo {
          totalPages
          currentPage
        }
        edges {
          node {
            ... on Blog {
              id
              title
              image(width: 800)
              path
              timeToRead
              featured
              humanTime: created(format: "DD MMM YYYY")
              datetime: created
              category {
                id
                title
                path
              }
              author {
                id
                name
                image(width: 64, height: 64, fit: inside)
                path
              }
            }
          }
        }
      }
    }  
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import Pagination from "~/components/Content/Pagination.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    Pagination,
    CardItem,
    ContentHeader
  },
  computed: {
    postLabel: function() {
      var pluralize = require("pluralize");
      return pluralize("post", this.$page.category.belongsTo.totalCount);
    },
    subTitle: function() {
      return `A collection of ${this.$page.category.belongsTo.totalCount} ${
        this.postLabel
      }`;
    }
  },
  metaInfo() {
    return {
      title: this.$page.category.title
    };
  }
};
</script>
EOF
	# }}}
	# Author.vue {{{
	file=src/templates/Author.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header :image="$page.author.cover" :staticImage="false">
      <div
        class="text-center text-white bg-gray-800 bg-opacity-50 lg:py-32 md:py-24 sm:py-16 py-8"
      >
        <div class="w-full">
          <g-image
            :src="$page.author.image"
            width="100"
            height="100"
            class="md:h-32 md:w-32 h-24 w-24 rounded-full bg-white border-4 border-white mx-auto"
          ></g-image>
        </div>
        <div class="w-full text-center pb-5">
          <h2 class="sm:text-5xl text-3xl font-extrabold">
            {{ $page.author.name }}
          </h2>
          <p class="sm:text-xl font-sans">{{ $page.author.bio }}</p>
        </div>
        <div class="w-full text-center">
          {{ $page.author.belongsTo.totalCount }} {{ postLabel }}
          &nbsp;&bull;&nbsp;
          <a
            :href="$page.author.facebook"
            target="_blank"
            rel="noopener noreferrer"
            class="text-gray-400 hover:text-white"
          >
            <font-awesome :icon="['fab', 'facebook']" />
          </a>
          &nbsp;
          <a
            :href="$page.author.twitter"
            target="_blank"
            rel="noopener noreferrer"
            class="text-gray-400 hover:text-white"
          >
            <font-awesome :icon="['fab', 'twitter']" />
          </a>
          &nbsp;
          <a
            :href="$page.author.linkedin"
            target="_blank"
            rel="noopener noreferrer"
            class="text-gray-400 hover:text-white"
          >
            <font-awesome :icon="['fab', 'linkedin']" />
          </a>
        </div>
      </div>
    </content-header>

    <div class="container mx-auto">
      <div class="flex flex-wrap my-4">
        <CardItem
          v-for="edge in $page.author.belongsTo.edges"
          :key="edge.node.id"
          :record="edge.node"
        />
      </div>

      <div class="pagination flex justify-center mb-8">
        <Pagination
          :baseUrl="$page.author.path"
          :currentPage="$page.author.belongsTo.pageInfo.currentPage"
          :totalPages="$page.author.belongsTo.pageInfo.totalPages"
          :maxVisibleButtons="5"
          v-if="$page.author.belongsTo.pageInfo.totalPages > 1"
        />
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($id: ID!, $page:Int) {
    author(id: $id) {
      name
      path
      bio
      image(width:150, height:150)
      cover
      facebook
      twitter
      linkedin
      belongsTo(perPage: 6, page: $page) @paginate {
        totalCount
        pageInfo {
          totalPages
          currentPage
        }
        edges {
          node {
            ... on Blog {
              id
              title
              image(width: 800)
              path
              timeToRead
              featured
              humanTime: created(format: "DD MMM YYYY")
              datetime: created
              category {
                id
                title
                path
              }
              author {
                id
                name
                image(width: 64, height: 64, fit: inside)
                path
              }
            }
          }
        }
      }
    }  
  }
</page-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import Pagination from "~/components/Content/Pagination.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    Pagination,
    CardItem,
    ContentHeader
  },
  computed: {
    postLabel: function() {
      var pluralize = require("pluralize");
      return pluralize("post", this.$page.author.belongsTo.totalCount);
    }
  },
  metaInfo() {
    return {
      title: this.$page.author.name
    };
  }
};
</script>
EOF
	# }}}
	# components/content:
	# CardItem.vue {{{
	file=src/components/Content/CardItem.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="w-full md:w-1/2 lg:w-1/3 px-4 my-4">
    <div
      class="post-card border-gray-200 dark:border-gray-900 bg-white dark:bg-black rounded-lg hover:shadow-xl z-100 dark:bg-gray-900"
      :id="record.id"
    >
      <div
        class="post-card-author pt-4 pl-4"
        v-tooltip="{
          classes: 'card-author-tooltip',
          content: this.authors,
          placement: 'right'
        }"
      >
        <g-link
          :to="record.author[0].path"
          @mouseover="showTooltip = true"
          @mouseleave="showTooltip = false"
          @mouseover.native="showTooltip = true"
          @mouseleave.native="showTooltip = false"
        >
          <g-image
            :src="record.author[0].image"
            :alt="record.author[0].name"
            class="w-8 h-8 rounded-full bg-gray-200 border-2 border-white"
          />
        </g-link>
      </div>

      <g-link :to="record.path" class="post-card-image-link">
        <div
          v-if="record.featured"
          class="absolute top-0 right-0 pr-4 pt-4 z-10"
        >
          <span
            class="w-6 h-6 relative block text-center leading-tight bg-white border border-gray-300 text-black rounded-full"
          >
            <font-awesome :icon="['fas', 'star']" size="xs"></font-awesome>
          </span>
        </div>
        <g-image
          :src="record.image"
          :alt="record.title"
          class="post-card-image"
        ></g-image>
      </g-link>
      <div
        class="post-card-content bg-white dark:bg-gray-900 h-full rounded-b-lg"
      >
        <div class="flex-col relative flex justify-between px-6 pt-4">
          <p class="text-xs tracking-wide font-medium mt-3 dark:text-white">
            <g-link :to="record.category.path">{{
              record.category.title
            }}</g-link>
          </p>
        </div>
        <g-link
          :to="record.path"
          class="flex-col relative flex justify-between rounded-b-lg px-6 h-40 mt-2 dark:text-white"
        >
          <h3 class="post-card-title tracking-wide mt-0">{{ record.title }}</h3>

          <div class="text-xs leading-none absolute bottom-0 pb-6">
            <p>
              <time :datetime="record.datetime">{{ record.humanTime }}</time>
              &nbsp;&bull;&nbsp;
              {{ record.timeToRead }} min read
            </p>
          </div>
        </g-link>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    record: {}
  },
  computed: {
    authors() {
      let tooltipText = [];
      for (let index = 0; index < this.record.author.length; index++) {
        if (index == 0) {
          tooltipText.push(`Posted by ${this.record.author[index].name}`);
        } else {
          if (index == 1) {
            tooltipText.push(
              `<br> Among with ${this.record.author[index].name}`
            );
          } else {
            tooltipText.push(`, ${this.record.author[index].name}`);
          }
        }
      }

      return tooltipText.join("");
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# FeaturedCard.vue {{{
	file=src/components/Content/FeaturedCard.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="w-full lg:w-2/3 px-4 my-4 items-stretch">
    <div
      class="featured-post-card bg-gray-200 rounded-lg hover:shadow-xl z-100 h-half-screen lg:h-full"
    >
      <VueSlickCarousel :arrows="true" :dots="false" class="h-full rounded-lg">
        <div
          class="h-full relative"
          v-for="edge in records"
          :key="edge.node.id"
        >
          <g-link :to="edge.node.path">
            <div class="h-full relative" :id="edge.node.id">
              <g-image
                :src="edge.node.image"
                :alt="edge.node.title"
                class="rounded-lg object-cover absolute -z-10 h-full w-full"
              ></g-image>

              <div
                class="featured-label absolute top-0 right-0 pr-10 pt-10 z-10"
              >
                <span
                  class="p-1 relative block text-center text-xs leading-tight bg-white border border-gray-300 text-black rounded-full"
                >
                  <font-awesome :icon="['fas', 'star']"></font-awesome>Featured
                </span>
              </div>

              <div
                class="post-card-author pt-4 mt-6 pl-10"
                v-tooltip="{
                  classes: 'card-author-tooltip',
                  content: authors(edge.node),
                  placement: 'right'
                }"
              >
                <g-link
                  :to="edge.node.author[0].path"
                  @mouseover="showTooltip = true"
                  @mouseleave="showTooltip = false"
                  @mouseover.native="showTooltip = true"
                  @mouseleave.native="showTooltip = false"
                >
                  <g-image
                    :src="edge.node.author[0].image"
                    :alt="edge.node.author[0].name"
                    class="w-10 h-10 rounded-full bg-gray-200 border-2 border-white"
                  />
                </g-link>
              </div>

              <div class="post-card-content">
                <p class="tracking-wide font-semibold mt-3">
                  <g-link :to="edge.node.category.path">{{
                    edge.node.category.title
                  }}</g-link>
                </p>

                <h3 class="tracking-wider mt-3 mb-3 text-4xl max-w-xl">
                  {{ edge.node.title }}
                </h3>
              </div>

              <div class="post-card-footer">
                <p>
                  <time :datetime="edge.node.datetime">{{
                    edge.node.humanTime
                  }}</time>
                  &nbsp;&bull;&nbsp;
                  {{ edge.node.timeToRead }} min read
                </p>
              </div>
            </div>
          </g-link>
        </div>
        <template #prevArrow>
          <div class="w-16 h-16 mr-10 z-40">
            <font-awesome
              :icon="['fas', 'arrow-left']"
              size="lg"
            ></font-awesome>
          </div>
        </template>
        <template #nextArrow>
          <div class="w-16 h-16 z-50">
            <font-awesome
              :icon="['fas', 'arrow-right']"
              size="lg"
            ></font-awesome>
          </div>
        </template>
      </VueSlickCarousel>
    </div>
  </div>
</template>

<script>
import VueSlickCarousel from "vue-slick-carousel";

export default {
  props: {
    records: {}
  },
  components: {
    VueSlickCarousel
  },
  methods: {
    authors(record) {
      let tooltipText = [];
      for (let index = 0; index < record.author.length; index++) {
        if (index == 0) {
          tooltipText.push(`Posted by ${record.author[index].name}`);
        } else {
          if (index == 1) {
            tooltipText.push(`<br> Among with ${record.author[index].name}`);
          } else {
            tooltipText.push(`, ${record.author[index].name}`);
          }
        }
      }

      return tooltipText.join("");
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# Pagination.vue {{{
	file=src/components/Content/Pagination.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <ul class="flex pl-0 list-none rounded my-2">
    <li v-if="!isFirstPage(currentPage, totalPages)">
      <g-link
        :to="previousPage(currentPage, totalPages)"
        class="w-10 relative block py-2 text-center dark:text-gray-400 dark:border-gray-800 dark-hover:bg-gray-700 dark:bg-gray-800 leading-tight bg-white border border-gray-300 text-black ml-1 rounded-full hover:bg-gray-300 focus:outline-none"
        tabindex="-1"
      >
        <font-awesome :icon="['fas', 'arrow-left']"></font-awesome>
      </g-link>
    </li>

    <li class="self-center mx-4 dark:text-gray-400 ">
      Page {{ currentPage }} of {{ totalPages }}
    </li>

    <li v-if="!isLastPage(currentPage, totalPages)">
      <g-link
        :to="nextPage(currentPage, totalPages)"
        class="w-10 relative block py-2 text-center dark:text-gray-400 dark:border-gray-800 dark-hover:bg-gray-700 dark:bg-gray-800 leading-tight bg-white border border-gray-300 text-black ml-1 rounded-full hover:bg-gray-300 focus:outline-none"
        tabindex="-1"
      >
        <font-awesome :icon="['fas', 'arrow-right']"></font-awesome>
      </g-link>
    </li>
  </ul>
</template>

<script>
export default {
  props: {
    baseUrl: String,
    currentPage: Number,
    totalPages: Number,
    maxVisibleButtons: {
      type: Number,
      required: false,
      default: 3
    }
  },
  methods: {
    isFirstPage(currentPage, totalPages) {
      return currentPage == 1;
    },
    isLastPage(currentPage, totalPages) {
      return currentPage == totalPages;
    },
    isCurrentPage(currentPage, pageElement) {
      return currentPage == pageElement;
    },
    nextPage(currentPage, totalPages) {
      return `${this.baseUrl}/${currentPage + 1}`;
    },
    previousPage(currentPage, totalPages) {
      return currentPage === 2
        ? `${this.baseUrl}/`
        : `${this.baseUrl}/${currentPage - 1}`;
    }
  },
  computed: {
    startPage() {
      if (this.currentPage === 1) {
        return 1;
      }
      if (this.currentPage === this.totalPages) {
        return this.currentPage - 1;
      }
      return this.currentPage - 1;
    },
    pages() {
      const range = [];
      for (
        let i = this.startPage;
        i <=
        Math.min(this.startPage + this.maxVisibleButtons - 1, this.totalPages);
        i += 1
      ) {
        range.push({
          name: i,
          isDisabled: i === this.currentPage,
          link: i === 1 ? `${this.baseUrl}/` : `${this.baseUrl}/${i}`
        });
      }
      return range;
    }
  }
};
</script>
EOF
	# }}}
	# components/Modal:
	# Modal.vue {{{
	file=src/components/Modal/Modal.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Transition name="fade">
    <div
      v-if="showModal"
      class="fixed inset-0 w-full h-full overflow-y-auto overflow-x-hidden flex items-center justify-center dark:bg-black bg-white z-1000"
      @click.self="close"
    >
      <div class="relative w-full h-full dark:bg-black bg-white p-8">
        <button
          aria-label="close"
          class="absolute mr-8 right-0 text-xl border rounded-full dark:border-gray-600 border-gray-700 text-gray-700 dark:text-gray-600 hover:bg-gray-700 dark-hover:bg-gray-600 hover:text-white dark-hover:text-black h-8 w-8 md:h-12 md:w-12 focus:outline-none"
          @click.prevent="close"
        >
          <font-awesome :icon="['fas', 'times']"></font-awesome>
        </button>
        <div class="mt-12 md:mt-16 overflow-y-auto">
          <slot />
        </div>
      </div>
    </div>
  </Transition>
</template>

<script>
export default {
  props: {
    showModal: {
      required: true,
      type: Boolean
    }
  },
  methods: {
    close() {
      this.$emit("close");
    }
  }
};
</script>
EOF
	# }}}
	# NavbarMobileModal.vue {{{
	file=src/components/Modal/NavbarMobileModal.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div class="w-full mb-2 overflow-x-hidden dark:text-gray-400">
      <h2 class="text-xl my-0">Navigation</h2>
      <div class="menu-links">
        <ul>
          <li
            v-for="navItem in $static.metadata.headerNavigation"
            :key="navItem.name"
            class="py-1"
          >
            <g-link
              class="block py-1"
              :to="navItem.link"
              :title="navItem.name"
              v-if="navItem.external != true && navItem.children.length <= 0"
              >{{ navItem.name }}</g-link
            >
            <a
              class="block"
              :href="navItem.link"
              target="_blank"
              :title="navItem.name"
              v-if="navItem.external == true && navItem.children.length <= 0"
              >{{ navItem.name }}</a
            >
            <ClientOnly>
              <v-popover
                placement="right"
                popoverClass="mobile-navbar-popover"
                offset="16"
                v-if="navItem.children.length > 0"
              >
                <a class="block py-1" style="cursor:pointer;">
                  {{ navItem.name }}
                  <font-awesome :icon="['fas', 'angle-right']"></font-awesome>
                </a>

                <template slot="popover">
                  <ul>
                    <li
                      v-for="subItem in navItem.children"
                      :key="subItem.name"
                      class="px-4 py-2 submenu-item hover:text-white"
                    >
                      <g-link
                        class="block"
                        :to="subItem.link"
                        :title="subItem.name"
                        v-if="subItem.external != true"
                        >{{ subItem.name }}</g-link
                      >
                      <a
                        class="block"
                        :href="subItem.link"
                        target="_blank"
                        :title="subItem.name"
                        v-if="subItem.external == true"
                        >{{ subItem.name }}</a
                      >
                    </li>
                  </ul>
                </template>
              </v-popover>
            </ClientOnly>
          </li>
        </ul>
      </div>
    </div>
    <div class="mobileSubnav pl-2 dark:text-gray-400">
      <subnavigation />
    </div>
  </div>
</template>

<static-query>
query {
   metadata {
    siteName
    headerNavigation {
      name
      link
      external
      children {
        name
        link
        external
      }
    }
  }
}
</static-query>
<script>
import Subnavigation from "~/components/Navbar/NavbarSubNavigation.vue";

export default {
  components: {
    Subnavigation
  }
};
</script>

<style></style>
EOF
	# }}}
	# SearchModal.vue {{{
	file=src/components/Modal/SearchModal.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div class="flex justify-center">
      <div class="search-form w-full md:w-8/12 lg:w-6/12">
        <div class="relative">
          <div class="flex flex-wrap items-stretch w-full mb-4 relative">
            <div class="flex -mr-px">
              <span
                class="flex items-center leading-normal rounded rounded-r-none border border-r-0 border-gray-500 px-3 whitespace-no-wrap text-gray-400 dark:bg-gray-900 dark:text-gray-700 dark:border-gray-700"
              >
                <font-awesome
                  :icon="['fas', 'search']"
                  size="lg"
                ></font-awesome>
              </span>
            </div>
            <input
              type="text"
              class="flex-shrink flex-grow flex-auto text-gray-700 dark:text-gray-600 leading-normal w-px flex-1 border h-12 text-xl md:h-16 md:text-3xl border-l-0 focus:outline-none focus:shadow-none border-gray-500 dark:bg-gray-900 dark:border-gray-700 rounded rounded-l-none px-3 relative"
              placeholder="Search..."
              id="search"
              v-model="searchTerm"
            />
          </div>
        </div>
      </div>
    </div>
    <div class="flex justify-center">
      <div class="search-results w-full">
        <div class="container px-5 py-12 md:py-12 mx-auto">
          <div class="flex flex-wrap -mx-4 -my-8">
            <div
              class="py-2 px-4 sm:w-2/4 md:w-1/3"
              v-for="resultEntry in searchResults"
              :key="resultEntry.id"
            >
              <g-link :to="resultEntry.path">
                <div
                  class="h-full flex items-start hover:bg-gray-200 dark:bg-gray-900 dark-hover:bg-gray-800 rounded-lg"
                >
                  <div class="flex-grow px-6">
                    <h2
                      class="tracking-widest text-xs title-font font-medium text-indigo-500 mb-1"
                    >
                      {{ resultEntry.node.category }}
                    </h2>
                    <h1
                      class="title-font text-xl font-medium text-gray-900 dark:text-gray-400 mb-3"
                    >
                      {{ resultEntry.title }}
                    </h1>
                    <p class="leading-relaxed mb-5 dark:text-gray-500">
                      {{ resultEntry.node.excerpt }}
                    </p>
                  </div>
                </div>
              </g-link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  data: () => ({
    searchTerm: ""
  }),
  computed: {
    searchResults() {
      const searchTerm = this.searchTerm;
      if (searchTerm.length < 3) return [];
      return this.$search.search({ query: searchTerm, limit: 5 });
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# components/Navbar:
	# Navbar.vue {{{
	file=src/components/Navbar/Navbar.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="h-16 dark:bg-black bg-white">
    <headroom
      :classes="{
        initial: 'headroom bg-white dark:bg-black border-b dark:border-gray-900'
      }"
      :downTolerance="10"
      :upTolerance="20"
      :offset="15"
      @unpin="navbarUnpinned = true"
      @pin="navbarUnpinned = false"
    >
      <navbar-desktop
        v-on="$listeners"
        @openSearchModal="openSearchModal"
        :theme="theme"
        :hideSubnav="this.navbarUnpinned"
      />

      <navbar-mobile
        @openSearchModal="openSearchModal"
        @openNavbarModal="openNavbarModal"
        v-on="$listeners"
        :theme="theme"
      />
    </headroom>

    <modal :showModal="this.showSearchModal" @close="closeSearchModal">
      <search-modal></search-modal>
    </modal>

    <modal :showModal="this.showNavbarModal" @close="closeNavbarModal">
      <navbar-modal></navbar-modal>
    </modal>
  </div>
</template>

<script>
import NavbarDesktop from "~/components/Navbar/NavbarDesktop.vue";
import NavbarMobile from "~/components/Navbar/NavbarMobile.vue";
import Modal from "~/components/Modal/Modal.vue";
import SearchModal from "~/components/Modal/SearchModal.vue";
import NavbarModal from "~/components/Modal/NavbarMobileModal.vue";
import { headroom } from "vue-headroom";

export default {
  props: {
    theme: {
      type: String
    }
  },
  data: function() {
    return {
      showSearchModal: false,
      showNavbarModal: false,
      headerHeight: 100,
      navbarUnpinned: false
    };
  },
  components: {
    NavbarDesktop,
    NavbarMobile,
    Modal,
    SearchModal,
    NavbarModal,
    headroom
  },
  methods: {
    openSearchModal() {
      this.showSearchModal = true;
    },
    closeSearchModal() {
      this.showSearchModal = false;
    },
    openNavbarModal() {
      this.showNavbarModal = true;
    },
    closeNavbarModal() {
      this.showNavbarModal = false;
    }
  },
  watch: {
    $route(to, from) {
      this.closeNavbarModal();
      this.closeSearchModal();
    }
  }
};
</script>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>
EOF
	# }}}
	# NavbarDesktop.vue {{{
	file=src/components/Navbar/NavbarDesktop.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <nav
    class="hidden md:block lg:block xl:block flex items-center justify-between flex-wrap container mx-auto py-3 z-20 dark:text-gray-400"
  >
    <div class="block flex-grow flex items-center w-auto mx-4">
      <div class="flex items-center flex-shrink-0 mr-6">
        <span class="font-semibold text-xl tracking-tight">{{
          $static.metadata.siteName
        }}</span>
      </div>
      <div class="flex-grow">
        <ul class="list-none flex justify-left">
          <li
            v-for="navItem in $static.metadata.headerNavigation"
            :key="navItem.name"
            class="px-4 py-1"
          >
            <g-link
              class="block py-1"
              :to="navItem.link"
              :title="navItem.name"
              v-if="navItem.external != true && navItem.children.length <= 0"
              >{{ navItem.name }}</g-link
            >
            <a
              class="block"
              :href="navItem.link"
              target="_blank"
              :title="navItem.name"
              v-if="navItem.external == true && navItem.children.length <= 0"
              >{{ navItem.name }}</a
            >
            <ClientOnly>
              <v-popover
                placement="top"
                popoverClass="navbar-popover"
                offset="16"
                v-if="navItem.children.length > 0"
              >
                <a class="block py-1" style="cursor:pointer;">
                  {{ navItem.name }}
                  <font-awesome :icon="['fas', 'angle-down']"></font-awesome>
                </a>

                <template slot="popover">
                  <ul>
                    <li
                      v-for="subItem in navItem.children"
                      :key="subItem.name"
                      class="px-4 py-2 submenu-item hover:text-white"
                    >
                      <g-link
                        class="block"
                        :to="subItem.link"
                        :title="subItem.name"
                        v-if="subItem.external != true"
                        >{{ subItem.name }}</g-link
                      >
                      <a
                        class="block"
                        :href="subItem.link"
                        target="_blank"
                        :title="subItem.name"
                        v-if="subItem.external == true"
                        >{{ subItem.name }}</a
                      >
                    </li>
                  </ul>
                </template>
              </v-popover>
            </ClientOnly>
          </li>
          <li class="px-4 py-1">
            <a
              role="button"
              @click.prevent="toggleSubNavigation()"
              class="block px-4 py-1"
              aria-label="Open Subnavigation"
              title="Open Subnavigation"
              v-bind:class="{
                'text-blue-600': showSubNavigation,
                '': !showSubNavigation
              }"
            >
              <font-awesome
                :icon="['fas', 'ellipsis-h']"
                size="lg"
              ></font-awesome>
            </a>

            <div
              v-click-outside="onClickOutside"
              class="py-4 mega-menu mb-16 border-t border-gray-200 shadow-xl bg-white dark:bg-black dark:border-gray-900"
              v-bind:class="{
                '': showSubNavigation,
                hidden: !showSubNavigation
              }"
            >
              <div>
                <subnavigation />
              </div>
            </div>
          </li>
        </ul>
      </div>

      <div class="inline-block">
        <ul class="list-none flex justify-center md:justify-end">
          <li class="mr-6">
            <search-button v-on="$listeners"></search-button>
          </li>
          <li>
            <theme-switcher v-on="$listeners" :theme="theme" />
          </li>
        </ul>
      </div>
    </div>
  </nav>
</template>

<script>
import ThemeSwitcher from "~/components/Navbar/ThemeSwitcher.vue";
import SearchButton from "~/components/Navbar/SearchButton.vue";
import Subnavigation from "~/components/Navbar/NavbarSubNavigation.vue";

export default {
  data: function() {
    return {
      showSubNavigation: false,
      vcoConfig: {
        events: ["dblclick", "click"],
        isActive: true
      }
    };
  },
  components: {
    ThemeSwitcher,
    SearchButton,
    Subnavigation
  },
  props: {
    theme: {
      type: String
    },
    hideSubnav: {
      type: Boolean
    }
  },
  methods: {
    toggleSubNavigation() {
      this.showSubNavigation = !this.showSubNavigation;
    },
    onClickOutside(event) {
      if (!event.defaultPrevented && this.showSubNavigation == true) {
        this.toggleSubNavigation();
      }
    },
    hideSubNavigation() {
      this.showSubNavigation = false;
    }
  },
  watch: {
    hideSubnav() {
      if (this.hideSubnav) {
        this.hideSubNavigation();
      }
    },
    $route(to, from) {
      this.hideSubNavigation();
    }
  }
};
</script>

<static-query>
query {
  metadata {
    siteName
    headerNavigation {
      name
      link
      external
      children {
        name
        link
        external
      }
    }
  }
}
</static-query>
EOF
	# }}}
	# NavbarMobile.vue {{{
	file=src/components/Navbar/NavbarMobile.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <nav
    class="block md:hidden flex items-center justify-between flex-wrap container mx-auto py-4 dark:text-gray-400"
  >
    <div class="block flex-grow flex items-center w-auto mx-4">
      <div class="flex items-center flex-shrink-0 mr-6">
        <a
          role="button"
          @click.prevent="openNavbarModal()"
          aria-label="Open Navigation"
          title="Open Navigation"
        >
          <font-awesome :icon="['fas', 'bars']"></font-awesome>
        </a>
      </div>
      <div class="flex-grow text-center font-bold text-lg">
        <span class="font-semibold text-xl tracking-tight">{{
          $static.metadata.siteName
        }}</span>
      </div>

      <div class="inline-block">
        <ul class="list-none flex justify-center md:justify-end">
          <li class="mr-6">
            <search-button v-on="$listeners"></search-button>
          </li>
          <li>
            <theme-switcher v-on="$listeners" :theme="theme" />
          </li>
        </ul>
      </div>
    </div>
  </nav>
</template>

<script>
import ThemeSwitcher from "~/components/Navbar/ThemeSwitcher.vue";
import SearchButton from "~/components/Navbar/SearchButton.vue";

export default {
  props: {
    theme: {
      type: String
    },
    showNavigation: {
      type: Boolean
    }
  },
  components: {
    ThemeSwitcher,
    SearchButton
  },
  methods: {
    openNavbarModal() {
      this.$emit("openNavbarModal");
    }
  }
};
</script>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>
EOF
	# }}}
	# NavbarSubNavigation.vue {{{
	file=src/components/Navbar/NavbarSubNavigation.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="container mx-auto">
    <div class="flex flex-wrap md:my-4 md:mx-4">
      <div class="w-full mb-2">
        <h2 class="text-xl mt-0 mb-2">Recent articles</h2>

        <div class="w-full">
          <VueSlickCarousel
            :arrows="true"
            :dots="false"
            class="-mx-4"
            v-bind="sliderSettings"
          >
            <div
              v-for="edge in $static.recent.edges"
              :key="edge.node.id"
              class="px-4"
            >
              <g-link :to="edge.node.path">
                <div :id="edge.node.id" class>
                  <g-image
                    :src="edge.node.image"
                    :alt="edge.node.title"
                    class="rounded-lg h-32 object-cover w-full"
                  ></g-image>

                  <div class="post-card-content">
                    <h3
                      class="tracking-wider mt-3 mb-3 text-lg font-light max-w-xl"
                    >
                      {{ edge.node.title }}
                    </h3>
                  </div>

                  <div class="post-card-footer">
                    <p class="text-xs">
                      <time :datetime="edge.node.datetime">{{
                        edge.node.humanTime
                      }}</time>
                      &nbsp;&bull;&nbsp;
                      {{ edge.node.timeToRead }} min read
                    </p>
                  </div>
                </div>
              </g-link>
            </div>
          </VueSlickCarousel>
        </div>
      </div>
      <div class="w-full mb-8">
        <h2 class="text-xl mt-2 mb-2">Tags</h2>

        <ul class="flex flex-wrap">
          <li
            class="text-xs bg-transparent hover:text-blue-700 py-2 px-4 mr-4 mb-4 border hover:border-blue-500 border-gray-600 text-gray-700 dark:text-gray-400 rounded-full"
            v-for="tag in $static.tags.edges"
            :key="tag.node.id"
          >
            <g-link :to="tag.node.path">{{ tag.node.title }}</g-link>
          </li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
import VueSlickCarousel from "vue-slick-carousel";

export default {
  components: {
    VueSlickCarousel
  },
  data() {
    return {
      sliderSettings: {
        dots: false,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              arrows: false,
              slidesToShow: 3,
              slidesToScroll: 1,
              dots: false,
              infinite: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              arrows: false,
              slidesToShow: 2,
              slidesToScroll: 1,
              initialSlide: 2,
              dots: false,
              infinite: true
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
              infinite: true
            }
          }
        ]
      }
    };
  }
};
</script>

<style></style>

<static-query>

query {
  tags: allTag {
    edges {
      node {
        title
        path
      }
    }
  },
  recent : allBlog(limit: 4, sort: { by: "created", order: DESC }) {
    edges {
      node {
        id
        title
        path
        image(width:230, height:130)
        humanTime: created(format: "DD MMM YYYY")
        datetime: created
        timeToRead
      }
    }
  }
}


</static-query>
EOF
	# }}}
	# SearchButton.vue {{{
	file=src/components/Navbar/SearchButton.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <a
    role="button"
    @click.prevent="openSearchModal()"
    aria-label="Open Search"
    title="Open Search"
  >
    <font-awesome :icon="['fas', 'search']"></font-awesome>
  </a>
</template>

<script>
export default {
  methods: {
    openSearchModal() {
      this.$emit("openSearchModal");
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# ThemeSwitcher {{{
	file=src/components/Navbar/ThemeSwitcher.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <a
    role="button"
    @click.prevent="toggleTheme()"
    :aria-label="'Toggle ' + nextTheme"
    :title="'Toggle ' + nextTheme"
    class="toggle-theme"
  >
    <font-awesome :icon="['fas', 'sun']" v-if="theme === 'dark'"></font-awesome>
    <font-awesome
      :icon="['fas', 'moon']"
      v-if="theme === 'light'"
    ></font-awesome>
  </a>
</template>

<script>
let themes = ["light", "dark"];

export default {
  props: {
    theme: {
      type: String
    }
  },

  computed: {
    nextTheme() {
      const currentIndex = themes.indexOf(this.theme);
      const nextIndex = (currentIndex + 1) % themes.length;
      return themes[nextIndex];
    }
  },
  methods: {
    toggleTheme() {
      const currentIndex = themes.indexOf(this.theme);
      const nextIndex = (currentIndex + 1) % themes.length;
      window.__setPreferredTheme(themes[nextIndex]);

      this.$emit("setTheme", themes[nextIndex]);
    }
  },
  async mounted() {
    // set default
    if (typeof window.__theme !== "undefined")
      this.$emit("setTheme", window.__theme);
  }
};
</script>
EOF
	# }}}
	# components/Partials:
	# ContentHeader.vue {{{
	file=src/components/Partials/ContentHeader.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div
      class="z-100 text-center bg-gray-200 dark:bg-gray-900 py-10 md:py-20"
      v-if="!hasImage"
    >
      <h2 v-if="title != null" class="h1 font-extrabold dark:text-gray-400">
        {{ title }}
      </h2>
      <p v-if="sub != null" class="text-gray-600 text-light font-sans">
        {{ sub }}
      </p>
    </div>

    <div v-if="hasImage" class="z-100 relative mt-0 h-auto">
      <g-image
        v-if="hasImage && staticImage"
        :src="require(`!!assets-loader!@pageImage/${image}`)"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <g-image
        v-if="hasImage && !staticImage"
        :src="image"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <slot>
        <div
          class="text-center text-white bg-gray-800 lg:py-48 md:py-32 py-24"
          :class="`bg-opacity-${opacity}`"
        >
          <h2 v-if="title != null" class="h1 font-extrabold">{{ title }}</h2>
          <p v-if="sub != null" class="h5 font-sans">{{ sub }}</p>
        </div>
      </slot>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    title: {
      type: String,
      default: null
    },
    sub: {
      type: String,
      default: null
    },
    image: {
      type: String | Object,
      default: null
    },
    staticImage: {
      type: Boolean,
      default: true
    },
    opacity: {
      type: Number,
      default: 50
    }
  },
  computed: {
    hasImage() {
      return this.image ? true : false;
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# Footer.vue {{{
	file=src/components/Partials/Footer.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div
    class="bg-blue-900 dark:bg-gray-900 text-white dark:text-gray-400 text-center text-sm"
  >
    <div class="container mx-auto py-16">
      <div class="mb-4">
        <p>
          Copyright {{ new Date().getFullYear() }} by
          {{ $static.metadata.siteName }} &middot; Powered by
          <a href="https://www.gridsome.org" target="_blank">Gridsome</a>
        </p>
      </div>
      <div class="mb-4">
        <ul class="list-reset flex justify-center">
          <li
            v-for="socialItem in $static.metadata.social"
            :key="socialItem.name"
            class="px-4"
          >
            <a :href="socialItem.link" target="_blank" :title="socialItem.name">
              <font-awesome
                :icon="icon(socialItem.icon)"
                size="lg"
              ></font-awesome>
            </a>
          </li>
        </ul>
      </div>
      <div class="mb-4">
        <ul class="list-reset flex justify-center">
          <li
            v-for="navItem in $static.metadata.footerNavigation"
            :key="navItem.name"
            class="px-4"
          >
            <g-link
              :to="navItem.link"
              :title="navItem.name"
              v-if="navItem.external != true"
              >{{ navItem.name }}</g-link
            >
            <a
              :href="navItem.link"
              target="_blank"
              :title="navItem.name"
              v-if="navItem.external == true"
              >{{ navItem.name }}</a
            >
          </li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  methods: {
    icon(icon) {
      return ["fab", icon];
    }
  }
};
</script>

<static-query>
query {
  metadata {
    siteName

    social {
      name
      icon
      link
    }
  
    footerNavigation {
        name
        link
        external
    }
  }
}
</static-query>
EOF
	# }}}
} #}}}
# main {{{
[ -z $1 ] && opt="null" || opt=$1
[ -z $2 ] && dir="test" || dir=$2
arr=(
	gen
	all
)
[[ " ${arr[@]} " =~ " ${opt} " ]] || cd $dir

case $opt in
"gen") gen ;;
"deps") deps ;;
"fill") fill ;;
"file") file ;;
"all") gen && deps && fill && file && gridsome develop ;;
*) echo "Options are: gen, fill, dev" ;;
esac
