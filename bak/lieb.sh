#!/bin/bash
gen() { #{{{
	[ -d $dir ] && echo "Removing $dir" && rm -rf $dir
	gridsome create $dir
	cd $dir
	ncu -u
	yarn
}        #}}}
deps() { #{{{
	dev=(
		@gridsome/plugin-sitemap
		gridsome-plugin-rss
		#gridsome-plugin-flexsearch
		#gridsome-plugin-remark-shiki
		#@gridsome/remark-prismjs
		gridsome-plugin-remark-prismjs-all
		@gridsome/source-filesystem
		@gridsome/transformer-remark
		node-sass
		sass-loader
		tailwindcss
		tailwindcss-dark-mode
		gridsome-plugin-tailwindcss
		#vue-feather-icons
		@fortawesome/fontawesome-svg-core
		@fortawesome/free-brands-svg-icons
		@fortawesome/free-solid-svg-icons
		@fortawesome/vue-fontawesome
		rfs
	)
	yarn add -D ${dev[@]}
}        #}}}
fill() { #{{{
	# main.js {{{
	file=src/main.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
import DefaultLayout from "~/layouts/Default.vue";
import 'prismjs/themes/prism.css'
import '~/assets/scss/main.scss'

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { config, library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import "@fortawesome/fontawesome-svg-core/styles.css";

config.autoAddCss = false;
library.add(fas);
library.add(fab);

export default function(Vue, { router, head, isClient }) {
  Vue.component("Layout", DefaultLayout);
  Vue.component("font-awesome", FontAwesomeIcon);
}
EOF
	# }}}
	# main.scss {{{
	file=src/assets/scss/main.scss
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
@import "~rfs/scss";

@tailwind base;

blockquote {
  @apply border-l;
  @apply border-l-4;
  @apply border-l-blue-500;
  @apply pl-4;
  @apply italic;
  @apply my-8;

  p {
    padding: 0 !important;
  }
}

h1,
.h1 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(3rem);
}

h2,
.h2 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(2.25rem);
}

h3,
.h3 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.875rem);
}

h4,
.h4 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.5rem);
}

h5,
.h5 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.25rem);
}

h6,
.h6 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.125rem);
}

@tailwind components;
@tailwind utilities;

.container {
  @apply max-w-screen-xl;
  @apply px-0;
}

.fade-enter-active,
.fade-leave-active {
  @apply transition-all;
  @apply duration-200;
}

.fade-enter,
.fade-leave-to {
  opacity: 0;
}

.featured-post-card {
  .slick-list {
    @apply h-full;
    @apply rounded-lg;

    div:not(.post-card-author):not(.featured-label):not(.post-card-content):not(.post-card-footer) {
      @apply h-full;
    }
  }

  .slick-arrow {
    @apply absolute;
    @apply bottom-0;
    @apply right-0;
    @apply text-white;
  }

  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  &:hover {
    transform: translateY(-5px);
  }

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-content {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
    @apply mt-20;
    @apply ml-10;
    @apply text-white;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
    @apply left-0;
    @apply text-white;
    @apply ml-10;
    @apply mb-10;
    @apply text-sm;
    @apply font-semibold;
  }
}

.post-card {
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  &:hover {
    transform: translateY(-5px);
  }

  @apply relative;
  @apply border;

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-title {
    @apply leading-8;
    @apply text-2xl;
  }

  .post-card-excerpt {
    @apply font-serif;
  }

  .post-card-content {
    @apply relative;
    @apply flex-1;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
  }
}

.header {
  min-height: 500px;
}

.mobileSubnav {
  @apply absolute;
  @apply w-full;
  @apply -mx-2;
}

@media (max-width: 767px) {
  .header {
    min-height: 360px;
  }
}

.mega-menu {
  left: 0;
  top: 0;
  @apply mt-16;
  @apply w-full;
  @apply absolute;
  @apply text-left;
}

/* ––––––––––––––––––––––––––––––––––––––––––––––––––
    Content
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

.mode-dark {
  .post-content {
    p,
    span:not(.token),
    li {
      @apply tracking-wider;
      @apply leading-relaxed;
      @apply font-normal;
      @apply text-gray-500;
      @include font-size(1.1rem);
    }
  }
}

.post-content {
  p,
  span:not(.token),
  li {
    @apply tracking-wider;
    @apply leading-relaxed;
    @apply font-normal;
    @apply text-gray-800;
    @include font-size(1.1rem);
  }

  ol {
    @apply list-decimal;
    @apply ml-5;
    @apply mt-5;
  }

  ul {
    @apply list-disc;
    @apply ml-5;
    @apply mt-5;
  }

  li > ul {
    @apply mt-0;
  }
}

.post-authors a::after {
  content: ", ";
}

.post-authors a:last-child::after {
  content: "";
}

pre[class*="language-"],
code[class*="language-"] {
  color: #5c6e74;

  text-shadow: none;
  font-family: Consolas, Monaco, "Andale Mono", "Ubuntu Mono", monospace;
  @include font-size(0.9rem);
  direction: ltr;
  text-align: left;
  white-space: pre;
  word-spacing: normal;
  word-break: normal;
  line-height: 1.5;
  -moz-tab-size: 4;
  -o-tab-size: 4;
  tab-size: 4;
  -webkit-hyphens: none;
  -moz-hyphens: none;
  -ms-hyphens: none;
  hyphens: none;
}

pre[class*="language-"]::selection,
code[class*="language-"]::selection,
pre[class*="language-"]::mozselection,
code[class*="language-"]::mozselection {
  text-shadow: none;
  @apply bg-gray-200;
}

pre[class*="language-"] {
  padding: 1em;
  margin: 0.5em 0;
  overflow: auto;
  @apply bg-gray-200;
}

:not(pre) > code[class*="language-"] {
  padding: 0.3em 0.3em;
  border-radius: 0.3em;
  color: #db4c69;
  @apply bg-gray-200;
}

.mode-dark {
  pre[class*="language-"]::selection,
  code[class*="language-"]::selection,
  pre[class*="language-"]::mozselection,
  code[class*="language-"]::mozselection {
    text-shadow: none;
    @apply bg-gray-800;
  }

  pre[class*="language-"],
  code[class*="language-"] {
    @apply text-gray-400;
  }

  pre[class*="language-"] {
    padding: 1em;
    margin: 0.5em 0;
    overflow: auto;
    @apply bg-gray-800;
  }

  :not(pre) > code[class*="language-"] {
    padding: 0.3em 0.3em;
    border-radius: 0.3em;
    color: #db4c69;
    @apply bg-gray-800;
  }
}

@media print {
  pre[class*="language-"],
  code[class*="language-"] {
    text-shadow: none;
  }
}

/*********************************************************
* Tokens
*/
.namespace {
  opacity: 0.7;
}

.token.comment,
.token.prolog,
.token.doctype,
.token.cdata {
  color: #93a1a1;
}

.token.punctuation {
  color: #999999;
}

.token.property,
.token.tag,
.token.boolean,
.token.number,
.token.constant,
.token.symbol,
.token.deleted {
  color: #990055;
}

.token.selector,
.token.attr-name,
.token.string,
.token.char,
.token.builtin,
.token.inserted {
  color: #669900;
}

.token.operator,
.token.entity,
.token.url,
.language-css .token.string,
.style .token.string {
  color: #a67f59;
  background: transparent;
}

.token.atrule,
.token.attr-value,
.token.keyword {
  color: #0077aa;
}

.token.function {
  color: #dd4a68;
}

.token.regex,
.token.important,
.token.variable {
  color: #ee9900;
}

.token.important,
.token.bold {
  font-weight: bold;
}

.token.italic {
  font-style: italic;
}

.token.entity {
  cursor: help;
}

/*********************************************************
* Line highlighting
*/
pre[data-line] {
  position: relative;
}

pre[class*="language-"] > code[class*="language-"] {
  position: relative;
  z-index: 1;
}

.line-highlight {
  position: absolute;
  left: 0;
  right: 0;
  padding: inherit 0;
  margin-top: 1em;
  background: #000;
  box-shadow: inset 5px 0 0 #f7d87c;
  z-index: 0;
  pointer-events: none;
  line-height: inherit;
  white-space: pre;
}

.mode-dark {
  .medium-zoom-overlay {
    background-color: #000 !important;
  }
}

.medium-zoom-overlay {
  @apply z-1000;
}

.medium-zoom-image {
  @apply z-1000;
}

figure {
  @apply my-8;
  @apply -mx-16;
  width: calc(100% + 8rem);

  figcaption {
    @apply text-center;
  }
}
EOF
	# }}}
	# image {{{
	# blog
	file=content/blog/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author
	file=content/author/images/author.jpg
	mkdir -p "$(dirname $file)"
	dl='https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimg4.wikia.nocookie.net%2F__cb20100607195345%2Fdeusex%2Fen%2Fimages%2F3%2F32%2FJccover.jpg&f=1&nofb=1'
	wget -c $dl -O $file

	# author cover
	file=content/author/cover/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file
	# }}}
	# blog {{{
	for num in {1..12}; do
		file=content/blog/entry$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Styles$num
EOF
		cat <<'EOF' >>$file
tags: tag1, tag2
category: Digital
excerpt: Lorem markdownum aptos pes, Inachidos caput corrumpere! Vincere ferocia arva.
created: 2019-01-10
image: ./images/alexandr-podvalny-220262-unsplash.jpg
image_caption: Photo by Josh Spires on Unsplash
author: author1, author2, author3
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# author {{{
	for num in {1..3}; do
		file=content/author/author$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
id: author$num
name: author$num
EOF
		cat <<'EOF' >>$file
bio: Primis vitae mauris turpis ornare libero odio torquent vehicula proin consequat curabitur mattis
facebook: https://www.facebook.com
twitter: https://www.twitter.com
linkedin: https://www.linkedin.com
image: ./images/author.png
cover: ./cover/benjamin-voros-Lxq_TyMMHtQ-unsplash.jpg
---
EOF
	done
	# }}}
	# about.md {{{
	file=content/pages/about.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: About us
---

## Ossa narrat sortita fecerat sit conataque

Lorem markdownum aptos pes, Inachidos caput corrumpere! Hanc haud quam [est
candore](http://quisquis-in.io/ramossuperum) conpulit meriti. Vincere ferocia
arva.

## Eleis celeberrimus loci ait falsa infelix tuoque

Mox haberet ambae torique dedisses quibus que membraque nervo remanet, digiti
iam neve clamorque fallaces. Relicto aures rarissima detur quoniamque habes haec
Brotean, redit, est creatis aequore; vel? Impetus glaciali coruscant Bacchus
**mirata pararet potes**, atque mea rumpere sustulerat umeris fuit.

## Facundis quid

Venerit conveniunt per memori sed laniarat Dromas, solum tum. Undis lacteus
infitiatur adest [acies certius](http://www.tollit-clamavit.io/) inscius, cum ad
emittunt dextra.

Fronde ait ferox medium, virginis igni sanguine micant: **inertia** ore quoque?
Iaculi quicquid **virescere misit stirpe** Theseus Venerem! Falce taceo oves,
idem fugit, non abiit palam quantum, fontes vinci et abiit. Deiectoque exstabant
**Phrygiae** cepit munus tanto.

## Et capienda Peneia

*Haec moenia pater* signataque urget, ait quies laqueo sumitque. Misit sit
moribunda terrae sequar longis hoc, cingebant copia cultros! Alis templi taeda
solet suum mihi penates quae. Cecidere *deo agger infantem* indetonsusque ipsum;
ova formasque cornu et pectora [voce oculos](http://www.tibibene.io/iter.html),
prodis pariterque sacra finibus, Sabinae. Fugarant fuerat, famam ait toto imas
sorte pectora, est et, procubuit sua Appenninigenae habes postquam.

## Quoque aut gurgite aliquis igneus

Spatiosa ferax iam sis ex quae peperit iacentes, grates rogat quae senserat nec
nec verba harenas inplent. Per dum necis in in versus quin loquendi latens;
inde. **Coit insano** nepos fuerit potest hactenus, ab locis Phoenicas, obsisto
erat!

> Nec uterum Aurorae petentes abstulit. Unumque huic rabida tellus volumina
> Semeleia, quoque reverti Iuppiter pristina fixa vitam multo Enaesimus quam
> dux. Sua **damus** decipere, ut **obortas** nomen sine vestrae vita.

Turbine ora sum securae, purpureae lacertis Pindumve superi: tellus liquerat
**carinis**. Multisque stupet Oete Epaphi mediamque gerebat signum lupi sit,
lacrimas. Tumidi fassusque hosti, deus [vixque desint
dedit](http://hisnurus.com/putares-pars) dum et, quo non, dea [suras
tantum](http://mactata.org/inducere.php). Unus acta capulo. In Dryope sic
vestigia est neu ignis in **illa mirantur agilis** densior.
EOF
	# }}}
	# gridsome.config.js {{{
	file=gridsome.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
module.exports = {
  siteName: "Gridsome",
  siteDescription: "An open-source framework to generate awesome pages",
  plugins: [
    {
      use: "gridsome-plugin-tailwindcss",
      options: {
        tailwindConfig: "./tailwind.config.js",
        purgeConfig: {
          whitelist: [
            "svg-inline--fa",
            "table",
            "table-striped",
            "table-bordered",
            "table-hover",
            "table-sm"
          ],
          whitelistPatterns: [
            /fa-$/,
            /blockquote$/,
            /code$/,
            /pre$/,
            /table$/,
            /table-$/,
            /vueperslide$/,
            /vueperslide-$/
          ]
        },
        presetEnvConfig: {},
        shouldPurge: false,
        shouldImport: true,
        shouldTimeTravel: true,
        shouldPurgeUnusedKeyframes: true
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Author",
        path: "./content/author/*.md"
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Blog",
        path: "./content/blog/**/*.md",
        refs: {
          author: "Author",
          tags: {
            typeName: "Tag",
            create: true
          },
          category: {
            typeName: "Category",
            create: true
          }
        }
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "CustomPage",
        path: "./content/pages/*.md"
      }
    }
  ],
  transformers: {
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        [
          "gridsome-plugin-remark-prismjs-all",
          {
            noInlineHighlight: false,
            showLineNumbers: false
          }
        ]
      ]
    }
  },
  templates: {
    Blog: [
      {
        path: "/:title"
      }
    ],
    CustomPage: [
      {
        path: "/:title",
        component: "~/templates/CustomPage.vue"
      }
    ],
    Category: [
      {
        path: "/category/:title",
        component: "~/templates/Category.vue"
      }
    ],
    Author: [
      {
        path: "/author/:name",
        component: "~/templates/Author.vue"
      }
    ],
    Tag: [
      {
        path: "/tags/:title",
        component: "~/templates/Tag.vue"
      }
    ]
  },
  chainWebpack: config => {
    config.resolve.alias.set("@pageImage", "@/assets/images");
  }
};
EOF
	# }}}
	# tailwind.config.js {{{
	file=tailwind.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
//tailwind border color plugin powered by
//https://github.com/tailwindcss/tailwindcss/pull/560#issuecomment-503222143
var _ = require("lodash");
var flattenColorPalette = require("tailwindcss/lib/util/flattenColorPalette")
  .default;

module.exports = {
  purge: {
    content: ["./src/**/*.html", "./src/**/*.vue", "./src/**/*.jsx"],
    options: {
      whitelist: [
        "bg-opacity-0",
        "bg-opacity-25",
        "bg-opacity-50",
        "bg-opacity-75",
        "bg-opacity-100",
        "mode-dark"
      ]
    }
  },
  theme: {
    extend: {
      height: {
        "128": "32rem",
        "half-screen": "50vh"
      },
      backgroundOpacity: {
        "0": "0",
        "10": "0.1",
        "20": "0.2",
        "30": "0.3",
        "40": "0.4",
        "50": "0.5",
        "60": "0.6",
        "70": "0.7",
        "80": "0.8",
        "90": "0.9",
        "100": "1"
      }
    },
    fontFamily: {
      sans: [
        '"Source Sans Pro"',
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: ["Georgia", "Cambria", '"Times New Roman"', "Times", "serif"],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    },
    zIndex: {
      "-10": "-10",
      "0": 0,
      "10": 10,
      "20": 20,
      "30": 30,
      "40": 40,
      "50": 50,
      "25": 25,
      "50": 50,
      "75": 75,
      "100": 100,
      "1000": 1000,
      auto: "auto"
    },
    boxShadow: {
      default: "0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)",
      md: "0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)",
      lg:
        "0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)",
      xl:
        "0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)",
      "2xl": "0 25px 50px -12px rgba(0, 0, 0, .25)",
      "2xl-strong": "0 25px 50px -12px rgba(0, 0, 0, .5)",
      "3xl": "0 35px 60px -15px rgba(0, 0, 0, .3)",
      inner: "inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)",
      outline: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      focus: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      none: "none"
    }
  },
  variants: {
    backgroundColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    textColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    borderColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ]
  },
  plugins: [
    require("tailwindcss-dark-mode")(),
    function({ addUtilities, e, theme, variants }) {
      const colors = flattenColorPalette(theme("borderColor"));

      const utilities = _.flatMap(
        _.omit(colors, "default"),
        (value, modifier) => ({
          [`.${e(`border-t-${modifier}`)}`]: {
            borderTopColor: `${value}`
          },
          [`.${e(`border-r-${modifier}`)}`]: {
            borderRightColor: `${value}`
          },
          [`.${e(`border-b-${modifier}`)}`]: {
            borderBottomColor: `${value}`
          },
          [`.${e(`border-l-${modifier}`)}`]: {
            borderLeftColor: `${value}`
          }
        })
      );

      addUtilities(utilities, variants("borderColor"));
    }
  ],
  corePlugins: {
    container: false
  }
};
EOF
	# }}}
}        #}}}
file() { #{{{
	# Index.html {{{
	file=src/index.html
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<!DOCTYPE html>
<html ${htmlAttrs}>
  <head>
    ${head}
  </head>
  <body ${bodyAttrs}>
    <script>
    </script>

    ${app} ${scripts}
  </body>
</html>
EOF
	# }}}
	# Layouts
	# Default.vue {{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="font-sans antialiased text-ui-typo bg-ui-background">
    <header class="font-sans antialiased text-ui-typo bg-ui-background">
      <div class="">
          <ToggleDarkMode class="ml-2 sm:ml-8">
            <template slot="default" slot-scope="{ dark }">
              <MoonIcon v-if="dark" size="1.5x" />
              <SunIcon v-else size="1.5x" />
            </template>
          </ToggleDarkMode>
      </div>
    </header>

    <main class="">
      <slot />
    </main>

    <footer class="">
      <span class="footer__copyright"
        >Copyright © {{ new Date().getFullYear() }}.
      </span>
      <span class=""
        >Powered by <a href="//gridsome.org"> Gridsome </a></span
      >
    </footer>
  </div>
</template>

<script>
import Logo from "~/components/Logo.vue";
import ToggleDarkMode from "@/components/ToggleDarkMode";
import {
  SunIcon,
  MoonIcon
} from "vue-feather-icons";

export default {
  props: {
    showLogo: { default: true }
  },
  components: {
    Logo,
    ToggleDarkMode,
    SunIcon,
    MoonIcon
  }
};
</script>

<style lang="scss">
:root {
  --color-ui-background: theme('colors.white');
  --color-ui-typo: theme('colors.gray.700');
  --color-ui-sidebar: theme('colors.gray.200');
  --color-ui-border: theme('colors.gray.300');
  --color-ui-primary: theme('colors.indigo.600');
}

html[lights-out] {
  --color-ui-background: theme('colors.gray.900');
  --color-ui-typo: theme('colors.gray.100');
  --color-ui-sidebar: theme('colors.gray.800');
  --color-ui-border: theme('colors.gray.800');
  --color-ui-primary: theme('colors.indigo.500');

  pre[class*="language-"],
  code[class*="language-"] {
    @apply bg-ui-border;
  }
}

* {
  transition-property: color, background-color, border-color;
  transition-duration: 90ms;
  transition-timing-function: ease-in-out;
}

h1 {
  @apply text-4xl;
}

h2 {
  @apply text-2xl;
}

h3 {
  @apply text-xl;
}

h4 {
  @apply text-lg;
}

a:not(.active):not(.text-ui-primary):not(.text-white) { @apply text-ui-typo }

p,
ol,
ul,
pre,
strong,
blockquote {
  @apply mb-4 text-base text-ui-typo;
}

.content {
  a {
    @apply text-ui-primary underline;
  }

  h1, h2, h3, h4, h5, h6 {
    @apply -mt-12 pt-20;
  }
    
  h2 + h3,
  h2 + h2,
  h3 + h3 {
    @apply border-none -mt-20;
  }

  h2,
  h3 {
    @apply border-b border-ui-border pb-1 mb-3;
  }

  ul {
    @apply list-disc;

    ul {
      list-style: circle;
    }
  }

  ol {
    @apply list-decimal;
  }

  ol,
  ul {
    @apply pl-5 py-1;

    li {
      @apply mb-2;

      p {
        @apply mb-0;
      }

      &:last-child {
        @apply mb-0;
      }
    }
  }
}

blockquote {
  @apply border-l-4 border-ui-border py-2 pl-4;

  p:last-child {
    @apply mb-0;
  }
}

code {
  @apply px-1 py-1 text-ui-typo bg-ui-sidebar font-mono border-b border-r border-ui-border text-sm rounded;
}

pre[class*="language-"] {
  @apply max-w-full overflow-x-auto rounded;

  & + p {
    @apply mt-4;
  }

  & > code[class*="language-"] {
    @apply border-none leading-relaxed;
  }
}

header {
  background-color: rgba(255, 255, 255, 0.9);
  backdrop-filter: blur(4px);
}

table {
  @apply text-left mb-6;

  td, th {
    @apply py-3 px-4;
    &:first-child {
      @apply pl-0;
    }
    &:last-child {
      @apply pr-0;
    }
  }

  tr {
    @apply border-b border-ui-border;
    &:last-child {
      @apply border-b-0;
    }
  }
}

.sidebar {
  @apply fixed bg-ui-background px-4 inset-x-0 bottom-0 w-full border-r border-ui-border overflow-y-auto transition-all z-40;
  transform: translateX(-100%);

  &.open {
    transform: translateX(0);
  }

  @screen lg {
    @apply w-1/4 px-0 bg-transparent top-0 bottom-auto inset-x-auto sticky z-0;
    transform: translateX(0);
  }
}
</style>
EOF
	# }}}
	# Templates:
	# Post.vue {{{
	file=src/templates/Post.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <div class="post-title">
      <h1 class="post-title__text">
        {{ $page.post.title }}
      </h1>

      <PostMeta :post="$page.post" />
    </div>

    <div class="post content-box">
      <div class="post__header">
        <g-image
          alt="Cover image"
          v-if="$page.post.cover_image"
          :src="$page.post.cover_image"
        />
      </div>

      <div class="post__content" v-html="$page.post.content" />

      <div class="post__footer">
        <PostTags :post="$page.post" />
      </div>
    </div>

    <div class="post-comments">
      <!-- Add comment widgets here -->
    </div>

    <Author class="post-author" />
  </Layout>
</template>

<script>
import PostMeta from "~/components/PostMeta";
import PostTags from "~/components/PostTags";
import Author from "~/components/Author.vue";

export default {
  components: {
    Author,
    PostMeta,
    PostTags
  },
  metaInfo() {
    return {
      title: this.$page.post.title,
      meta: [
        {
          name: "description",
          content: this.$page.post.description
        }
      ]
    };
  }
};
</script>

<page-query>
query Post ($id: ID!) {
  post: post (id: $id) {
    title
    path
    date (format: "D. MMMM YYYY")
    timeToRead
    tags {
      id
      title
      path
    }
    description
    content
    cover_image (width: 860, blur: 10)
  }
}
</page-query>

<style lang="scss">
</style>
EOF
	# }}}
	# Tag.vue {{{
	file=src/templates/Tag.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <h1 class="tag-title text-center space-bottom"># {{ $page.tag.title }}</h1>

    <Pagination
      :baseUrl="$page.tag.path"
      :currentPage="$page.tag.belongsTo.pageInfo.currentPage"
      :totalPages="$page.tag.belongsTo.pageInfo.totalPages"
      :maxVisibleButtons="5"
      v-if="$page.tag.belongsTo.pageInfo.totalPages > 1"
    />

    <div class="posts">
      <PostCard
        v-for="edge in $page.tag.belongsTo.edges"
        :key="edge.node.id"
        :post="edge.node"
      />
    </div>
  </Layout>
</template>

<page-query>
query($id: ID!, $page:Int) {
  tag (id: $id) {
    title
    path
    belongsTo(perPage: 5, page: $page) @paginate {
      totalCount
      pageInfo {
        totalPages
        currentPage
      }
      edges {
        node {
          ...on Post {
            title
            path
            date (format: "D. MMMM YYYY")
            timeToRead
            description
            content
          }
        }
      }
    }
  }
}
</page-query>

<script>
import Author from "~/components/Author.vue";
import PostCard from "~/components/PostCard.vue";
import Pagination from "~/components/Pagination.vue";

export default {
  components: {
    Pagination,
    Author,
    PostCard
  },
  metaInfo: {
    title: "Hello, world!"
  }
};
</script>

<style lang="scss"></style>
EOF
	# }}}
	# Components:
	# ToggleDarkMode.vue {{{
	file=src/components/ToggleDarkMode.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <button
    @click="handleClick"
    aria-label="Toggle Darkmode"
    title="Toggle Darkmode"
  >
    <slot :dark="isDarkMode" />
  </button>
</template>

<script>
export const LIGHTS_OUT = "lights-out";

export default {
  data() {
    return {
      isDarkMode: false
    };
  },

  methods: {
    handleClick() {
      const hasDarkMode = document.documentElement.hasAttribute(LIGHTS_OUT);

      // Toggle dark mode on click.
      return this.toggleDarkMode(!hasDarkMode);
    },

    toggleDarkMode(shouldBeDark) {
      document.documentElement.toggleAttribute(LIGHTS_OUT, shouldBeDark);

      this.isDarkMode = shouldBeDark;

      this.writeToStorage(shouldBeDark);

      return shouldBeDark;
    },

    detectPrefered() {
      return window.matchMedia("(prefers-color-scheme: dark)").matches;
    },

    hasInStorage() {
      const check = localStorage.getItem(LIGHTS_OUT);

      return check !== null;
    },

    writeToStorage(prefersDark) {
      localStorage.setItem(LIGHTS_OUT, prefersDark ? "true" : "false");
    },

    getFromStorage() {
      return localStorage.getItem(LIGHTS_OUT) === "true" ? true : false;
    }
  },

  mounted() {
    if (this.hasInStorage()) {
      this.toggleDarkMode(this.getFromStorage());
    } else if (process.isClient && window.matchMedia) {
      this.toggleDarkMode(this.detectPrefered());
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# Pagination.vue {{{
	file=src/components/Pagination.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <ul class="flex pl-0 list-none rounded my-2">
    <li
      class="font-sans antialiased text-ui-typo bg-ui-background"
      v-if="!isFirstPage(currentPage, totalPages)"
    >
      <g-link
        :to="previousPage(currentPage, totalPages)"
        class="page-link"
        tabindex="-1"
        >&laquo;</g-link
      >
    </li>

    <li
      v-for="page in pages"
      :key="page.name"
      v-bind:class="[
        isCurrentPage(currentPage, page.name) ? 'border-l-2 border-l-black' : ''
      ]"
      class="w-10 relative block py-2 text-center leading-tight bg-white border border-gray-300 text-black rounded hover:bg-gray-300 ml-1 mr-1"
    >
      <g-link
        :to="page.link"
        class="page-link"
        :aria-label="page.name"
        :aria-current="page.name"
        >{{ page.name }}</g-link
      >
    </li>

    <li
      class="w-10 relative block py-2 text-center leading-tight bg-white border border-gray-300 text-black ml-1 rounded hover:bg-gray-300"
      v-if="!isLastPage(currentPage, totalPages)"
    >
      <g-link
        :to="nextPage(currentPage, totalPages)"
        class="page-link"
        tabindex="-1"
        >&raquo;</g-link
      >
    </li>
  </ul>
</template>

<script>
export default {
  props: {
    baseUrl: {
      type: String,
      default: ""
    },
    currentPage: Number,
    totalPages: Number,
    maxVisibleButtons: {
      type: Number,
      required: false,
      default: 3
    }
  },
  methods: {
    isFirstPage(currentPage, totalPages) {
      return currentPage == 1;
    },
    isLastPage(currentPage, totalPages) {
      return currentPage == totalPages;
    },
    isCurrentPage(currentPage, pageElement) {
      return currentPage == pageElement;
    },
    nextPage(currentPage, totalPages) {
      return `${this.baseUrl}/${currentPage + 1}`;
    },
    previousPage(currentPage, totalPages) {
      return currentPage === 2
        ? `${this.baseUrl}/`
        : `${this.baseUrl}/${currentPage - 1}`;
    }
  },
  computed: {
    startPage() {
      if (this.currentPage === 1) {
        return 1;
      }
      if (this.currentPage === this.totalPages) {
        return this.currentPage - 1;
      }
      return this.currentPage - 1;
    },
    pages() {
      const range = [];
      for (
        let i = this.startPage;
        i <=
        Math.min(this.startPage + this.maxVisibleButtons - 1, this.totalPages);
        i += 1
      ) {
        range.push({
          name: i,
          isDisabled: i === this.currentPage,
          link: i === 1 ? `${this.baseUrl}/` : `${this.baseUrl}/${i}`
        });
      }
      return range;
    }
  }
};
</script>
EOF
	# }}}
	# ToggleTheme.vue {{{
	file=src/components/ToggleTheme.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <button
    role="button"
    aria-label="Toggle dark/light"
    @click.prevent="toggleTheme"
    class="toggle-theme"
  >
    <svg
      v-if="darkTheme"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      stroke-width="2"
      stroke-linecap="round"
      stroke-linejoin="round"
      class="feather feather-sun"
    >
      <circle cx="12" cy="12" r="5"></circle>
      <line x1="12" y1="1" x2="12" y2="3"></line>
      <line x1="12" y1="21" x2="12" y2="23"></line>
      <line x1="4.22" y1="4.22" x2="5.64" y2="5.64"></line>
      <line x1="18.36" y1="18.36" x2="19.78" y2="19.78"></line>
      <line x1="1" y1="12" x2="3" y2="12"></line>
      <line x1="21" y1="12" x2="23" y2="12"></line>
      <line x1="4.22" y1="19.78" x2="5.64" y2="18.36"></line>
      <line x1="18.36" y1="5.64" x2="19.78" y2="4.22"></line>
    </svg>
    <svg
      v-else
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      stroke-width="2"
      stroke-linecap="round"
      stroke-linejoin="round"
      class="feather feather-moon"
    >
      <path d="M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z"></path>
    </svg>
  </button>
</template>

<script>
export default {
  data() {
    return {
      darkTheme: false
    };
  },
  methods: {
    toggleTheme() {
      this.darkTheme = !this.darkTheme;

      // This is using a script that is added in index.html
      window.__setPreferredTheme(this.darkTheme ? "dark" : "light");
    }
  },
  mounted() {
    if (window.__theme == "dark") this.darkTheme = true;
  }
};
</script>

<style lang="scss">
.toggle-theme {
  background-color: transparent;
  border: 0;
  color: var(--body-color);
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }
  &:focus {
    outline: none;
  }
}
</style>
EOF
	# }}}
	# PostTags.vue {{{
	file=src/components/PostTags.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="post-tags">
    <g-link
      class="post-tags__link"
      v-for="tag in post.tags"
      :key="tag.id"
      :to="tag.path"
    >
      <span>#</span> {{ tag.title }}
    </g-link>
  </div>
</template>

<script>
export default {
  props: ["post"]
};
</script>

<style lang="scss">
.post-tags {
  margin: 1em 0 0;

  &__link {
    margin-right: 0.7em;
    font-size: 0.8em;
    color: currentColor;
    text-decoration: none;
    background-color: var(--bg-color);
    color: currentColor !important; //Todo: remove important;
    padding: 0.5em;
    border-radius: var(--radius);
  }
}
</style>
EOF
	# }}}
	# PostMeta.vue {{{
	file=src/components/PostMeta.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="post-meta">
    Posted {{ post.date }}.
    <template v-if="post.timeToRead">
      <strong>{{ post.timeToRead }} min read.</strong>
    </template>
  </div>
</template>

<script>
export default {
  props: ["post"]
};
</script>

<style lang="scss">
.post-meta {
  font-size: 0.8em;
  opacity: 0.8;
}
</style>
EOF
	# }}}
	# PostCard.vue {{{
	file=src/components/PostCard.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div
    class="post-card content-box"
    :class="{ 'post-card--has-poster': post.poster }"
  >
    <div class="post-card__header">
      <g-image
        alt="Cover image"
        v-if="post.cover_image"
        class="post-card__image"
        :src="post.cover_image"
      />
    </div>
    <div class="post-card__content">
      <h2 class="post-card__title" v-html="post.title" />
      <p class="post-card__description" v-html="post.description" />

      <PostMeta class="post-card__meta" :post="post" />
      <PostTags class="post-card__tags" :post="post" />

      <g-link class="post-card__link" :to="post.path">Link</g-link>
    </div>
  </div>
</template>

<script>
import PostMeta from "~/components/PostMeta";
import PostTags from "~/components/PostTags";

export default {
  components: {
    PostMeta,
    PostTags
  },
  props: ["post"]
};
</script>

<style lang="scss">
.post-card {
  margin-bottom: var(--space);
  position: relative;

  &__header {
    margin-left: calc(var(--space) * -1);
    margin-right: calc(var(--space) * -1);
    margin-bottom: calc(var(--space) / 2);
    margin-top: calc(var(--space) * -1);
    overflow: hidden;
    border-radius: var(--radius) var(--radius) 0 0;

    &:empty {
      display: none;
    }
  }

  &__image {
    min-width: 100%;
  }

  &__title {
    margin-top: 0;
  }

  &:hover {
    transform: translateY(-5px);
    box-shadow: 1px 10px 30px 0 rgba(0, 0, 0, 0.1);
  }

  &__tags {
    z-index: 1;
    position: relative;
  }

  &__link {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0;
    overflow: hidden;
    text-indent: -9999px;
    z-index: 0;
  }
}
</style>
EOF
	# }}}
	# Logo.vue {{{
	file=src/components/Logo.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <g-link class="logo" to="/">
    <span class="logo__text"> &larr; {{ $static.metadata.siteName }} </span>
  </g-link>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<style lang="scss">
.logo {
  text-decoration: none;
  color: var(--body-color) !important;
  font-size: 0.9em;

  &__image {
    vertical-align: middle;
    border-radius: 99px;
    height: 40px;
    width: 40px;
    margin-right: 0.5em;
  }
}
</style>
EOF
	# }}}
	# Author.vue {{{
	file=src/components/Author.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="author">
    <g-image
      alt="Author image"
      class=""
      src="~/assets/images/author.jpg"
      width="180"
      height="180"
      blur="5"
    />

    <h1 v-if="showTitle" class="">
      {{ $static.metadata.siteName }}
    </h1>

    <p class="">
      A simple, hackable & minimalistic starter for Gridsome that uses Markdown
      for content.
    </p>

    <p class="">
      <a href="//twitter.com/gridsome">Follow on Twitter</a>
      <a href="//github.com/gridsome/gridsome-starter-blog">GitHub</a>
    </p>
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<script>
export default {
  props: ["showTitle"]
};
</script>

<style lang="scss">
</style>
EOF
	# }}}
	# Pages:
	# Index.vue {{{
	file=src/pages/Index.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout :show-logo="false">

    <!-- List posts -->
    <div class="posts">
      <PostCard
        v-for="edge in $page.posts.edges"
        :key="edge.node.id"
        :post="edge.node"
      />
    </div>

    <Pagination
      :baseUrl="posts"
      :currentPage="$page.posts.pageInfo.currentPage"
      :totalPages="$page.posts.pageInfo.totalPages"
      :maxVisibleButtons="5"
      v-if="$page.posts.pageInfo.totalPages > 1"
    />

    <!-- Author intro -->
    <Author :show-title="true" />

  </Layout>
</template>

<page-query>
query($page: Int) {
  posts: allPost (page: $page, perPage: 3) @paginate {
    totalCount
    pageInfo {
      totalPages
      currentPage
    }
    edges {
      node {
        id
        title
        date (format: "D. MMMM YYYY")
        timeToRead
        description
        cover_image (width: 770, height: 380, blur: 10)
        path
        tags {
          id
          title
          path
        }
        author {
          id
          title
          path
        }
      }
    }
  }
}
</page-query>

<script>
import Author from "~/components/Author.vue";
import PostCard from "~/components/PostCard.vue";
import Pagination from "~/components/Pagination.vue";

export default {
  components: {
    Pagination,
    Author,
    PostCard
  },
  metaInfo: {
    title: "Hello, world!"
  }
};
</script>
EOF
	# }}}
} #}}}
# main {{{
[ -z $1 ] && opt="null" || opt=$1
[ -z $2 ] && dir="test" || dir=$2
arr=(
	gen
	all
)
[[ " ${arr[@]} " =~ " ${opt} " ]] || cd $dir

case $opt in
"gen") gen ;;
"deps") deps ;;
"fill") fill ;;
"file") file ;;
"all") gen && deps && fill && gridsome develop ;;
#"all") gen && deps && fill && file && gridsome develop ;;
*) echo "Options are: gen, fill, dev" ;;
esac
