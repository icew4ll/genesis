#!/bin/bash
gen() { #{{{
	[ -d $dir ] && echo "Removing $dir" && rm -rf $dir
	gridsome create $dir
	cd $dir
	ncu -u
	yarn
}        #}}}
deps() { #{{{
	dev=(
		@gridsome/plugin-sitemap
		gridsome-plugin-rss
		#gridsome-plugin-remark-shiki
		@gridsome/remark-prismjs
		@gridsome/source-filesystem
		@gridsome/transformer-remark
		node-sass
		sass-loader
		tailwindcss
		gridsome-plugin-tailwindcss
		tailwindcss-gradients
		tailwindcss-tables
		vue-feather-icons
	)
	yarn add -D ${dev[@]}
}        #}}}
fill() { #{{{
	# main.js {{{
	file=src/main.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
// Import main css

// Import default layout so we don't need to import it to every page
import DefaultLayout from "~/layouts/Default.vue";

// The Client API can be used here. Learn more: gridsome.org/docs/client-api
export default function(Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);
}
EOF
	# }}}
	# image {{{
	file=content/posts/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author
	file=src/assets/images/author.jpg
	mkdir -p "$(dirname $file)"
	dl='https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimg4.wikia.nocookie.net%2F__cb20100607195345%2Fdeusex%2Fen%2Fimages%2F3%2F32%2FJccover.jpg&f=1&nofb=1'
	wget -c $dl -O $file
	# }}}
	# blog {{{
	for num in {1..12}; do
		file=content/posts/entry$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Styles$num
EOF
		cat <<'EOF' >>$file
date: 2019-01-07
author: gridsome
cover_image: ./images/alexandr-podvalny-220262-unsplash.jpg
description: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
tags: ['Markdown', 'Cover Image']
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# gridsome.config.js {{{
	file=gridsome.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
const url = "https://spiritwalk.netlify.com";
const site = "Spiritwalk";
const desc =
  "Believe not every spirit, but try the spirits whether they are of God";
module.exports = {
  siteName: site,
  siteDescription: desc,
  siteUrl: url,
  titleTemplate: `%s | ` + site,

  templates: {
    Post: "/:title",
    Tag: "/tag/:id",
    Author: '/author/:id',
  },

  plugins: [
    {
      // Create posts from markdown files
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Post",
        path: "content/posts/*.md",
        refs: {
          // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
          tags: {
            typeName: "Tag",
            create: true
          },
          author: {
            typeName: 'Author',
            create: true,
          }
        }
      }
    },
    {
      use: 'gridsome-plugin-tailwindcss',
      options: {
        tailwindConfig: './tailwind.config.js',
        purgeConfig: {
          // Prevent purging of prism classes.
          whitelistPatternsChildren: [
            /token$/
          ]
        }
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        cacheTime: 600000 // default
      }
    },
    {
      use: "gridsome-plugin-rss",
      options: {
        contentTypeName: "Post",
        feedOptions: {
          title: desc,
          feed_url: url + "/rss.xml",
          site_url: url
        },
        feedItemOptions: node => ({
          title: node.title,
          description: node.description,
          url: url + node.path,
          author: node.author,
          date: node.date
        }),
        output: {
          dir: "./static",
          name: "rss.xml"
        }
      }
    }
  ],
  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: "_blank",
      externalLinksRel: ["nofollow", "noopener", "noreferrer"],
      anchorClassName: "icon icon-link",
      plugins: ["@gridsome/remark-prismjs"]
    }
  }
};
EOF
	# }}}
	# tailwind.config.js {{{
	file=tailwind.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
module.exports = {
  purge: ["./src/**/*.html", "./src/**/*.vue", "./src/**/*.jsx"],
  theme: {
    extend: {
      colors: {
        ui: {
          background: 'var(--color-ui-background)',
          sidebar: 'var(--color-ui-sidebar)',
          typo: 'var(--color-ui-typo)',
          primary: 'var(--color-ui-primary)',
          border: 'var(--color-ui-border)'
        }
      },
      spacing: {
        sm: '24rem'
      },
      screens: {
        xxl: '1400px'
      }
    },
    radialGradients: {
      shapes: {
        // defaults to this value
        default: "ellipse"
      },
      sizes: {
        // defaults to this value
        default: ""
      },
      positions: {
        // defaults to these values
        default: "center",
        t: "top"
      },
      colors: {
        // defaults to {}
        "gray-to-black": [
          "rgba(25, 25, 25, 1)",
          "rgba(8, 8, 8, 1)",
          "rgba(0, 0, 0, 1)"
        ]
      }
    }
  },
  variants: {},
  plugins: [
    function({ addComponents }) {
      addComponents({
        ".container": {
          maxWidth: "100%",
          "@screen sm": {
            maxWidth: "640px"
          },
          "@screen md": {
            maxWidth: "768px"
          },
          "@screen lg": {
            maxWidth: "1024px"
          },
          "@screen xl": {
            maxWidth: "1040px"
          }
        }
      });
    },
    function({ addBase, config }) {
      addBase({
        h1: {
          fontSize: config("theme.fontSize.5xl"),
          fontWeight: config("theme.fontWeight.bold"),
          fontFamily: config("theme.fontFamily.sans").join(", "),
          marginTop: config("theme.margin.4"),
          marginBottom: config("theme.margin.4")
        },
        h2: {
          fontSize: config("theme.fontSize.4xl"),
          fontWeight: config("theme.fontWeight.bold"),
          fontFamily: config("theme.fontFamily.sans").join(", "),
          marginTop: config("theme.margin.4"),
          marginBottom: config("theme.margin.4")
        },
        h3: {
          fontSize: config("theme.fontSize.3xl"),
          fontWeight: config("theme.fontWeight.bold"),
          fontFamily: config("theme.fontFamily.sans").join(", "),
          marginTop: config("theme.margin.4"),
          marginBottom: config("theme.margin.4")
        },
        h4: {
          fontSize: config("theme.fontSize.2xl"),
          fontWeight: config("theme.fontWeight.bold"),
          fontFamily: config("theme.fontFamily.sans").join(", "),
          marginTop: config("theme.margin.4"),
          marginBottom: config("theme.margin.4")
        },
        h5: {
          fontSize: config("theme.fontSize.xl"),
          fontWeight: config("theme.fontWeight.bold"),
          fontFamily: config("theme.fontFamily.sans").join(", "),
          marginTop: config("theme.margin.4"),
          marginBottom: config("theme.margin.4")
        },
        h6: {
          fontSize: config("theme.fontSize.lg"),
          fontWeight: config("theme.fontWeight.bold"),
          fontFamily: config("theme.fontFamily.sans").join(", "),
          marginTop: config("theme.margin.4"),
          marginBottom: config("theme.margin.4")
        }
      });
    },
    require("tailwindcss-tables")(),
    require("tailwindcss-gradients")
  ]
};
EOF
	# }}}
}        #}}}
file() { #{{{
	# Index.html {{{
	file=src/index.html
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<!DOCTYPE html>
<html ${htmlAttrs}>
  <head>
    ${head}
  </head>
  <body ${bodyAttrs}>
    <script>
    </script>

    ${app} ${scripts}
  </body>
</html>
EOF
	# }}}
	# Layouts
	# Default.vue {{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="font-sans antialiased text-ui-typo bg-ui-background">
    <header class="font-sans antialiased text-ui-typo bg-ui-background">
      <div class="">
          <ToggleDarkMode class="ml-2 sm:ml-8">
            <template slot="default" slot-scope="{ dark }">
              <MoonIcon v-if="dark" size="1.5x" />
              <SunIcon v-else size="1.5x" />
            </template>
          </ToggleDarkMode>
      </div>
    </header>

    <main class="">
      <slot />
    </main>

    <footer class="">
      <span class="footer__copyright"
        >Copyright © {{ new Date().getFullYear() }}.
      </span>
      <span class=""
        >Powered by <a href="//gridsome.org"> Gridsome </a></span
      >
    </footer>
  </div>
</template>

<script>
import Logo from "~/components/Logo.vue";
import ToggleDarkMode from "@/components/ToggleDarkMode";
import {
  SunIcon,
  MoonIcon
} from "vue-feather-icons";

export default {
  props: {
    showLogo: { default: true }
  },
  components: {
    Logo,
    ToggleDarkMode,
    SunIcon,
    MoonIcon
  }
};
</script>

<style lang="scss">
:root {
  --color-ui-background: theme('colors.white');
  --color-ui-typo: theme('colors.gray.700');
  --color-ui-sidebar: theme('colors.gray.200');
  --color-ui-border: theme('colors.gray.300');
  --color-ui-primary: theme('colors.indigo.600');
}

html[lights-out] {
  --color-ui-background: theme('colors.gray.900');
  --color-ui-typo: theme('colors.gray.100');
  --color-ui-sidebar: theme('colors.gray.800');
  --color-ui-border: theme('colors.gray.800');
  --color-ui-primary: theme('colors.indigo.500');

  pre[class*="language-"],
  code[class*="language-"] {
    @apply bg-ui-border;
  }
}

* {
  transition-property: color, background-color, border-color;
  transition-duration: 100ms;
  transition-timing-function: ease-in-out;
}


h1 {
  @apply text-4xl;
}

h2 {
  @apply text-2xl;
}

h3 {
  @apply text-xl;
}

h4 {
  @apply text-lg;
}

a:not(.active):not(.text-ui-primary):not(.text-white) { @apply text-ui-typo }

p,
ol,
ul,
pre,
strong,
blockquote {
  @apply mb-4 text-base text-ui-typo;
}

.content {
  a {
    @apply text-ui-primary underline;
  }

  h1, h2, h3, h4, h5, h6 {
    @apply -mt-12 pt-20;
  }
    
  h2 + h3,
  h2 + h2,
  h3 + h3 {
    @apply border-none -mt-20;
  }

  h2,
  h3 {
    @apply border-b border-ui-border pb-1 mb-3;
  }

  ul {
    @apply list-disc;

    ul {
      list-style: circle;
    }
  }

  ol {
    @apply list-decimal;
  }

  ol,
  ul {
    @apply pl-5 py-1;

    li {
      @apply mb-2;

      p {
        @apply mb-0;
      }

      &:last-child {
        @apply mb-0;
      }
    }
  }
}

blockquote {
  @apply border-l-4 border-ui-border py-2 pl-4;

  p:last-child {
    @apply mb-0;
  }
}

code {
  @apply px-1 py-1 text-ui-typo bg-ui-sidebar font-mono border-b border-r border-ui-border text-sm rounded;
}

pre[class*="language-"] {
  @apply max-w-full overflow-x-auto rounded;

  & + p {
    @apply mt-4;
  }

  & > code[class*="language-"] {
    @apply border-none leading-relaxed;
  }
}

header {
  background-color: rgba(255, 255, 255, 0.9);
  backdrop-filter: blur(4px);
}

table {
  @apply text-left mb-6;

  td, th {
    @apply py-3 px-4;
    &:first-child {
      @apply pl-0;
    }
    &:last-child {
      @apply pr-0;
    }
  }

  tr {
    @apply border-b border-ui-border;
    &:last-child {
      @apply border-b-0;
    }
  }
}

.sidebar {
  @apply fixed bg-ui-background px-4 inset-x-0 bottom-0 w-full border-r border-ui-border overflow-y-auto transition-all z-40;
  transform: translateX(-100%);

  &.open {
    transform: translateX(0);
  }

  @screen lg {
    @apply w-1/4 px-0 bg-transparent top-0 bottom-auto inset-x-auto sticky z-0;
    transform: translateX(0);
  }
}
</style>
EOF
	# }}}
	# Templates:
	# Post.vue {{{
	file=src/templates/Post.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <div class="post-title">
      <h1 class="post-title__text">
        {{ $page.post.title }}
      </h1>

      <PostMeta :post="$page.post" />
    </div>

    <div class="post content-box">
      <div class="post__header">
        <g-image
          alt="Cover image"
          v-if="$page.post.cover_image"
          :src="$page.post.cover_image"
        />
      </div>

      <div class="post__content" v-html="$page.post.content" />

      <div class="post__footer">
        <PostTags :post="$page.post" />
      </div>
    </div>

    <div class="post-comments">
      <!-- Add comment widgets here -->
    </div>

    <Author class="post-author" />
  </Layout>
</template>

<script>
import PostMeta from "~/components/PostMeta";
import PostTags from "~/components/PostTags";
import Author from "~/components/Author.vue";

export default {
  components: {
    Author,
    PostMeta,
    PostTags
  },
  metaInfo() {
    return {
      title: this.$page.post.title,
      meta: [
        {
          name: "description",
          content: this.$page.post.description
        }
      ]
    };
  }
};
</script>

<page-query>
query Post ($id: ID!) {
  post: post (id: $id) {
    title
    path
    date (format: "D. MMMM YYYY")
    timeToRead
    tags {
      id
      title
      path
    }
    description
    content
    cover_image (width: 860, blur: 10)
  }
}
</page-query>

<style lang="scss">
</style>
EOF
	# }}}
	# Tag.vue {{{
	file=src/templates/Tag.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <h1 class="tag-title text-center space-bottom"># {{ $page.tag.title }}</h1>

    <div class="posts">
      <PostCard
        v-for="edge in $page.tag.belongsTo.edges"
        :key="edge.node.id"
        :post="edge.node"
      />
    </div>
  </Layout>
</template>

<page-query>
query Tag ($id: ID!) {
  tag (id: $id) {
    title
    belongsTo {
      edges {
        node {
          ...on Post {
            title
            path
            date (format: "D. MMMM YYYY")
            timeToRead
            description
            content
          }
        }
      }
    }
  }
}
</page-query>

<script>
import Author from "~/components/Author.vue";
import PostCard from "~/components/PostCard.vue";

export default {
  components: {
    Author,
    PostCard
  },
  metaInfo: {
    title: "Hello, world!"
  }
};
</script>

<style lang="scss"></style>
EOF
	# }}}
	# Components:
	# ToggleDarkMode.vue {{{
	file=src/components/ToggleDarkMode.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <button
    @click="handleClick"
    aria-label="Toggle Darkmode"
    title="Toggle Darkmode"
  >
    <slot :dark="isDarkMode" />
  </button>
</template>

<script>
export const LIGHTS_OUT = "lights-out";

export default {
  data() {
    return {
      isDarkMode: false
    };
  },

  methods: {
    handleClick() {
      const hasDarkMode = document.documentElement.hasAttribute(LIGHTS_OUT);

      // Toggle dark mode on click.
      return this.toggleDarkMode(!hasDarkMode);
    },

    toggleDarkMode(shouldBeDark) {
      document.documentElement.toggleAttribute(LIGHTS_OUT, shouldBeDark);

      this.isDarkMode = shouldBeDark;

      this.writeToStorage(shouldBeDark);

      return shouldBeDark;
    },

    detectPrefered() {
      return window.matchMedia("(prefers-color-scheme: dark)").matches;
    },

    hasInStorage() {
      const check = localStorage.getItem(LIGHTS_OUT);

      return check !== null;
    },

    writeToStorage(prefersDark) {
      localStorage.setItem(LIGHTS_OUT, prefersDark ? "true" : "false");
    },

    getFromStorage() {
      return localStorage.getItem(LIGHTS_OUT) === "true" ? true : false;
    }
  },

  mounted() {
    if (this.hasInStorage()) {
      this.toggleDarkMode(this.getFromStorage());
    } else if (process.isClient && window.matchMedia) {
      this.toggleDarkMode(this.detectPrefered());
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# Pagination.vue {{{
	file=src/components/Pagination.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <section
    class=""
  >
    <nav role="navigation" aria-label="pagination">
      <ul class="">
        <li class="lg:w-1/5">
          <g-link
            :to="previousPage(info.currentPage)"
            :class="{ 'pointer-events-none opacity-0': info.currentPage == 1 }"
            class=""
            :rel="info.currentPage == 1 ? 'nofollow' : 'prev'"
          >
            &larr; Previous
          </g-link>
        </li>
        <li class="">
          Page {{ info.currentPage }} of {{ info.totalPages }}
        </li>
        <li class="lg:w-1/5 text-right">
          <g-link
            :to="nextPage(info.currentPage, info.totalPages)"
            :class="{
              'pointer-events-none opacity-0':
                info.currentPage == info.totalPages
            }"
            class=""
            :rel="info.currentPage == info.totalPages ? 'nofollow' : 'next'"
          >
            Next &rarr;
          </g-link>
        </li>
      </ul>
    </nav>
  </section>
</template>

<script>
export default {
  props: ["base", "info"],
  methods: {
    previousPage(currentPage) {
      return [0, 1].includes(currentPage - 1)
        ? `${this.basePath}/`
        : `${this.basePath}/${currentPage - 1}/`;
    },
    nextPage(currentPage, totalPages) {
      return totalPages > currentPage
        ? `${this.basePath}/${currentPage + 1}/`
        : `${this.basePath}/${currentPage}/`;
    }
  },
  computed: {
    basePath() {
      return this.base || "";
    }
  }
};
</script>
EOF
	# }}}
	# ToggleTheme.vue {{{
	file=src/components/ToggleTheme.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <button
    role="button"
    aria-label="Toggle dark/light"
    @click.prevent="toggleTheme"
    class="toggle-theme"
  >
    <svg
      v-if="darkTheme"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      stroke-width="2"
      stroke-linecap="round"
      stroke-linejoin="round"
      class="feather feather-sun"
    >
      <circle cx="12" cy="12" r="5"></circle>
      <line x1="12" y1="1" x2="12" y2="3"></line>
      <line x1="12" y1="21" x2="12" y2="23"></line>
      <line x1="4.22" y1="4.22" x2="5.64" y2="5.64"></line>
      <line x1="18.36" y1="18.36" x2="19.78" y2="19.78"></line>
      <line x1="1" y1="12" x2="3" y2="12"></line>
      <line x1="21" y1="12" x2="23" y2="12"></line>
      <line x1="4.22" y1="19.78" x2="5.64" y2="18.36"></line>
      <line x1="18.36" y1="5.64" x2="19.78" y2="4.22"></line>
    </svg>
    <svg
      v-else
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      stroke-width="2"
      stroke-linecap="round"
      stroke-linejoin="round"
      class="feather feather-moon"
    >
      <path d="M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z"></path>
    </svg>
  </button>
</template>

<script>
export default {
  data() {
    return {
      darkTheme: false
    };
  },
  methods: {
    toggleTheme() {
      this.darkTheme = !this.darkTheme;

      // This is using a script that is added in index.html
      window.__setPreferredTheme(this.darkTheme ? "dark" : "light");
    }
  },
  mounted() {
    if (window.__theme == "dark") this.darkTheme = true;
  }
};
</script>

<style lang="scss">
.toggle-theme {
  background-color: transparent;
  border: 0;
  color: var(--body-color);
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }
  &:focus {
    outline: none;
  }
}
</style>
EOF
	# }}}
	# PostTags.vue {{{
	file=src/components/PostTags.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="post-tags">
    <g-link
      class="post-tags__link"
      v-for="tag in post.tags"
      :key="tag.id"
      :to="tag.path"
    >
      <span>#</span> {{ tag.title }}
    </g-link>
  </div>
</template>

<script>
export default {
  props: ["post"]
};
</script>

<style lang="scss">
.post-tags {
  margin: 1em 0 0;

  &__link {
    margin-right: 0.7em;
    font-size: 0.8em;
    color: currentColor;
    text-decoration: none;
    background-color: var(--bg-color);
    color: currentColor !important; //Todo: remove important;
    padding: 0.5em;
    border-radius: var(--radius);
  }
}
</style>
EOF
	# }}}
	# PostMeta.vue {{{
	file=src/components/PostMeta.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="post-meta">
    Posted {{ post.date }}.
    <template v-if="post.timeToRead">
      <strong>{{ post.timeToRead }} min read.</strong>
    </template>
  </div>
</template>

<script>
export default {
  props: ["post"]
};
</script>

<style lang="scss">
.post-meta {
  font-size: 0.8em;
  opacity: 0.8;
}
</style>
EOF
	# }}}
	# PostCard.vue {{{
	file=src/components/PostCard.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div
    class="post-card content-box"
    :class="{ 'post-card--has-poster': post.poster }"
  >
    <div class="post-card__header">
      <g-image
        alt="Cover image"
        v-if="post.cover_image"
        class="post-card__image"
        :src="post.cover_image"
      />
    </div>
    <div class="post-card__content">
      <h2 class="post-card__title" v-html="post.title" />
      <p class="post-card__description" v-html="post.description" />

      <PostMeta class="post-card__meta" :post="post" />
      <PostTags class="post-card__tags" :post="post" />

      <g-link class="post-card__link" :to="post.path">Link</g-link>
    </div>
  </div>
</template>

<script>
import PostMeta from "~/components/PostMeta";
import PostTags from "~/components/PostTags";

export default {
  components: {
    PostMeta,
    PostTags
  },
  props: ["post"]
};
</script>

<style lang="scss">
.post-card {
  margin-bottom: var(--space);
  position: relative;

  &__header {
    margin-left: calc(var(--space) * -1);
    margin-right: calc(var(--space) * -1);
    margin-bottom: calc(var(--space) / 2);
    margin-top: calc(var(--space) * -1);
    overflow: hidden;
    border-radius: var(--radius) var(--radius) 0 0;

    &:empty {
      display: none;
    }
  }

  &__image {
    min-width: 100%;
  }

  &__title {
    margin-top: 0;
  }

  &:hover {
    transform: translateY(-5px);
    box-shadow: 1px 10px 30px 0 rgba(0, 0, 0, 0.1);
  }

  &__tags {
    z-index: 1;
    position: relative;
  }

  &__link {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0;
    overflow: hidden;
    text-indent: -9999px;
    z-index: 0;
  }
}
</style>
EOF
	# }}}
	# Logo.vue {{{
	file=src/components/Logo.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <g-link class="logo" to="/">
    <span class="logo__text"> &larr; {{ $static.metadata.siteName }} </span>
  </g-link>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<style lang="scss">
.logo {
  text-decoration: none;
  color: var(--body-color) !important;
  font-size: 0.9em;

  &__image {
    vertical-align: middle;
    border-radius: 99px;
    height: 40px;
    width: 40px;
    margin-right: 0.5em;
  }
}
</style>
EOF
	# }}}
	# Author.vue {{{
	file=src/components/Author.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="author">
    <g-image
      alt="Author image"
      class="author__image"
      src="~/assets/images/author.jpg"
      width="180"
      height="180"
      blur="5"
    />

    <h1 v-if="showTitle" class="author__site-title">
      {{ $static.metadata.siteName }}
    </h1>

    <p class="author__intro">
      A simple, hackable & minimalistic starter for Gridsome that uses Markdown
      for content.
    </p>

    <p class="author__links">
      <a href="//twitter.com/gridsome">Follow on Twitter</a>
      <a href="//github.com/gridsome/gridsome-starter-blog">GitHub</a>
    </p>
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<script>
export default {
  props: ["showTitle"]
};
</script>

<style lang="scss">
.author {
  margin: 0 auto;
  max-width: 500px;
  text-align: center;
  padding: calc(var(--space) / 2) 0;

  &__image {
    border-radius: 100%;
    width: 90px;
    height: 90px;
    margin-bottom: 1em;
  }

  &__intro {
    opacity: 0.8;
  }

  &__site-title {
    font-size: 1.5em;
  }

  &__links {
    margin-top: -0.5em;
    a {
      margin: 0 0.5em;
    }
  }
}
</style>
EOF
	# }}}
	# Pages:
	# Index.vue {{{
	file=src/pages/Index.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout :show-logo="false">
    <!-- Author intro -->
    <Author :show-title="true" />

    <!-- List posts -->
    <div class="posts">
      <PostCard
        v-for="edge in $page.posts.edges"
        :key="edge.node.id"
        :post="edge.node"
      />
    </div>
  </Layout>
</template>

<page-query>
query Home ($page: Int) {
  posts: allPost (page: $page, perPage: 6) @paginate {
    totalCount
    pageInfo {
      totalPages
      currentPage
    }
    edges {
      node {
        id
        title
        date (format: "D. MMMM YYYY")
        timeToRead
        description
        cover_image (width: 770, height: 380, blur: 10)
        path
        tags {
          id
          title
          path
        }
        author {
          id
          title
          path
        }
      }
    }
  }
}
</page-query>

<script>
import Author from "~/components/Author.vue";
import PostCard from "~/components/PostCard.vue";

export default {
  components: {
    Author,
    PostCard
  },
  metaInfo: {
    title: "Hello, world!"
  }
};
</script>
EOF
	# }}}
} #}}}
# main {{{
[ -z $1 ] && opt="null" || opt=$1
[ -z $2 ] && dir="test" || dir=$2
arr=(
	gen
	all
)
[[ " ${arr[@]} " =~ " ${opt} " ]] || cd $dir

case $opt in
"gen") gen ;;
"deps") deps ;;
"fill") fill ;;
"file") file ;;
"all") gen && deps && fill && file && gridsome develop ;;
*) echo "Options are: gen, fill, dev" ;;
esac
