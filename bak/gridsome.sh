#!/bin/bash

# setup
name=$1
red='\033[0;31m'
green='\033[0;32m'
nc='\033[0m' # No Color
gridsome create $name
cd $name

create_project() { #{{{
	dev=(
		@gridsome/source-filesystem
		@gridsome/transformer-remark
		@gridsome/plugin-sitemap
		gridsome-plugin-remark-shiki
		gridsome-plugin-tailwindcss
		tailwindcss
		vue-feather-icons
		node-sass
		sass-loader
	)
	yarn add -D ${dev[@]}
	echo -e "${green}DEV INSTALL: ${dev[@]}"
}                   #}}}
gridsome_config() { #{{{
	file=gridsome.config.js
	cat <<'EOF' >$file
module.exports = {
  siteName: "Gridsome",

  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Post",
        path: "content/posts/*.md",
        refs: {
          tags: "Tag"
        },
        remark: {
          plugins: [
            [
              "gridsome-plugin-remark-shiki",
              { theme: "Material-Theme-Palenight", skipInline: false }
            ]
          ]
        }
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Tag",
        path: "content/tags/*.md",
        pathPrefix: "/tags",
      }
    }
  ],

  templates: {
    Post: "/:title",
    Tag: "/tags/:id"
  }
}
EOF
}            #}}}
tag_file() { #{{{
	file=content/tags/test.md
	mkdir -p "$(dirname $file)"
	cat << 'EOF' >$file
---
id: vue
color: green
---
EOF
}             #}}}
post_file() { #{{{
	file=content/posts/test.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: test
author: Fluid
created_at: 2019-09-22T09:30:00.788Z
tags: [vue, tutorial]
---

# TEST

## Using the Gridsome CLI

The easiest way to install this theme or a Gridsome theme in general is by using their CLI tool.

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```
EOF
}                 #}}}
post_template() { #{{{
	file=src/templates/Post.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF'> $file
<template>
  <Layout>
    <div>
      <h1>
        {{ $page.post.title }}
      </h1>

    <strong>Author: </strong><span>{{ $page.post.author }}</span>

    <br />

    <span>{{ $page.post.created_at }}</span>

    <br />

    <strong>Tags: </strong>

    <span
      v-for="tag in $page.post.tags"
      :key="tag.id"
      :style="
        `background: ${
          tag.color
        }; border-radius: 4px; margin: 12px; padding: 4px;`
      "
    >
      <g-link :to="tag.path">{{ tag.id }}</g-link>
    </span>
      <div v-html="$page.post.content" />
    </div>
  </Layout>
</template>

<page-query>
query Post($id: ID!) {
  post(id: $id) {
    title
    author
    content
    created_at
    tags {
      id
      color
      path
    }
  }
}
</page-query>
EOF
}                #}}}
tag_template() { #{{{
	file=src/templates/Tag.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF'> $file
<template>
  <div>
    <h1>{{ $page.tag.id }}</h1>
    <div v-for="edge in $page.tag.belongsTo.edges" :key="edge.node.id">
      <g-link :to="edge.node.path">{{ edge.node.title }}</g-link>
    </div>
  </div>
</template>

<page-query>
query Tag($id: ID!) {
  tag(id: $id) {
    id
    belongsTo {
      edges {
        node {
          ... on Post {
            title
            path
          }
        }
      }
    }
  }
}
</page-query>
EOF
}           #}}}
Default() { #{{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF'> $file
<template>
  <div>
    <header>
      <strong>
        <g-link to="/">{{ $static.metadata.siteName }}</g-link>
      </strong>
      <nav>
        <g-link to="/">Home</g-link>
        <g-link to="/test/">Test</g-link>
      </nav>
    </header>
    <slot />
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<style lang="scss">
html {
  background-color: gray;
}
</style>
#+END_SRC
EOF
} #}}}

create_project
gridsome_config
tag_file
post_file
post_template
tag_template
Default

gridsome develop
