#!/bin/bash
gen() { #{{{
	[ -d $dir ] && echo "Removing $dir" && rm -rf $dir
	mkdir $dir
	cd $dir
	# .gitignore {{{
	file=.gitignore
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
*.log
.cache
.DS_Store
src/.temp
node_modules
dist
.env
.env.*
EOF
	# }}}
	# package.json {{{
	file=package.json
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
{
  "name": "test",
  "private": true,
  "scripts": {
    "build": "gridsome build",
    "develop": "gridsome develop",
    "explore": "gridsome explore"
  },
  "dependencies": {
    "gridsome": "^0.7.17"
  },
  "devDependencies": {
    "@gridsome/plugin-sitemap": "^0.3.0",
    "gridsome-plugin-remark-prismjs-all": "^0.3.5",
    "gridsome-source-static-meta": "github:noxify/gridsome-source-static-meta#master",
    "@gridsome/source-filesystem": "^0.6.2",
    "@gridsome/transformer-remark": "^0.6.0",
    "remark-attr": "^0.11.1",
    "gridsome-plugin-rss": "^1.2.0",
    "gridsome-plugin-tailwindcss": "^3.0.1",
    "vue-headroom": "^0.10.1",
    "node-sass": "^4.14.1",
    "rfs": "^9.0.3",
    "sass-loader": "^8.0.2",
    "tailwindcss": "^1.4.6",
    "tailwindcss-dark-mode": "^1.1.4",
    "vue-feather-icons": "^5.0.0"
  }
}
EOF
	# }}}
	mkdir static
	ncu -u
	yarn
}        #}}}
fill() { #{{{
	# main.js {{{
	file=src/main.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
import '~/assets/scss/main.scss'
import DefaultLayout from "~/layouts/Default.vue";

export default function(Vue, { router, head, isClient }) {
  Vue.component("Layout", DefaultLayout);
}
EOF
	# }}}
	# main.scss {{{
	file=src/assets/scss/main.scss
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
@import "~rfs/scss";

@tailwind base;

blockquote {
  @apply border-l;
  @apply border-l-4;
  @apply border-l-blue-500;
  @apply pl-4;
  @apply italic;
  @apply my-8;

  p {
    padding: 0 !important;
  }
}

h1,
.h1 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(3rem);
}

h2,
.h2 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(2.25rem);
}

h3,
.h3 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.875rem);
}

h4,
.h4 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.5rem);
}

h5,
.h5 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.25rem);
}

h6,
.h6 {
  @apply font-sans;
  @apply my-4;
  @apply font-medium;
  @include font-size(1.125rem);
}

@tailwind components;
@tailwind utilities;

.container {
  @apply max-w-screen-xl;
  @apply px-0;
}

.fade-enter-active,
.fade-leave-active {
  @apply transition-all;
  @apply duration-200;
}

.fade-enter,
.fade-leave-to {
  opacity: 0;
}

.featured-post-card {
  .slick-list {
    @apply h-full;
    @apply rounded-lg;

    div:not(.post-card-author):not(.featured-label):not(.post-card-content):not(.post-card-footer) {
      @apply h-full;
    }
  }

  .slick-arrow {
    @apply absolute;
    @apply bottom-0;
    @apply right-0;
    @apply text-white;
  }

  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  &:hover {
    transform: translateY(-5px);
  }

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-content {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
    @apply mt-20;
    @apply ml-10;
    @apply text-white;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
    @apply left-0;
    @apply text-white;
    @apply ml-10;
    @apply mb-10;
    @apply text-sm;
    @apply font-semibold;
  }
}

.post-card {
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  &:hover {
    transform: translateY(-5px);
  }

  @apply relative;
  @apply border;

  .post-card-author {
    @apply absolute;
    @apply top-0;
    @apply left-0;
    @apply z-10;
  }

  .post-card-image {
    @apply h-56;
    @apply w-full;
    @apply object-cover;
    @apply rounded-lg;
    @apply relative;
    @apply rounded-b-none;
  }

  .post-card-title {
    @apply leading-8;
    @apply text-2xl;
  }

  .post-card-excerpt {
    @apply font-serif;
  }

  .post-card-content {
    @apply relative;
    @apply flex-1;
  }

  .post-card-footer {
    @apply absolute;
    @apply bottom-0;
  }
}

.header {
  min-height: 500px;
}

.mobileSubnav {
  @apply absolute;
  @apply w-full;
  @apply -mx-2;
}

@media (max-width: 767px) {
  .header {
    min-height: 360px;
  }
}

.headroom {
  z-index: 500 !important;
  @apply shadow-md;
}

.mega-menu {
  left: 0;
  top: 0;
  @apply mt-16;
  @apply w-full;
  @apply absolute;
  @apply text-left;
}

/* ––––––––––––––––––––––––––––––––––––––––––––––––––
    Content
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

.mode-dark {
  .post-content {
    p,
    span:not(.token),
    li {
      @apply tracking-wider;
      @apply leading-relaxed;
      @apply font-normal;
      @apply text-gray-500;
      @include font-size(1.1rem);
    }
  }
}

.post-content {
  p,
  span:not(.token),
  li {
    @apply tracking-wider;
    @apply leading-relaxed;
    @apply font-normal;
    @apply text-gray-800;
    @include font-size(1.1rem);
  }

  ol {
    @apply list-decimal;
    @apply ml-5;
    @apply mt-5;
  }

  ul {
    @apply list-disc;
    @apply ml-5;
    @apply mt-5;
  }

  li > ul {
    @apply mt-0;
  }
}

.post-authors a::after {
  content: ", ";
}

.post-authors a:last-child::after {
  content: "";
}

pre[class*="language-"],
code[class*="language-"] {
  color: #5c6e74;

  text-shadow: none;
  font-family: Consolas, Monaco, "Andale Mono", "Ubuntu Mono", monospace;
  @include font-size(0.9rem);
  direction: ltr;
  text-align: left;
  white-space: pre;
  word-spacing: normal;
  word-break: normal;
  line-height: 1.5;
  -moz-tab-size: 4;
  -o-tab-size: 4;
  tab-size: 4;
  -webkit-hyphens: none;
  -moz-hyphens: none;
  -ms-hyphens: none;
  hyphens: none;
}

pre[class*="language-"]::selection,
code[class*="language-"]::selection,
pre[class*="language-"]::mozselection,
code[class*="language-"]::mozselection {
  text-shadow: none;
  @apply bg-gray-200;
}

pre[class*="language-"] {
  padding: 1em;
  margin: 0.5em 0;
  overflow: auto;
  @apply bg-gray-200;
}

:not(pre) > code[class*="language-"] {
  padding: 0.3em 0.3em;
  border-radius: 0.3em;
  color: #db4c69;
  @apply bg-gray-200;
}

.mode-dark {
  pre[class*="language-"]::selection,
  code[class*="language-"]::selection,
  pre[class*="language-"]::mozselection,
  code[class*="language-"]::mozselection {
    text-shadow: none;
    @apply bg-gray-800;
  }

  pre[class*="language-"],
  code[class*="language-"] {
    @apply text-gray-400;
  }

  pre[class*="language-"] {
    padding: 1em;
    margin: 0.5em 0;
    overflow: auto;
    @apply bg-gray-800;
  }

  :not(pre) > code[class*="language-"] {
    padding: 0.3em 0.3em;
    border-radius: 0.3em;
    color: #db4c69;
    @apply bg-gray-800;
  }
}

@media print {
  pre[class*="language-"],
  code[class*="language-"] {
    text-shadow: none;
  }
}

/*********************************************************
* Tokens
*/
.namespace {
  opacity: 0.7;
}

.token.comment,
.token.prolog,
.token.doctype,
.token.cdata {
  color: #93a1a1;
}

.token.punctuation {
  color: #999999;
}

.token.property,
.token.tag,
.token.boolean,
.token.number,
.token.constant,
.token.symbol,
.token.deleted {
  color: #990055;
}

.token.selector,
.token.attr-name,
.token.string,
.token.char,
.token.builtin,
.token.inserted {
  color: #669900;
}

.token.operator,
.token.entity,
.token.url,
.language-css .token.string,
.style .token.string {
  color: #a67f59;
  background: transparent;
}

.token.atrule,
.token.attr-value,
.token.keyword {
  color: #0077aa;
}

.token.function {
  color: #dd4a68;
}

.token.regex,
.token.important,
.token.variable {
  color: #ee9900;
}

.token.important,
.token.bold {
  font-weight: bold;
}

.token.italic {
  font-style: italic;
}

.token.entity {
  cursor: help;
}

/*********************************************************
* Line highlighting
*/
pre[data-line] {
  position: relative;
}

pre[class*="language-"] > code[class*="language-"] {
  position: relative;
  z-index: 1;
}

.line-highlight {
  position: absolute;
  left: 0;
  right: 0;
  padding: inherit 0;
  margin-top: 1em;
  background: #000;
  box-shadow: inset 5px 0 0 #f7d87c;
  z-index: 0;
  pointer-events: none;
  line-height: inherit;
  white-space: pre;
}

.mode-dark {
  .medium-zoom-overlay {
    background-color: #000 !important;
  }
}

.medium-zoom-overlay {
  @apply z-1000;
}

.medium-zoom-image {
  @apply z-1000;
}

figure {
  @apply my-8;
  @apply -mx-16;
  width: calc(100% + 8rem);

  figcaption {
    @apply text-center;
  }
}
EOF
	# }}}
	# image {{{
	# blog
	file=content/author/cover/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author cover
	file=content/blog/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file

	# author
	file=src/assets/images/author.jpg
	mkdir -p "$(dirname $file)"
	dl='https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimg4.wikia.nocookie.net%2F__cb20100607195345%2Fdeusex%2Fen%2Fimages%2F3%2F32%2FJccover.jpg&f=1&nofb=1'
	wget -c $dl -O $file

	# assets images
	file=src/assets/images/danil_silantev_F6Da4r2x5to.jpg
	mkdir -p "$(dirname $file)"
	dl='https://images.unsplash.com/photo-1500829243541-74b677fecc30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
	wget -c $dl -O $file
	# }}}
	# blog {{{
	for num in {1..12}; do
		file=content/blog/entry$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
title: Styles$num
EOF
		cat <<'EOF' >>$file
tags: ['tag1', 'tag2']
author: ['author1', 'author2']
created: 2019-01-07
category: Digital
image: ./images/danil_silantev_F6Da4r2x5to.jpg
excerpt: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |  $12  |
| col 3 is | right-aligned |  $1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
EOF
	done
	# }}}
	# author {{{
	for num in {1..3}; do
		file=content/author/author$num.md
		mkdir -p "$(dirname $file)"
		cat <<EOF >$file
---
id: author$num
name: author$num
EOF
		cat <<'EOF' >>$file
bio: Primis vitae mauris turpis ornare libero odio torquent vehicula proin consequat curabitur mattis
facebook: https://www.facebook.com
twitter: https://www.twitter.com
linkedin: https://www.linkedin.com
image: ./images/author.png
cover: ./cover/danil_silantev_F6Da4r2x5to.jpg
---
EOF
	done
	# }}}
	# about.md {{{
	file=content/pages/about.md
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
---
title: About us
---

## Ossa narrat sortita fecerat sit conataque

Lorem markdownum aptos pes, Inachidos caput corrumpere! Hanc haud quam [est
candore](http://quisquis-in.io/ramossuperum) conpulit meriti. Vincere ferocia
arva.

## Eleis celeberrimus loci ait falsa infelix tuoque

Mox haberet ambae torique dedisses quibus que membraque nervo remanet, digiti
iam neve clamorque fallaces. Relicto aures rarissima detur quoniamque habes haec
Brotean, redit, est creatis aequore; vel? Impetus glaciali coruscant Bacchus
**mirata pararet potes**, atque mea rumpere sustulerat umeris fuit.

## Facundis quid

Venerit conveniunt per memori sed laniarat Dromas, solum tum. Undis lacteus
infitiatur adest [acies certius](http://www.tollit-clamavit.io/) inscius, cum ad
emittunt dextra.

Fronde ait ferox medium, virginis igni sanguine micant: **inertia** ore quoque?
Iaculi quicquid **virescere misit stirpe** Theseus Venerem! Falce taceo oves,
idem fugit, non abiit palam quantum, fontes vinci et abiit. Deiectoque exstabant
**Phrygiae** cepit munus tanto.

## Et capienda Peneia

*Haec moenia pater* signataque urget, ait quies laqueo sumitque. Misit sit
moribunda terrae sequar longis hoc, cingebant copia cultros! Alis templi taeda
solet suum mihi penates quae. Cecidere *deo agger infantem* indetonsusque ipsum;
ova formasque cornu et pectora [voce oculos](http://www.tibibene.io/iter.html),
prodis pariterque sacra finibus, Sabinae. Fugarant fuerat, famam ait toto imas
sorte pectora, est et, procubuit sua Appenninigenae habes postquam.

## Quoque aut gurgite aliquis igneus

Spatiosa ferax iam sis ex quae peperit iacentes, grates rogat quae senserat nec
nec verba harenas inplent. Per dum necis in in versus quin loquendi latens;
inde. **Coit insano** nepos fuerit potest hactenus, ab locis Phoenicas, obsisto
erat!

> Nec uterum Aurorae petentes abstulit. Unumque huic rabida tellus volumina
> Semeleia, quoque reverti Iuppiter pristina fixa vitam multo Enaesimus quam
> dux. Sua **damus** decipere, ut **obortas** nomen sine vestrae vita.

Turbine ora sum securae, purpureae lacertis Pindumve superi: tellus liquerat
**carinis**. Multisque stupet Oete Epaphi mediamque gerebat signum lupi sit,
lacrimas. Tumidi fassusque hosti, deus [vixque desint
dedit](http://hisnurus.com/putares-pars) dum et, quo non, dea [suras
tantum](http://mactata.org/inducere.php). Unus acta capulo. In Dryope sic
vestigia est neu ignis in **illa mirantur agilis** densior.
EOF
	# }}}
	# gridsome.config.js {{{
	file=gridsome.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
const url = "https://spiritwalk.netlify.com";
const site = "Gridsome";
const desc = "blog posts";
module.exports = {
  siteName: site,
  siteDescription: desc,
  siteUrl: url,
  titleTemplate: `%s | ` + site,

  templates: {
    Blog: [
      {
        path: "/:title"
      }
    ],
    CustomPage: [
      {
        path: "/:title",
        component: "~/templates/CustomPage.vue"
      }
    ],
    Category: [
      {
        path: "/category/:title",
        component: "~/templates/Category.vue"
      }
    ],
    Author: [
      {
        path: "/author/:name",
        component: "~/templates/Author.vue"
      }
    ],
    Tag: [
      {
        path: "/tags/:title",
        component: "~/templates/Tag.vue"
      }
    ]
  },

  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Author",
        path: "./content/author/*.md"
      }
    },
    {
      // Create posts from markdown files
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Blog",
        path: "content/blog/**/*.md",
        refs: {
          // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
          author: "Author",
          tags: {
            typeName: "Tag",
            create: true
          },
          category: {
            typeName: "Category",
            create: true
          }
        }
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "CustomPage",
        path: "./content/pages/*.md"
      }
    },
    {
      use: "gridsome-plugin-tailwindcss",
      options: {
        tailwindConfig: "./tailwind.config.js",
        purgeConfig: {
          whitelist: [
            "svg-inline--fa",
            "table",
            "table-striped",
            "table-bordered",
            "table-hover",
            "table-sm"
          ],
          whitelistPatterns: [
            /fa-$/,
            /blockquote$/,
            /code$/,
            /pre$/,
            /table$/,
            /table-$/,
            /vueperslide$/,
            /vueperslide-$/
          ]
        },
        presetEnvConfig: {},
        shouldPurge: false,
        shouldImport: true,
        shouldTimeTravel: true,
        shouldPurgeUnusedKeyframes: true
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        cacheTime: 600000 // default
      }
    },
    {
      use: "gridsome-plugin-rss",
      options: {
        contentTypeName: "Blog",
        feedOptions: {
          title: desc,
          feed_url: url + "/rss.xml",
          site_url: url
        },
        feedItemOptions: node => ({
          title: node.title,
          excerpt: node.excerpt,
          url: url + node.path,
          author: node.author,
          created: node.created
        }),
        output: {
          dir: "./static",
          name: "rss.xml"
        }
      }
    }
  ],
  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: "_blank",
      externalLinksRel: ["nofollow", "noopener", "noreferrer"],
      anchorClassName: "icon icon-link",
      plugins: [
        "remark-autolink-headings",
        "remark-attr",
        [
          "gridsome-plugin-remark-prismjs-all",
          {
            noInlineHighlight: false,
            showLineNumbers: false
          }
        ]
      ]
    }
  },
  chainWebpack: config => {
    config.resolve.alias.set("@pageImage", "@/assets/images");
  }
};
EOF
	# }}}
	# tailwind.config.js {{{
	file=tailwind.config.js
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
//tailwind border color plugin powered by
//https://github.com/tailwindcss/tailwindcss/pull/560#issuecomment-503222143
var _ = require("lodash");
var flattenColorPalette = require("tailwindcss/lib/util/flattenColorPalette")
  .default;

module.exports = {
  purge: {
    content: ["./src/**/*.html", "./src/**/*.vue", "./src/**/*.jsx"],
    options: {
      whitelist: [
        "bg-opacity-0",
        "bg-opacity-25",
        "bg-opacity-50",
        "bg-opacity-75",
        "bg-opacity-100",
        "mode-dark"
      ]
    }
  },
  theme: {
    extend: {
      height: {
        "128": "32rem",
        "half-screen": "50vh"
      },
      backgroundOpacity: {
        "0": "0",
        "10": "0.1",
        "20": "0.2",
        "30": "0.3",
        "40": "0.4",
        "50": "0.5",
        "60": "0.6",
        "70": "0.7",
        "80": "0.8",
        "90": "0.9",
        "100": "1"
      }
    },
    fontFamily: {
      sans: [
        '"Source Sans Pro"',
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: ["Georgia", "Cambria", '"Times New Roman"', "Times", "serif"],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    },
    zIndex: {
      "-10": "-10",
      "0": 0,
      "10": 10,
      "20": 20,
      "30": 30,
      "40": 40,
      "50": 50,
      "25": 25,
      "50": 50,
      "75": 75,
      "100": 100,
      "1000": 1000,
      auto: "auto"
    },
    boxShadow: {
      default: "0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)",
      md: "0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)",
      lg:
        "0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)",
      xl:
        "0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)",
      "2xl": "0 25px 50px -12px rgba(0, 0, 0, .25)",
      "2xl-strong": "0 25px 50px -12px rgba(0, 0, 0, .5)",
      "3xl": "0 35px 60px -15px rgba(0, 0, 0, .3)",
      inner: "inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)",
      outline: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      focus: "0 0 0 3px rgba(66, 153, 225, 0.5)",
      none: "none"
    }
  },
  variants: {
    backgroundColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    textColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ],
    borderColor: [
      "responsive",
      "hover",
      "focus",
      "dark",
      "dark-hover",
      "dark-focus"
    ]
  },
  plugins: [
    require("tailwindcss-dark-mode")(),
    function({ addUtilities, e, theme, variants }) {
      const colors = flattenColorPalette(theme("borderColor"));

      const utilities = _.flatMap(
        _.omit(colors, "default"),
        (value, modifier) => ({
          [`.${e(`border-t-${modifier}`)}`]: {
            borderTopColor: `${value}`
          },
          [`.${e(`border-r-${modifier}`)}`]: {
            borderRightColor: `${value}`
          },
          [`.${e(`border-b-${modifier}`)}`]: {
            borderBottomColor: `${value}`
          },
          [`.${e(`border-l-${modifier}`)}`]: {
            borderLeftColor: `${value}`
          }
        })
      );

      addUtilities(utilities, variants("borderColor"));
    }
  ],
  corePlugins: {
    container: false
  }
};
EOF
	# }}}
}        #}}}
file() { #{{{
	# Layouts:
	# Default.vue {{{
	file=src/layouts/Default.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div id="app" class="dark:bg-black">
    <navbar @setTheme="setTheme" :theme="this.theme"></navbar>
    <slot />
  </div>
</template>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>

<script>
import Navbar from "~/components/Navbar/Navbar.vue";

export default {
  data: function() {
    return {
      theme: "light"
    };
  },
  components: {
    Navbar
  },
  methods: {
    setTheme(mode) {
      this.theme = mode;
    }
  }
};
</script>
EOF
	# }}}
	# Pages:
	# Index.vue {{{
	file=src/pages/Index.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :title="$static.metadata.siteName"
      :sub="$static.metadata.siteDescription"
      image="danil_silantev_F6Da4r2x5to.jpg"
    >
    </content-header>

    <div class="container mx-auto">
      <div class="flex flex-wrap my-4">
        <CardItem
          v-for="edge in $page.entries.edges"
          :key="edge.node.id"
          :record="edge.node"
        />
      </div>
    </div>
  </Layout>
</template>

<page-query>
  query($page: Int) {
    entries: allBlog(perPage: 24, page: $page, sortBy:"created") @paginate {
      totalCount
      pageInfo {
        totalPages
        currentPage
      }
      edges {
        node {
          id
          title
          image(width: 800)
          path
          timeToRead
          humanTime: created(format: "DD MMM YYYY")
          datetime: created
          category {
            id
            title
            path
          }
          author {
            id
            name
            image(width: 64, height: 64, fit: inside)
            path
          }
        }
      }
    }
  }
</page-query>

<static-query>
query {
  metadata {
    siteName
    siteDescription
  }
}
</static-query>

<script>
import CardItem from "~/components/Content/CardItem.vue";
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  metaInfo: {
    title: "Hello, world!"
  },
  components: {
    CardItem,
    ContentHeader
  }
};
</script>
EOF
	# }}}
	# Components:
	# ThemeSwitcher.vue {{{
	file=src/components/Navbar/ThemeSwitcher.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <a
    role="button"
    @click.prevent="toggleTheme()"
    :aria-label="'Toggle ' + nextTheme"
    :title="'Toggle ' + nextTheme"
    class="toggle-theme"
  >
    <sun-icon size="1.5x" class="custom-class" v-if="theme === 'dark'"></sun-icon>
    <sun-icon size="1.5x" class="custom-class" v-if="theme === 'light'"></sun-icon>
  </a>
</template>

<script>
import { SunIcon } from 'vue-feather-icons'
let themes = ["light", "dark"];

export default {
  components: {
    SunIcon
  },
  props: {
    theme: {
      type: String
    }
  },

  computed: {
    nextTheme() {
      const currentIndex = themes.indexOf(this.theme);
      const nextIndex = (currentIndex + 1) % themes.length;
      return themes[nextIndex];
    }
  },
  methods: {
    toggleTheme() {
      const currentIndex = themes.indexOf(this.theme);
      const nextIndex = (currentIndex + 1) % themes.length;
      window.__setPreferredTheme(themes[nextIndex]);

      this.$emit("setTheme", themes[nextIndex]);
    }
  },
  async mounted() {
    // set default
    if (typeof window.__theme !== "undefined")
      this.$emit("setTheme", window.__theme);
  }
};
</script>
EOF
	# }}}
	# NavbarDesktop.vue {{{
	file=src/components/Navbar/NavbarDesktop.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <nav
    class="hidden md:block lg:block xl:block flex items-center justify-between flex-wrap container mx-auto py-3 z-20 dark:text-gray-400"
  >
    <div class="block flex-grow flex items-center w-auto mx-4">
      <div class="flex items-center flex-shrink-0 mr-6">
        <span class="font-semibold text-xl tracking-tight">{{
          $static.metadata.siteName
        }}</span>
      </div>

      <div class="inline-block">
        <ul class="list-none flex justify-center md:justify-end">
          <li>
            <theme-switcher v-on="$listeners" :theme="theme" />
          </li>
        </ul>
      </div>
    </div>
  </nav>
</template>

<script>
import ThemeSwitcher from "~/components/Navbar/ThemeSwitcher.vue";

export default {
  data: function() {
    return {
      showSubNavigation: false,
      vcoConfig: {
        events: ["dblclick", "click"],
        isActive: true
      }
    };
  },
  components: {
    ThemeSwitcher
  },
  props: {
    theme: {
      type: String
    },
    hideSubnav: {
      type: Boolean
    }
  },
  methods: {}
};
</script>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>
EOF
	# }}}
	# Navbar.vue {{{
	file=src/components/Navbar/Navbar.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="h-16 dark:bg-black bg-white">
    <headroom
      :classes="{
        initial: 'headroom bg- dark:bg-black border-b dark:border-gray-900'
      }"
      :downTolerance="10"
      :upTolerance="20"
      :offset="15"
      @unpin="navbarUnpinned = true"
      @pin="navbarUnpinned = false"
    >
      <navbar-desktop
        v-on="$listeners"
        @openSearchModal="openSearchModal"
        :theme="theme"
        :hideSubnav="this.navbarUnpinned"
      />
    </headroom>
  </div>
</template>

<script>
import NavbarDesktop from "~/components/Navbar/NavbarDesktop.vue";
import { headroom } from "vue-headroom";

export default {
  props: {
    theme: {
      type: String
    }
  },
  data: function() {
    return {
      headerHeight: 100,
      navbarUnpinned: false
    };
  },
  components: {
    NavbarDesktop,
    headroom
  },
  methods: {},
  watch: {}
};
</script>

<static-query>
query {
  metadata {
    siteName
  }
}
</static-query>
EOF
	# }}}
	# ContentHeader.vue {{{
	file=src/components/Partials/ContentHeader.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div>
    <div
      class="z-100 text-center bg-gray-200 dark:bg-gray-900 py-10 md:py-20"
      v-if="!hasImage"
    >
      <h2 v-if="title != null" class="h1 font-extrabold dark:text-gray-400">
        {{ title }}
      </h2>
      <p v-if="sub != null" class="text-gray-600 text-light font-sans">
        {{ sub }}
      </p>
    </div>

    <div v-if="hasImage" class="z-100 relative mt-0 h-auto">
      <g-image
        v-if="hasImage && staticImage"
        :src="require(`!!assets-loader!@pageImage/${image}`)"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <g-image
        v-if="hasImage && !staticImage"
        :src="image"
        width="1400"
        height="400"
        class="object-cover absolute -z-10 h-full w-full"
      ></g-image>

      <slot>
        <div
          class="text-center text-white bg-gray-800 lg:py-48 md:py-32 py-24"
          :class="`bg-opacity-${opacity}`"
        >
          <h2 v-if="title != null" class="h1 font-extrabold">{{ title }}</h2>
          <p v-if="sub != null" class="h5 font-sans">{{ sub }}</p>
        </div>
      </slot>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    title: {
      type: String,
      default: null
    },
    sub: {
      type: String,
      default: null
    },
    image: {
      type: String | Object,
      default: null
    },
    staticImage: {
      type: Boolean,
      default: true
    },
    opacity: {
      type: Number,
      default: 50
    }
  },
  computed: {
    hasImage() {
      return this.image ? true : false;
    }
  }
};
</script>

<style></style>
EOF
	# }}}
	# CardItem.vue {{{
	file=src/components/Content/CardItem.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <div class="w-full md:w-1/2 lg:w-1/3 px-4 my-4">
    <div
      class="post-card border-gray-200 dark:border-gray-900 bg-white dark:bg-black rounded-lg hover:shadow-xl z-100 dark:bg-gray-900"
      :id="record.id"
    ></div>

    <g-link :to="record.path" class="post-card-image-link">
      <div v-if="record.featured" class="absolute top-0 right-0 pr-4 pt-4 z-10">
        <span
          class="w-6 h-6 relative block text-center leading-tight bg-white border border-gray-300 text-black rounded-full"
        >
          <font-awesome :icon="['fas', 'star']" size="xs"></font-awesome>
        </span>
      </div>
      <g-image
        :src="record.image"
        :alt="record.title"
        class="post-card-image"
      ></g-image>
    </g-link>
    <div
      class="post-card-content bg-white dark:bg-gray-900 h-full rounded-b-lg"
    >
      <div class="flex-col relative flex justify-between px-6 pt-4">
        <p class="text-xs tracking-wide font-medium mt-3 dark:text-white">
          <g-link :to="record.category.path">{{
            record.category.title
          }}</g-link>
        </p>
      </div>
      <g-link
        :to="record.path"
        class="flex-col relative flex justify-between rounded-b-lg px-6 h-40 mt-2 dark:text-white"
      >
        <h3 class="post-card-title tracking-wide mt-0">{{ record.title }}</h3>

        <div class="text-xs leading-none absolute bottom-0 pb-6">
          <p>
            <time :datetime="record.datetime">{{ record.humanTime }}</time>
            &nbsp;&bull;&nbsp;
            {{ record.timeToRead }} min read
          </p>
        </div>
      </g-link>
    </div>
  </div>
</template>

<script>
export default {
  props: {
    record: {}
  },
  computed: {}
};
</script>

<style></style>
EOF
	# }}}
	# Templates:
	# Blog.vue {{{
	file=src/templates/Blog.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <content-header
      :image="$page.blog.image"
      :staticImage="false"
      :opacity="0"
    ></content-header>

    <div
      class="container sm:pxi-0 mx-auto overflow-x-hidden text-gray-800 dark:text-gray-500"
    >
      <div class="lg:mx-32 md:mx-16 sm:mx-8 mx-4 pt-8">
        <section class="post-header container mx-auto px-0 mb-16 text-center">
          <h1
            class="text-gray-800 dark:text-gray-400 font-extrabold tracking-wider mb-6"
          >
            {{ $page.blog.title }}
          </h1>
          <span class="tracking-wide text-sm">
            <g-link class="font-medium" :to="$page.blog.category.path">{{
              $page.blog.category.title
            }}</g-link
            >&nbsp;&middot;&nbsp;
            <time :datetime="$page.blog.datetime">{{
              $page.blog.humanTime
            }}</time>
            &nbsp;&middot;&nbsp;
            {{ $page.blog.timeToRead }} min read
          </span>
        </section>
      </div>

      <div class="lg:mx-32 md:mx-16 px-4">
        <section class="post-content container mx-auto relative">
          <div class="" v-html="$page.blog.content" />
        </section>

        <section class="post-tags container mx-auto relative py-10">
          <g-link
            v-for="tag in $page.blog.tags"
            :key="tag.id"
            :to="tag.path"
            class="text-xs bg-transparent hover:text-blue-700 py-2 px-4 mr-2 border hover:border-blue-500 border-gray-600 text-gray-700 dark:text-gray-400 rounded-full"
            >{{ tag.title }}</g-link
          >
        </section>
      </div>
    </div>
  </Layout>
</template>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.blog.title,
      meta: [
        {
          name: "description",
          content: this.$page.blog.excerpt
        }
      ]
    };
  }
};
</script>

<page-query>
query($id: ID!) {
  blog(id: $id) {
    title
    path
    image(width:1600, height:800, blur: 10)
    excerpt 
    content
    humanTime : created(format:"DD MMMM YYYY")
    datetime : created(format:"ddd MMM DD YYYY hh:mm:ss zZ")
    timeToRead
    tags {
      id
      title
      path
    }
    category {
      id
      title
      path
      belongsTo(limit:4) {
        totalCount
        edges {
          node {
            ... on Blog {
              title
              path
            }
          }
        }
      }
    }
    author {
      id
      name
      image
      path
      bio
    }
  }
}
</page-query>

<style lang="scss"></style>
EOF
	# }}}
	# Category.vue {{{
	file=src/templates/Category.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
  <h1>{{ $page.category.title }}</h1>
  </Layout>
</template>

<page-query>
  query($id: ID!) {
    category(id: $id) {
      title
      path
    }  
  }
</page-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.category.title
    };
  }
};
</script>
EOF
	# }}}
	# Author.vue {{{
	file=src/templates/Author.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <h1>{{ title }}</h1>
  </Layout>
</template>

<page-query>
  query($id: ID!) {
    author(id: $id) {
      name 
      path
    }  
  }
</page-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.author.name
    };
  }
};
</script>
EOF
	# }}}
	# CustomPage {{{
	file=src/templates/CustomPage.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
    <h1>{{ $page.page.title }}</h1>
    <h1>{{ $page.page.content }}</h1>
  </Layout>
</template>

<page-query>
  query($id: ID!) {
    page: customPage(id: $id) {
      title
      content
    }
  }
</page-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.page.title
    };
  }
};
</script>
EOF
	# }}}
	# Tag.vue {{{
	file=src/templates/Tag.vue
	mkdir -p "$(dirname $file)"
	cat <<'EOF' >$file
<template>
  <Layout>
  <h1>{{ $page.tag.title }}</h1>
  <h1>{{ $page.tag.path }}</h1>
  </Layout>
</template>

<page-query>
  query($id: ID!) {
    tag(id: $id) {
      title
      path
    }  
  }
</page-query>

<script>
import ContentHeader from "~/components/Partials/ContentHeader.vue";

export default {
  components: {
    ContentHeader
  },
  metaInfo() {
    return {
      title: this.$page.tag.title
    };
  }
};
</script>
EOF
	# }}}
} #}}}
# main {{{
[ -z $1 ] && opt="null" || opt=$1
[ -z $2 ] && dir="t2" || dir=$2
arr=(
	gen
	all
)
[[ " ${arr[@]} " =~ " ${opt} " ]] || cd $dir

case $opt in
"gen") gen ;;
"deps") deps ;;
"fill") fill ;;
"file") file ;;
"all") gen && fill && file && gridsome develop ;;
*) echo "Options are: gen, fill, dev" ;;
esac
